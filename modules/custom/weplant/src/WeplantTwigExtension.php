<?php

namespace Drupal\weplant;


/**
 * Class DefaultService.
 *
 * @package Drupal\weplant
 */
class WeplantTwigExtension extends \Twig_Extension
{

  /**
   * {@inheritdoc}
   * This function must return the name of the extension. It must be unique.
   */
  public function getName()
  {
    return 'weplant.twigextension';
  }


  /**
   * {@inheritdoc}
   * Return your custom twig function to Drupal
   */
  public function getFunctions()
  {
    return [
        new \Twig_SimpleFunction('print_title', [$this, 'print_title']),
        new \Twig_SimpleFunction('print_title_two', [$this, 'print_title_two']),
        new \Twig_SimpleFunction('print_subtitle', [$this, 'print_subtitle']),
        new \Twig_SimpleFunction('print_blurb', [$this, 'print_blurb']),
        new \Twig_SimpleFunction('print_body', [$this, 'print_body']),
        new \Twig_SimpleFunction('isset_decade', [$this, 'isset_decade']),
        new \Twig_SimpleFunction('print_decade', [$this, 'print_decade']),
        new \Twig_SimpleFunction('print_year', [$this, 'print_year']),
        new \Twig_SimpleFunction('print_button', [$this, 'print_button']),
        new \Twig_SimpleFunction('print_bg_color', [$this, 'print_bg_color']),
        new \Twig_SimpleFunction('isset_align', [$this, 'isset_align']),
        new \Twig_SimpleFunction('print_align', [$this, 'print_align']),
        new \Twig_SimpleFunction('print_video', [$this, 'print_video']),
        new \Twig_SimpleFunction('isset_video', [$this, 'isset_video']),
        new \Twig_SimpleFunction('print_image_path', [$this, 'print_image_path']),
        new \Twig_SimpleFunction('print_image', [$this, 'print_image']),
        new \Twig_SimpleFunction('isset_bg_vid', [$this, 'isset_bg_vid']),
        new \Twig_SimpleFunction('print_bg_vid', [$this, 'print_bg_vid']),
        new \Twig_SimpleFunction('print_webm_vid', [$this, 'print_webm_vid']),
        new \Twig_SimpleFunction('print_link', [$this, 'print_link']),
    ];
  }


  public static function print_title($item) {
    if(isset($item->field_title->value)){
    print $item->field_title->value;
    }else{
      print "";
    }
  }

  public static function print_title_two($item)
  {
    if(isset($item->field_title_two->value)){
      print $item->field_title_two->value;
    }else{
      print "";
    }
  }


  public static function print_subtitle($item)
  {
    if(isset($item->field_subtitle->value)){
      print $item->field_subtitle->value;
    }else{
      print "";
    }
  }

  public static function print_blurb($item)
  {
    if(isset($item->field_blurb->value)){
      print $item->field_blurb->value;
    }else{
      print "";
    }
  }

  public static function print_body($item)
  {
    if(isset($item->field_body->value)){
      print $item->field_body->value;
    }else{
      print "";
    }
  }

  public static function isset_decade($item){
    if(isset($item->field_decade->value)){
      return TRUE;
    }else{
      return FALSE;
    }
  }

  public static function print_decade($item)
  {
    if(isset($item->field_decade->value)){
      print $item->field_decade->value;
    }else{
      print "";
    }
  }


  public static function print_year($item)
  {
    if(isset($item->field_year->value)){
      print $item->field_year->value;
    }else{
      print "";
    }
  }


  public static function print_bg_color($item)
  {
    if(isset($item->field_background_color->value)){
      print $item->field_background_color->value;
    }else{
      print "";
    }
  }

  public static function isset_align($item){
    if(isset($item->field_align->value)){
      return TRUE;
    }else{
      return FALSE;
    }
  }
  public static function print_align($item)
  {
    if(self::isset_align($item)){
      print $item->field_align->value;
    }else{
      print "";
    }
  }

  public static function isset_video($item){
    if(isset($item->field_video_url->value)){
      return TRUE;
    }else{
      return FALSE;
    }
  }
  public static function print_video($item)
  {
    if(self::isset_video($item)){
      print $item->field_video_url->value;
    }else{
      print "";
    }
  }

  public static function isset_bg_vid($item){
    if(isset($item->field_video_file->entity->uri->value)){
      return TRUE;
    }else{
      return FALSE;
    }
  }
  public static function print_bg_vid($item)
  {
    if(self::isset_bg_vid($item)){
      print file_create_url($item->field_video_file->entity->uri->value);
    }else{
      print "";
    }
  }

  public static function print_webm_vid($item)
  {
    if(self::isset_bg_vid($item)){
      print file_create_url($item->field_video_webm->entity->uri->value);
    }else{
      print "";
    }
  }

  public static function print_link($item)
  {
    if(isset($item->field_link_url->value)){
      $target = 0;
      if(isset($item->field_target->value)){
        $target = $item->field_target->value;
      }

      if ($target == 1){
        print  '<a href="' . $item->field_link_url->value . '" target="_blank" class="url"></a>';
      }
      else{
        print  '<a href="' . $item->field_link_url->value . '" class="url"></a>';
      }
      return ;
    }
    return ;
  }

  public static function print_button($item)
  {
    if(isset($item->field_link_url->value)){
      $color = "";
      if(isset($item->field_button_style->value)){
        $color = $item->field_button_style->value;
      }
      $target = 0;
      if(isset($item->field_target->value)){
        $target = $item->field_target->value;
      }

      if ($target == 1){
        print  '<a href="' . $item->field_link_url->value . '" class=" btn-default '.$color. '" target="_blank">' . $item->field_link_text->value . '</a>';
      }
      else{
        print  '<a href="' . $item->field_link_url->value . '" class=" btn-default '.$color. '" >' . $item->field_link_text->value . '</a>';
      }
      return ;
    }
    return ;
  }

  public static function print_image_path($item)
  {
    if (!empty($item->field_image->entity->uri->value)) {
      $file_url = file_create_url($item->field_image->entity->uri->value);
      print $file_url;
      return ;
    } else {
      print "";
      return ;
    }

  }
  public static function print_image($item)
  {
    if (!empty($item->field_image->entity->uri->value)) {
      $file_url = file_create_url($item->field_image->entity->uri->value);
      print  '<img src="' . $file_url . '" alt="' . $item->field_image->alt . '" width="' . $item->field_image->width . ' " height="' . $item->field_image->height . '">';
      return ;
    } else {
      print "";
      return ;
    }

  }


}