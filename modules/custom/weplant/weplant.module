<?php

/**
 * @file
 * Contains weplant.module..
 */

/**
 * Pre view hook (For blog list contextual filter to current year default)
 */

use \Drupal\views\ViewExecutable;


function weplant_views_pre_view(ViewExecutable $view, $display_id, array &$args)
{

    // Blog list grid posts (Default: current year)
//    if( !isset($_REQUEST['year']) ) {
//        if ($view->id() == 'blog' && $display_id == 'block_5') {
//            $args[0] = date('Y');
//        }
//    }

    // Blog landing archive years (Default: current year)
    if ($view->id() == 'archive' && $display_id == 'block_2') {
        $args[0] = date('Y');
    }
}

/**
 * Blog Landing
 */
function weplant_views_query_alter($view, $query) {
    if ($view->id() == 'blog' && $view->current_display == 'block_3') {
        if(isset($_REQUEST['Year']) && isset($_REQUEST['Month'])) {
            $start = strtotime($_REQUEST['Year'].'-'.$_REQUEST['Month'].'-01 00:00:00');
            $end = strtotime($_REQUEST['Year'].'-'.$_REQUEST['Month'].'-31 00:00:00');

            $query->where[1]['conditions'][] = [
              "field"    => 'node_field_data.created',
              "value"    => array($start, $end),
              "operator" => 'BETWEEN',
            ];
        } else if(isset($_REQUEST['Year'])) {
            $start = strtotime($_REQUEST['Year'].'-01-01 00:00:00');
            $end = strtotime($_REQUEST['Year'].'-12-31 00:00:00');
            $query->where[1]['conditions'][] = [
              "field"    => 'node_field_data.created',
              "value"    => array($start, $end),
              "operator" => 'BETWEEN',
            ];
        } else if(isset($_REQUEST['Month'])) {
            $start = strtotime('2020-'.$_REQUEST['Month'].'-01 00:00:00');
            $end = strtotime('2020-'.$_REQUEST['Month'].'-31 00:00:00');
            $query->where[1]['conditions'][] = [
              "field"    => 'node_field_data.created',
              "value"    => array($start, $end),
              "operator" => 'BETWEEN',
            ];
        } else {
            $start = '-2208988800';
            $end = '2524608000';
            $query->where[1]['conditions'][] = [
              "field"    => 'node_field_data.created',
              "value"    => array($start, $end),
              "operator" => 'BETWEEN',
            ];
        }
    }
}


/*
 * Implements for generate json
 */
function weplant_cron() {
  $path = 'public://json/event-response/';

  // Owl response
  $owl_response = \Drupal::httpClient()->get('https://support.rms.com/o/delegate/public/event/categories', array('headers' => array('auth-token' => '524d534f574c5f537570706f7274')));
  $owl_data = (string)$owl_response->getBody();

  // Event response
  $event_response = \Drupal::httpClient()->get('https://support.rms.com/o/delegate/public/event/search', array('headers' => array('auth-token' => '524d534f574c5f537570706f7274')));
  $event_data = (string)$event_response->getBody();

  if (!empty($owl_data)) {
    if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
      file_put_contents($path . 'owl.json', $owl_data);
    }
    drupal_set_message(t('Owl json is successfully updated.'), $type = 'status', $repeat = TRUE);
  }
  if (!empty($event_data)) {
    if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
      file_put_contents($path . 'event.json', $event_data);
    }
    drupal_set_message(t('Event json is successfully updated.'), $type = 'status', $repeat = TRUE);
  }
}

/**
 * Handling the edit node
 */

function weplant_node_prepare_form(\Drupal\node\NodeInterface $node, $operation, \Drupal\Core\Form\FormStateInterface $form_state) {

    //exit;
    if ($operation == 'edit' && $node->getEntityTypeId() == 'node') {

        $database = \Drupal::database();
        $currentAccount = \Drupal::currentUser();
        $query = $database->select('user_node_editing', 'u');
        $query->condition('node_id', $node->id())->condition('uid', $currentAccount->id(), '!=');
        $query->fields('u', ['uid', 'name', 'node_id']);
        $get_user = $query->execute()->fetchAll();
        if(count($get_user) > 0) {
            drupal_set_message(t($get_user[0]->name.' is already editing this page.'), $type = 'warning', $repeat = TRUE);
        } else {
            $check = $database->select('user_node_editing');
            $check->condition('uid', $currentAccount->id());
            $num_rows = $check->countQuery()->execute()->fetchField();
            if($num_rows > 0) {
                $database->update('user_node_editing')
                  ->fields([
                    'node_id' => $node->id()
                  ])
                  ->condition('uid', $currentAccount->id())
                  ->execute();
            } else {
                $database->insert('user_node_editing')
                  ->fields([
                    'une_id' => NULL,
                    'uid' => $currentAccount->id(),
                    'name' => $currentAccount->getDisplayName(),
                    'node_id' => $node->id()
                  ])
                  ->execute();
            }
        }
    }
}

function weplant_form_alter(array &$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
    $route = \Drupal::routeMatch()->getRouteObject();
    $is_author = \Drupal::service('router.admin_context')->isAdminRoute($route);
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
        if ($form_id == 'node_' . $node->bundle() . '_edit_form' && $is_author) {

            $form_object = $form_state->getFormObject();
            if ($form_object instanceof \Drupal\Core\Entity\EntityForm) {

                $entity = $form_object->getEntity();
                $entity_id = $entity->id();
                $database = \Drupal::database();
                $currentAccount = \Drupal::currentUser();
                $query = $database->select('user_node_editing', 'u');
                $query->condition('node_id', $entity_id)->condition('uid', $currentAccount->id(), '!=');
                $query->fields('u', ['uid', 'name', 'node_id']);
                $get_user = $query->execute()->fetchAll();
                if (count($get_user) > 0) {
                    $form['#disabled'] = true;
                    $form['#attributes']['class'][] = 'noaccess-edit-node';
                }
            }
        }
    }
}

function weplant_preprocess_page(&$variables) {
    $logged_in = \Drupal::currentUser()->isAuthenticated();
    $query_string = $_SERVER['REQUEST_URI'];
    $database = \Drupal::database();
    $currentAccount = \Drupal::currentUser();

    if($logged_in) {
        if( strpos($query_string, '?') === false ) {
            $route = \Drupal::routeMatch()->getRouteObject();
            $is_author = \Drupal::service('router.admin_context')->isAdminRoute($route);
            if($is_author) {
                $database->delete('user_node_editing')
                  ->condition('uid', $currentAccount->id())
                  ->execute();
            }
        }
    }
}

function weplant_user_logout($account) {
    $database = \Drupal::database();
    $currentAccount = \Drupal::currentUser();

    $database->delete('user_node_editing')
      ->condition('uid', $currentAccount->id())
      ->execute();
}
