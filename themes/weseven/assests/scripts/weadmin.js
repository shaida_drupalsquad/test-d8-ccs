// Use $ instead of jQuery
(function ($) {

  // On DOM ready
  $(function () {

    $('body form.noaccess-edit-node').after('<div class="edit-redirect-btn"><a href="/admin/content" class="button button--primary">Go Back</a></div>');

  });
})(jQuery);