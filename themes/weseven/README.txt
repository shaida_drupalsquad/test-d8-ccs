ABOUT weseven
-----------

weseven is the default administration theme for core in Drupal 8. This theme is
used on all admin pages and by default on node edit forms.

To read more about the weseven theme's origins (in Drupal 7) please see:
https://www.drupal.org/docs/7/core/themes/weseven

See https://www.drupal.org/docs/8/core/themes/weseven-theme for more information
on using the weseven theme.

weseven is an internal theme and shouldn't be extended by other themes. Please
see https://www.drupal.org/node/2582945 for more info.

ABOUT DRUPAL THEMING
--------------------

See https://www.drupal.org/docs/8/theming for more information on Drupal 8
theming.
