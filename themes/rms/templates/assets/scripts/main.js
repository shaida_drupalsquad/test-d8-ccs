// Force external links to open in new tab / window
(function($){
  $(function(){
    var siteName = window.location.host.replace('www.', '');
    $('a[href^="htt"]:not([target]):not([href*="www.' + siteName + '"]):not([href*="//' + siteName + '"]):not([class*="fancybox"])').attr('target', '_blank');
  });
  if ($('body').hasClass('node-2256')) {
    $('a[href^="https://"]').attr('target', '_blank');
  }

})(jQuery);

(function ($) {
  // On DOM ready
  $(function () {
    // Initialize WOW.js plugin
    new WOW().init();

    jQuery(window).on({
      'orientationchange resize scroll': function (e) {
        detect_touchDevice();
      }
    });

    function detect_touchDevice() {
      var is_touch_device = 'ontouchstart' in document.documentElement;
      if (is_touch_device) {
        jQuery('body').addClass('touch');
      }
      // Add class on touch device//

      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        jQuery('body').addClass('touch');
      } else {
        jQuery('body').removeClass('touch');
      }
    }

    objectFitImages();
  });
})(jQuery);

(function ($) {

  $(window).on("load resize", function (e) {
    if ($("body section").hasClass("full-intro")) {
      $(".intro-box").addClass("full-width-intro");
    }
    else {
      $(".intro-box").removeClass("full-width-intro");
    }
    $('.col-three-with-text .content-column h4').matchHeight({byRow: 0});
  });


  $(window).on("load", function () {
    if ($("body table").hasClass("table-bordered")) {
      $("body table.table-bordered").wrap("<div class='table-responsive'></div>");
    }
  });

// Add class on touch device//
  jQuery(document).ready(function ($) {
    var deviceAgent = navigator.userAgent.toLowerCase();
    var agentID = deviceAgent.match(/(iPad|iPhone|iPod)/i);
    if (agentID) {
      window.addEventListener('DOMContentLoaded', function () {
        $('body .col-three-with-image .col-grid  .content-box, body .leadership-slider .slider-block .slider-outer .card-item').on('touchstart', function (e) {
          'use strict'; //satisfy code inspectors
          var link = $(this); //preselect the link
          if (link.hasClass('active')) {
            return true;
          } else {
            link.addClass('active');
            // console.log("n");
            $('a.taphover').not(this).removeClass('active');
            e.preventDefault();
            return false; //extra, and to make sure the function has consistent return points
          }
        });

        $(document).on('touchstart', function () {
          $("body .leadership-slider .slider-block .slider-outer .card-item").removeClass('active');
        })

        $("body .blog-grid .search .fixed-position input").click(function () {
          $(this).addClass("change");
        });

        $("body .blog-grid .search .fixed-position .close span").click(function (e) {
          e.stopPropagation();
          $(this).parent().parent().find("input").removeClass("change");
        });
      });
    }
    else {
      $('body .col-three-with-image .col-grid  .content-box, body .leadership-slider .slider-block .slider-outer .card-item').on('touchstart', function (e) {
        'use strict'; //satisfy code inspectors
        var link = $(this); //preselect the link
        if (link.hasClass('active')) {
          return true;
        } else {
          link.addClass('active');
          // console.log("n");
          $('a.taphover').not(this).removeClass('active');
          e.preventDefault();
          return false; //extra, and to make sure the function has consistent return points
        }
      });

      $("body .blog-grid .search .fixed-position input").click(function () {
        $(this).addClass("change");
      });

      $("body .blog-grid .search .fixed-position .close span").click(function (e) {
        e.stopPropagation();
        $(this).parent().parent().find("input").removeClass("change");
      });
    }
  });
})(jQuery);


// Limit content Js START

// Get part of an object if it is set
function object_get() {
  // Turn arguments into array and get what we need now
  var args = Array.prototype.slice.call(arguments);
  var object = args.shift();
  var parameter = args.shift();

  // Get value if it exists or run recursively to look for it
  if (typeof object === 'object' || typeof object === 'function') {
    if (parameter in object) {
      if (args.length === 0) {
        return object[parameter];
      }
      args.unshift(object[parameter]);
      return object_get.apply(null, args);
    }
  }
  return undefined;
}


// Return true or false based on if an object exists
function object_exists() {
  return typeof object_get.apply(this, arguments) !== 'undefined';
}

// Limit text to a certain number of lines (requires dotdotdot plugin)
function limit_lines(target, lines, ellipsis) {
  // If target element exists
  var $target = jQuery(target);

  if ($target.length) {
    // Get line height of target
    var lineHeight = $target.css('line-height');
    if (lineHeight === 'normal') {
      lineHeight = 1.2 * parseFloat($target.css('font-size'));
    }

    // Calculate the target height
    var targetHeight = lines * parseFloat(lineHeight);

    // Set default value for ellipsis
    if (typeof ellipsis === 'undefined')
      ellipsis = '...';

    if (ellipsis === 'space')
      ellipsis = ' '

    // Run the plugin if it exists
    if (object_exists(window, 'jQuery')
        && object_exists(jQuery, 'fn', 'dotdotdot')) {
      $target.dotdotdot({
        ellipsis: ellipsis,
        height: targetHeight,
        watch: true,
//        after: "a.see-more"
      });
    }
  }
}

// Limit content Js END
