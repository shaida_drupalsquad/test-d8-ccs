(function ($) {
    //New function

    if($("body .col-two-with-cards").hasClass("col-three-benefits-cards")){
        function loadMasonary() {
            // setTimeout(function () {
            $('body .col-three-benefits-cards .content-box .column-wrapper').masonry({
                itemSelector: '.card',
                columnWidth: '.card',
                horizontalOrder: true,
                isAnimated: false,
                // percentPosition: true,
            });
            // });
        }

        function addTransition(it){
            let length =$('body .col-three-benefits-cards .content-box .column-wrapper .card').length;
            for(let i=0; i<length;i++){
                if(i>=length-it){
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card:eq('+i+') .card-inner').addClass('last-items');
                }else{
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card:eq('+i+') .card-inner').removeClass('last-items');
                }
            }
        }
        let calc = $('body .col-three-benefits-cards .content-box .column-wrapper .card').outerHeight();
        function windowSize(){
            let length =$('body .col-three-benefits-cards .content-box .column-wrapper .card').length;
            if(window.matchMedia("(min-width: 992px)").matches){
                addTransition(3);
            }
            else if (window.matchMedia("(max-width: 991px)").matches && window.matchMedia("(min-width: 596px)").matches) {
                addTransition(2);
            }else{
                addTransition(length);
            }
        }
        windowSize();
        $(window).on("load resize",function () {
            loadMasonary();
            windowSize();
            calc = $('body .col-three-benefits-cards .content-box .column-wrapper .card').outerHeight();
            var heightbg =$('body .col-two-with-cards .content-box').height();
            $('body .col-two-with-cards .bg-img').css('height',heightbg+'px');
        });

        var heightbg =$('body .col-two-with-cards .content-box').height();
        $('body .col-two-with-cards .bg-img').css('height',heightbg+'px');
        $('body .col-three-benefits-cards .content-box .column-wrapper .card .card-inner').matchHeight();

        $('body .col-three-benefits-cards .content-box .column-wrapper .card').hover( function () {

            if(!$('body').hasClass('touch')){
                $(this).addClass("hover-active");
                let duration = $(this).find('.card-inner').css('transition-duration');
                duration=parseFloat(duration)*1000;
                setHeight.call(this);
                mas(duration);
                $('html, body').delay(700).animate({
                    'scrollTop': $(this).offset().top - $('header').height()
                }, 350, 'swing', function () {
                });
            }


        },function () {
            if(!$('body').hasClass('touch')){
                if($('html').hasClass( "ua-safari")){
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').removeAttr("style");
                    // $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').css('height',calc+'px');
                }

                $(this).removeClass('hover-active');
                let duration = $(this).find('.card-inner').css('transition-duration');
                duration=parseFloat(duration)*1000;
                $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').matchHeight();
                // $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').css('height',calc+'px');
                // $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').removeAttr("style");
                mas(duration);
            }
        });


        $('body .col-three-benefits-cards .content-box .column-wrapper .card').on('click',function (e) {
            console.log("l");
            e.stopPropagation();
            if($('body').hasClass('touch')){
                if(!$(this).hasClass('hover-active')){
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card').removeClass('hover-active');
                    $(this).addClass("hover-active");
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').css('height',calc+'px');
                    let duration = $(this).find('.card-inner').css('transition-duration');
                    duration=parseFloat(duration)*1000;
                    setHeight.call(this);
                    mas(duration);
                }
                else{
                    $(this).removeClass('hover-active');
                    let duration = $(this).find('.card-inner').css('transition-duration');
                    duration=parseFloat(duration)*1000;
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').css('height',calc+'px');
                    mas(50);
                }
            }
        });

        $(document).on('click',function(){
            if($('body').hasClass('touch')){
                $('body .col-three-benefits-cards .content-box .column-wrapper .card').removeClass('hover-active');
                // $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').matchHeight();
                mas(50);
                $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').removeAttr("style");

            }
        });
        function setHeight(){
            $this = $(this);
            var winWidth = $(window).width();
            var extra_space1;
            if (winWidth >= 992) {
                extra_space1 = 0;
            }
            else if (winWidth >= 596) {
                extra_space1 = 20;
            }
            else {
                extra_space1 = 40;
            }
            let pcalc = $this.find('p > span').outerHeight() - 168,
                totalheight= calc + pcalc;
            $this.find('.card-inner').css('height',totalheight + extra_space1 +'px');
        }
    }
    function mas(duration){
        setTimeout(function(){
            loadMasonary();
        },duration+50)

    }
})(jQuery);

