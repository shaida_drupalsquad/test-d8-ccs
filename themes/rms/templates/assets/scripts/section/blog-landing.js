(function ($) {
    //Height match funchtion
    function cardHeight() {
          var big_card_body = 0;
          $('.blog-grid .column-wrapper .column-wrap .card-grid .grid .box .card-with-img .card-body').each(function(){
            if($(this).height() > big_card_body){
              big_card_body = $(this).height();
            }
          });
          $('.blog-grid .column-wrapper .column-wrap .card-grid .grid .box .card-with-img .card-body').height(big_card_body);

          var smallcard_body = 0;
          $('.blog-grid .column-wrapper .column-wrap .card-grid .grid .box  .small-card .card-body').each(function(){
            if($(this).height() > smallcard_body){
              smallcard_body = $(this).height();
            }
          });
          $('.blog-grid .column-wrapper .column-wrap .card-grid .grid .box .small-card .card-body').height(smallcard_body);
    }

    $(window).on("load", function (e) {
        $(".blog-grid .column-wrapper .column-wrap .card-grid .grid").css({"opacity": 1});
        cardHeight();
    });

    $(function () {

        // Search functionality
        $("body .blog-grid .search .fixed-position input" ).click(function () {
            $(this).addClass("change");
        });

        $("body .blog-grid .search .fixed-position .close span").click(function (e) {
            e.stopPropagation();
            $(this).parent().parent().find("input").removeClass("change");
            removeUrl('search');
  $(".blog-listing .search input").val(' ');
  $('#views-exposed-form-blog-block-5 .form-item-keys input').val('');
  setTimeout(function(){
    $('#views-exposed-form-blog-block-5 .form-submit').trigger('click');
},300);
        });

        function removeUrl(str) {
          var href = window.location.href;
          var href1 = href.split("?");
          var newitem = href1[0].toString();
          href1.shift();
          var newhref = href1.join(" ");
          newhref = newhref.split("&");
          if (href.includes(str)) {
            for (var item = 0; item < newhref.length; item++) {
              if (newhref[item].toString().includes(str)) {
                newhref.splice(item, 1)
              }
            }
            newhref = newhref.join('&');
            if (newhref === '') {
              href1 = newitem + '' + newhref;
            }
            else {
              href1 = newitem + '?' + newhref;
            }
            history.pushState({}, null, href1);
          }
        }

    });

})(jQuery);
