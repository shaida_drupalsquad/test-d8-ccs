(function ($) {
    $(function () {
        var sliderElem = $("body .banner-with-carousel .right-block .slider-outer-block");
        $(sliderElem).slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            draggable: true,
        });

    });
    $(window).on("load resize", function () {
        $("body .banner-with-carousel .right-block .slider-outer-block").css({"opacity": 1});

        var itemLength = $("body .banner-with-carousel .row-wrapper .right-block .slider-outer-block .items").length;

        if (itemLength == 1) {
            $("body .banner-with-carousel .row-wrapper .right-block").addClass("no-border ")
        }

    });
})(jQuery);
