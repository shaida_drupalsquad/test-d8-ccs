(function ($) {
  function setBannerheight() {
    if ($('body .new-banner').length > 0) {
      var winWidth = $(window).width();
      var val1, val2, section_height;
      val1 = $('body .new-banner .inner-text').height();
      val2 = $('body .new-banner .inner-text').offset().top;
      var extra_space1;
      if (winWidth >= 992) {
        extra_space1 = 130;
      }
      else if (winWidth >= 596) {
        extra_space1 = 130;
      }
      else {
        extra_space1 = 130;
      }
      section_height = val1 + val2 + extra_space1;

        $('.new-banner .bg-img').css({"height": section_height + "px"});
    }

  }

  $(document).ready(function () {
    setBannerheight();
  });
  $(window).on('load resize', function () {
    setBannerheight();
    $("body .new-banner").css({"opacity": 1});
  });

})(jQuery);