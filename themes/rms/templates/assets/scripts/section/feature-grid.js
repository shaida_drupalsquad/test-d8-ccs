(function($) {
    $(document).ready(function() {
        $(".feature-grid .content-wrap").click(function(
            e1
        ) {
            e1.stopPropagation();
        });
        var win_size = $(window).width();
        $(".feature-grid .col-wrapper").addClass("active");
        width_calc();
        slide_active();
        slide_close();

    });
    $(window).resize(function() {
        var win_size = $(window).width();
        $(".feature-grid .col-wrapper ").addClass("active");
        $(
            ".feature-grid .content-wrap .open-close-link"
        ).removeClass("close-active");
        $(".feature-grid .col-wrapper").removeClass("slide-active").removeClass("single-item");
        $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap .close-btn").removeClass("close-active");
        width_calc();
        // if (win_size < 576) {
        //     $(
        //         ".feature-grid .col-wrapper.active .content-wrap"
        //     ).removeAttr("style");
        // }
    });
    $(document).click(function() {
        var win_size = $(window).width();
        $(".feature-grid .col-wrapper ").addClass("active");
        $(
            ".feature-grid .col-wrapper .open-close-link"
        ).removeClass("close-active");
        $(".feature-grid .col-wrapper").removeClass("slide-active").removeClass("single-item");
        $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap .close-btn").removeClass("close-active");
        width_calc();
        // if (win_size < 576) {
        //     $(
        //         ".feature-grid .col-wrapper.active .content-wrap"
        //     ).removeAttr("style");
        // }
    });
    function slide_active() {
        var win_size = $(window).width();
        $(
            ".feature-grid .col-wrapper .open-close-link  .open-link "
        ).click(function(e) {
            e.stopPropagation();
            if (
                !$(this)
                    .parent()
                    .hasClass("close-active")
            ) {
                $(
                    ".feature-grid .col-wrapper .open-close-link"
                ).removeClass("close-active");
                $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap .close-btn").removeClass("close-active");
                $(".feature-grid .col-wrapper").removeClass(
                    "slide-active"
                ).removeClass("single-item");
                $(".feature-grid .col-wrapper ").addClass("active");
                var prev_width =
                    $(".feature-grid .col-wrapper").outerWidth() + "px";
                $(".feature-grid .col-wrapper .content-wrap").css(
                    "width",
                    prev_width
                );
            }
            mas();
            $(this)
                .parents(".col-wrapper")
                .removeClass("active");
            $(this)
                .parent()
                .addClass("close-active");
            $(this)
                .parents(".col-wrapper")
                .addClass("slide-active");
            $(this)
                .parents(".content-wrap")
                .find(".close-btn")
                .addClass("close-active");


            var par_width = $(this)
                    .parents(".col-wrapper")
                    .outerWidth(),
                width_div =
                    parseFloat(Math.ceil(par_width * 10) / 10).toFixed(1) * 2 + 16 + "px";
            $(this)
                .parents(".content-wrap")
                .css("width", width_div);
        });
    }
    function slide_close() {
        $(
            ".feature-grid .col-wrapper  .open-close-link  .close-link "
        ).click(function(a) {
            a.stopPropagation();
            $(this)
                .parents(".col-wrapper")
                .addClass("active");
            var prev_width =
                $(this)
                    .parents(".col-wrapper")
                    .outerWidth() + "px";
            $(this)
                .parents(".content-wrap")
                .css("width", prev_width);
            $(this)
                .parent()
                .removeClass("close-active");
            $(this)
                .parents(".col-wrapper")
                .removeClass("slide-active")
                .removeClass("single-item");

            $(this)
                .parents(".content-wrap")
                .find(".close-btn")
                .removeClass("close-active");
        });

        $(
            "body .feature-grid .outer-wrapper .col-wrapper .content-wrap .close-btn "
        ).click(function(a) {
            a.stopPropagation();
            $(this)
                .parents(".col-wrapper")
                .addClass("active");
            var prev_width =
                $(this)
                    .parents(".col-wrapper")
                    .outerWidth() + "px";
            $(this)
                .parents(".content-wrap")
                .css("width", prev_width);
            $(this)
                .parent(".content-wrap")
                .find(".open-close-link")
                .removeClass("close-active");
            $(this)
                .parents(".col-wrapper")
                .removeClass("slide-active")
                .removeClass("single-item");

            $(this)
                .removeClass("close-active");
        });


    }

    function width_calc() {
        var win_size = $(window).width();
        if (win_size >= 480) {
            var init_width =
                $(".feature-grid .col-wrapper.active").outerWidth() +
                "px";
            $(".feature-grid .col-wrapper.active .content-wrap").css(
                "width",
                init_width
            );
        }
    }
    $(function () {
        //$('.feature-grid .outer-wrapper .col-wrapper .content-wrap').matchHeight();
    });

    $(window).on('load resize', function () {
        $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap").css({"opacity": 1});
        $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap p").each(function(){
            if($(this).text().length < 258){
                $(this).parents(".col-wrapper").addClass("no-read-more")
            }
        });
    });

    function mas(){
        setTimeout(function(){
            var item =$("body .feature-grid .outer-wrapper .col-wrapper.slide-active");
            $(item).addClass('single-item');
        },300)

    }

    // setTimeout(function(){
    //     $(this)
    //         .parents(".col-wrapper")
    //         .addClass('single-item ');
    // }, 6000);
})(jQuery);