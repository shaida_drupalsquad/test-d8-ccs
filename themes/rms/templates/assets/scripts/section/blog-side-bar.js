(function ($) {
    $(document).ready(function () {
        if($('html').hasClass( "ua-firefox") || $('html').hasClass( "ua-ie")){
            var blog_lines = 3;
            var blog_head = 4;
        }else{
            var blog_lines = 2;
            var blog_head = 3;
        }
        limit_lines('.card-with-blog  .card.card-without-image .content-btn.item p', blog_lines);
        limit_lines('.card-with-blog  .card.card-without-image .content-btn.item h6', blog_lines);
        limit_lines('.card-with-blog  .card .content-btn.item h6', blog_head);
    });
})(jQuery);