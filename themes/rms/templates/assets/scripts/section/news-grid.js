(function ($) {
    $(window).on("load", function (e) {
        var highestBox = 0;
        $('.news-grid .column-wrap .card-grid .grid .card-with-img').each(function () {
            if ($(this).height() > highestBox) {
                highestBox = $(this).height();
            }
        });
        $('.news-grid  .column-wrap .card-grid .grid .card-with-img').height(highestBox);


        var smallBox = 0;
        $('.news-grid .column-wrap .card-grid .grid .small-card').each(function () {
            if ($(this).height() > smallBox) {
                smallBox = $(this).height();
            }
        });
        $('.news-grid .column-wrap .card-grid .grid .small-card').height(smallBox);

        $("<div class='pagination'></div>").insertAfter("body .news-grid .cover-two-col .card-grid .grid");
        $(".pager").appendTo(".pagination");


    });


})(jQuery);