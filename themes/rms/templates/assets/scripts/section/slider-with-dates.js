(function ($) {
    $(function () {
        $('.left-slider-block').slick({
            vertical: true,
            verticalSwiping: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            infinite: false,
            useCSS:false,
            asNavFor: '.years-block',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                    }
                }
            ]
        });
        $('.years-block').slick({
            vertical: true,
            verticalSwiping: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.left-slider-block',
            dots: false,
            infinite: false,
            arrows: true,
            centerMode: true,
            focusOnSelect: true,
            prevArrow: $('body .slider-with-dates .right-slider-block .button-wrap .pre-btn'),
            nextArrow: $('body .slider-with-dates .right-slider-block .button-wrap .next-btn'),
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        draggable: true,
                        vertical: false,
                        verticalSwiping: false,
                        centerMode: true,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        draggable: true,
                        vertical: false,
                        verticalSwiping: false,
                        centerMode: true,
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                        draggable: true,
                        vertical: false,
                        verticalSwiping: false,
                        centerMode: false,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        draggable: true,
                        vertical: false,
                        verticalSwiping: false,
                        centerMode: true,
                    }
                }
            ]
        });

        $(window).on("load resize", function () {
            if ((window.matchMedia("(min-width: 767px)").matches)) {
                $('body .slider-with-dates .items .col-two').matchHeight();
            }
        });
    });
    $(window).on("load", function (e) {
        $('body .slider-with-dates .left-slider-block').css({"opacity": 1});
        $('body .slider-with-dates .right-slider-block .years-block').css({"opacity": 1});
    });
})(jQuery);