(function ($) {

    $(function () {

        $(window).on("load resize", function () {
            $("body .col-three-with-image").each(function () {
                $(".blue-overlay  h5").wrap('<div class="cover-heading" />');
            });
        });
        $("body .col-three-with-image .col-grid .content-box").matchHeight();

        var boxLength = $("body .col-three-with-image .col-grid .content-box").length;

        if (boxLength == 2) {
            $("body .col-three-with-image .col-grid .content-box").addClass("two-box");

        }
        if (boxLength == 1) {
            $("body .col-three-with-image .col-grid .content-box").addClass("m-0");
        }
        if (boxLength > 3) {
            $("body .col-three-with-image .col-grid .content-box").addClass("add-more");
            $("body .col-three-with-image .col-grid").css("justify-content", "left")
        }

    });



})(jQuery);