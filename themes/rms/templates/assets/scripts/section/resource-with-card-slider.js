(function ($) {

    $(function () {
      var itemLength = $("body .resource-with-card-slider .award-top-carousel .slider-outer .items").length;
      // Run code
      $("body .resource-with-card-slider .award-top-carousel .slider-outer").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        appendDots: $('body .resource-with-card-slider .button-wrap .dots-outer'),
        prevArrow: $('body .resource-with-card-slider .button-wrap .pre-btn'),
        nextArrow: $('body .resource-with-card-slider .button-wrap .next-btn'),
      });

      if (itemLength <= 1) {
        $("body .resource-with-card-slider .award-top-carousel .button-wrap").css({"display": "none"});
      }

        //new condition
        if ($('body .resource-with-card-slider  ').length > 0) {
            var window_width = $(window).width();
            if (window_width <= 768) {
                $(document).on("scroll");
                $("body .resource-with-card-slider .button-wrap .pre-btn, body .resource-with-card-slider .button-wrap .next-btn, body .resource-with-card-slider .button-wrap .dots-outer ").click(function () {
                    $('html, body').animate({
                        scrollTop: $(".resource-with-card-slider").offset().top - 60
                    }, 2000);
                    console.log("z")
                });

            }
        }

    });
    $(window).on("load resize", function (e) {
      $(".slider-outer").css({"opacity": 1});
    });

    //
    $(document).ready(function () {
      var urlHash = window.location.hash;
      if (urlHash) {
          var urlID = urlHash.substr(1);
          var testimonialItems = $('section.resource-with-card-slider .slider-outer .items[aria-describedby]');
          var testimonialFound = false;
          var occureTestimonial = 0;
          var testimonialInter = 0;
          if (testimonialItems.length > 0) {
              $(testimonialItems).each(function (index) {
                  var wrapperdataID = $(this).attr('data-id');
                  if (wrapperdataID == urlID) {
                      occureTestimonial = testimonialInter;
                      testimonialFound = true;
                      return false;
                  }
                  testimonialInter++;
              });
              if (testimonialFound) {
                  setTimeout(function () {
                      $("body .resource-with-card-slider .slider-outer").slick("slickGoTo", occureTestimonial);
                  }, 0);
                  $('html, body').animate({
                      scrollTop: $("body .resource-with-card-slider").offset().top - 80
                  }, 500);
              }
          }
      }

  });
})(jQuery);