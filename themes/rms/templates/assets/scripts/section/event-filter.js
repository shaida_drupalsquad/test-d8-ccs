(function($){   
    if($('body').hasClass('node-1902')){
    var tmp1 = [];
    
    $(window).on('load',function(){
      var href = window.location.href, array=[];
      href= href.split('?');
      href.shift();    
      var newhref= href.join(" ");
      newhref= newhref.split("&");
      for(var i=0; i< newhref.length;i++){
        array.push(newhref[i].toString().split('='));
      }
      
      for(var i=0;i<array.length;i++) {
        var item = array[i];

        if (item[0] === "tradeshows") {
            $('.event-grid .content-wrapper .filter-wrap .check-content input[value="9"]').prop('checked', true);
             tmp1.push('9');
        }
        if(item[0] === "upcomingwebcasts"){
          $('.event-grid .content-wrapper .filter-wrap .check-content input[value="10"]').prop('checked', true);
           tmp1.push('10');
        }
        if(item[0] === "ondemandwebcasts"){
          $('.event-grid .content-wrapper .filter-wrap .check-content input[value="7"]').prop('checked', true);
           tmp1.push('7');
        }
        if(item[0] === "speakingengagements"){
          $('.event-grid .content-wrapper .filter-wrap .check-content input[value="8"]').prop('checked', true);
           tmp1.push('8');
        }
      }
      for(var i=0;i<tmp1.length;i++){
        $('#views-exposed-form-event-block-2 .js-form-item select').val([tmp1[0], tmp1[1],tmp1[2],tmp1[3]]);
      }
      setTimeout(function(){
        $('#views-exposed-form-event-block-2 .form-submit').trigger('click');
      },300)           
  });

    $(document).on('click','.event-grid .check-content input',function(){
        var checked = $(this).val();
        var text= $(this).next().text();
        if(text.match('Tradeshows & Conferences'))
            text = 'tradeshows';
        else if(text.match('Upcoming Webcasts'))
            text= 'upcomingwebcasts';
        else if(text.match('Speaking Engagements'))
            text= 'speakingengagements';
        else
            text = 'ondemandwebcasts';

      if ($(this).is(':checked')) {
        tmp1.push(checked);
        urlUpdate(text,checked)
      } else {
        tmp1.splice($.inArray(checked, tmp1),1);
        removeUrl(text);
      }
        $('#views-exposed-form-event-block-2 .js-form-item select').val([tmp1[0], tmp1[1],tmp1[2],tmp1[3]]);
      if(tmp1.length===0){
        $('#views-exposed-form-event-block-2 .js-form-item select').val(['10', '9','7','8']);
      }
        setTimeout(function(){
          $('#views-exposed-form-event-block-2 .form-submit').trigger('click');
        },300)
    });
    $('.event-grid .reset-filter').on('click',function(){
      tmp1=[];
      var href = window.location.href;
        var href1 = href.split("?");
        history.pushState({}, null, href1[0]);
      $('.event-grid .content-wrapper .filter-wrap .check-content').each(function(){
        if ($(this).find('input').is(':checked')) {
          $(this).find('input').prop('checked', false);
        }
      });
      $('#views-exposed-form-event-block-2 .js-form-item select').val(['10', '9','7','8']);
      setTimeout(function(){
        $('#views-exposed-form-event-block-2 .form-submit').trigger('click');
      },300)
    })
    function removeUrl(str) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var item = 0; item < newhref.length; item++) {
            if (newhref[item].toString().includes(str)) {
              newhref.splice(item, 1)
            }
          }
          newhref = newhref.join('&');
          if (newhref === '') {
            href1 = newitem + '' + newhref;
          }
          else {
            href1 = newitem + '?' + newhref;
          }
          history.pushState({}, null, href1);
        }
      }
    
        // Param Update
        function urlUpdate(str, value) {
            var url = window.location.href;
            if (url.indexOf("?") > -1) {
                var urlArr = url.split("?");
                var makeURL = urlArr[0];
                for(var i=0;i<urlArr.length;i++) {
                    if(i == 0) {
                        makeURL = makeURL + '?'+urlArr[1];
                    } else {
                        makeURL = makeURL + '&'+str;
                    }
                }
                history.pushState({}, null, makeURL);
            } else {
                history.pushState({}, null, url + '?' + str);
            }
        }
        //
    }
})(jQuery);        