(function($){
    $(document).ready(function () {
        $('.banner-second-level .column-wrapper .slider').slick({
            dots: true,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            appendDots: $('body .banner-second-level .dots-outer'),
            prevArrow: $('body .banner-second-level .pre-btn'),
            nextArrow: $('body .banner-second-level .next-btn'),
            draggable: true,
            fade: true,
            speed: 900,
            infinite: true,
            cssEase: 'opacity 2000ms ease-out 0s',

        });
        $(window).on("load resize", function (e) {
            $(".banner-second-level .column-wrapper .slider").css({"opacity": 1});
        });
        
        var item_length = $('.banner-second-level .column-wrapper .slider .item').length;
        if (item_length > 1) {
            $('body .banner-second-level').addClass('all-item');

        } else if (item_length === 1) {
            $('body .banner-second-level').addClass('no-card-item');
        }

    });
})(jQuery);



