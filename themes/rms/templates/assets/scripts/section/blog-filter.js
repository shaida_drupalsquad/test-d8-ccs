if (!String.prototype.includes) {
    String.prototype.includes = function (search, start) {
        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}


(function ($) {

    $(function () {
        if ($('body').hasClass('node-1900')) {
            var val1 = 2020;
            var val = 0;

            /* Ajax filter functionality commented start */

            // $(document).on('change', '.blog-landing .select-plugin .Year', function () {
            //   $('#views-exposed-form-blog-block-3 .form-item-created-min input').val('2020-01-01 00:00:00');
            //   $('#views-exposed-form-blog-block-3 .form-item-created-max input').val('2020-12-31 00:00:00');
            //   val1 = $(this).val();
            //   if (val1 === '') {
            //     removeUrl('Month');
            //     removeUrl('Year');
            //     $('#blog-month').parent().css('display', 'none');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-min input').val('');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-max input').val('');
            //     setTimeout(function () {
            //       $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //
            //     }, 300)
            //   } else {
            //     urlUpdate('Year', val1);
            //     $('#blog-month').parent().css('display', 'block');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(val1 + '-01-01 00:00:00');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(val1 + '-12-31 00:00:00');
            //     setTimeout(function () {
            //       $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //     }, 300)
            //   }
            // });
            // $(document).on('change', '.blog-landing .select-plugin .Month', function () {
            //   $('#views-exposed-form-blog-block-3 .form-item-created-min input').val('2020-01-01 00:00:00');
            //   $('#views-exposed-form-blog-block-3 .form-item-created-max input').val('2020-12-31 00:00:00');
            //   val = $(this).val();
            //   if (val === '') {
            //     removeUrl('Month');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(val1 + '-01-01 00:00:00');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(val1 + '-12-31 00:00:00');
            //     setTimeout(function () {
            //       $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //     }, 300)
            //   } else {
            //     urlUpdate('Month', val);
            //     $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(val1 + '-' + val + '-01 00:00:00');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(val1 + '-' + val + '-31 00:00:00');
            //     setTimeout(function () {
            //       $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //     }, 300)
            //   }
            // });
            //
            // $(window).on('load', function () {
            //   var href = window.location.href, array = [], it = 2020;
            //   href = href.split('?');
            //   href.shift();
            //   var newhref = href.join(" ");
            //   newhref = newhref.split("&");
            //   for (var i = 0; i < newhref.length; i++) {
            //     array.push(newhref[i].toString().split('='));
            //   }
            //
            //   for (var i = 0; i < array.length; i++) {
            //     var item = array[i];
            //     if (item[0] === "Year") {
            //        it = item[1];
            //       $('.blog-landing .select-plugin .Year').val(it);
            //       $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(it + '-01-01 00:00:00');
            //       $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(it + '-12-31 00:00:00');
            //     }
            //
            //     if (item[0] === "Month") {
            //       var it1 = item[1];
            //       var it2;
            //       $('#blog-month').parent().css('display', 'block');
            //       $('.blog-landing .select-plugin .Month option').each(function () {
            //
            //         if (it1 === $(this).val())
            //           it2 = $(this).text();
            //       })
            //       setTimeout(function () {
            //         $('.blog-landing .select-plugin .Month').val(it1);
            //         $('.blog-landing .select-plugin .Month').parent().find('.selectboxit-text').attr('data-val', it1).text(it2)
            //         $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(it + '-' + it1 + '-01 00:00:00');
            //         $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(it + '-' + it1 + '-31 00:00:00');
            //       }, 50);
            //     }
            //   }
            //   $('body .blog-grid select').selectBoxIt('refresh');
            //   setTimeout(function () {
            //     $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //   }, 300)
            // });

            /* Ajax filter functionality ends */

            /* Non ajax functionality start */
            $(document).on('change', '.blog-landing .select-plugin .Year', function () {
                removeUrl('page');
                val1 = $(this).val();
                if (val1 === '') {
                    removeUrl('Month');
                    removeUrl('Year');
                    $('#blog-month').parent().css('display', 'none');
                } else {
                    urlUpdate('Year', val1);
                    $('#blog-month').parent().css('display', 'block');
                }
                location.reload();
                return false;
            });
            $(document).on('change', '.blog-landing .select-plugin .Month', function () {
                removeUrl('page');
                val = $(this).val();
                if (val === '') {
                    removeUrl('Month');
                } else {
                    urlUpdate('Month', val);
                }
                location.reload();
                return false;
            });

            $(window).on('load', function () {
                var href = window.location.href, array = [], it = 2020;
                href = href.split('?');
                href.shift();
                var newhref = href.join(" ");
                newhref = newhref.split("&");
                for (var i = 0; i < newhref.length; i++) {
                    array.push(newhref[i].toString().split('='));
                }

                for (var i = 0; i < array.length; i++) {
                    var item = array[i];
                    if (item[0] === "Year") {
                        it = item[1];
                        $('.blog-landing .select-plugin .Year').val(it);
                    }

                    if (item[0] === "Month") {
                        var it1 = item[1];
                        var it2;
                        $('#blog-month').parent().css('display', 'block');
                        $('.blog-landing .select-plugin .Month option').each(function () {

                            if (it1 === $(this).val())
                                it2 = $(this).text();
                        })
                        setTimeout(function () {
                            $('.blog-landing .select-plugin .Month').val(it1);
                            $('.blog-landing .select-plugin .Month').parent().find('.selectboxit-text').attr('data-val', it1).text(it2)
                        }, 50);
                    }
                }
                $('body .blog-grid select').selectBoxIt('refresh');
            });

            /* Non ajax functionality ends */

            function removeUrl(str) {
                var href = window.location.href;
                var href1 = href.split("?");
                var newitem = href1[0].toString();
                href1.shift();
                var newhref = href1.join(" ");
                newhref = newhref.split("&");
                if (href.includes(str)) {
                    for (var item = 0; item < newhref.length; item++) {
                        if (newhref[item].toString().includes(str)) {
                            newhref.splice(item, 1)
                        }
                    }
                    newhref = newhref.join('&');
                    if (newhref === '') {
                        href1 = newitem + '' + newhref;
                    }
                    else {
                        href1 = newitem + '?' + newhref;
                    }
                    history.pushState({}, null, href1);
                }
            }

            function urlUpdate(str, value) {
                var href = window.location.href;
                var href1 = href.split("?");
                var newitem = href1[0].toString();
                href1.shift();
                var newhref = href1.join(" ");
                newhref = newhref.split("&");
                if (href.includes(str)) {
                    for (var i = 0; i < newhref.length; i++) {
                        if (newhref[i].toString().includes(str)) {
                            newhref.splice(i, 1)
                            newhref.push(str + '=' + value)
                        }
                    }
                    newhref = newhref.join('&')
                    href1 = newitem + '?' + newhref;
                    history.pushState({}, null, href1);
                }
                else {
                    if (href1.length < 1) {
                        history.pushState({}, null, href + '?' + str + '=' + value);
                    } else {
                        history.pushState({}, null, href + '&' + str + '=' + value);
                    }
                }
            }

            // Pagination to get rid zero
            $('nav.pager ul li').each(function () {
                var pageLink = $(this).children('a').attr('href');
                if (typeof pageLink !== typeof undefined && pageLink !== false) {
                    if (pageLink.indexOf('page=0') > -1 && pageLink.indexOf('page=0') == 1) {
                        $(this).children('a').attr('href', window.location.pathname);
                    } else if (pageLink.indexOf('page=0') > -1 && pageLink.indexOf('page=0') > 1) {
                        $(this).children('a').attr('href', $(this).children('a').attr('href').replace('&page=0', ''));
                    }
                }
            });

        }
    });

})(jQuery);
