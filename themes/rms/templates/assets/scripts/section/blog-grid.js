(function ($) {
    //New function
    function loadMasonary() {
        setTimeout(function () {
            $('body .blog-grid .column-wrapper .column-wrap .card-grid .grid').masonry({
                itemSelector: '.box',
                columnWidth: '.box',
                horizontalOrder: true,
            });

            $('body .news-grid .cover-two-col .card-grid .grid').masonry({
                itemSelector: '.box',
                columnWidth: '.box',
                horizontalOrder: true,
            });
            $('body .event-grid .column-wrapper .column-wrap .card-grid .grid').masonry({
                itemSelector: '.box',
                columnWidth: '.box',
                horizontalOrder: true,
            });
        }, 500);
        console.log("y");
    }

    $(window).on("load",function () {
        loadMasonary();
    });
    // $(document).ajaxComplete(function () {
    //     loadMasonary();
    // });

})(jQuery);


(function ($) {
    $(window).on("load ", function () {
        setTimeout(function () {
            $("body .news-grid .cover-two-col .card-grid").css({"opacity": 1});
        }, 500);
    });
    if ($("body .blog-grid").hasClass("blog-list-cards")) {
        var flag1 = 0;
        $(window).bind("load scroll resize", function () {
            if ($('.blog-grid.blog-list-cards').length > 0) {
                var $winTop = $(window).scrollTop();
                var $winWidth = $(window).width();
                var $offset = 0;
                var $blogContainer = $('.blog-grid.blog-list-cards .container');
                if ($blogContainer.length) {
                    $offset = $('.blog-grid.blog-list-cards .container').offset().top;
                }
                var height = $('.blog-grid.blog-list-cards .container').height();
                var total = $offset + height;

                var setValue1 = 140;
                if ($winWidth >= 992) {
                    if (flag1 == 0) {
                        if (($winTop >= $offset - setValue1) && ($winTop <= $offset - 130)) {
                            $('.blog-grid.blog-list-cards').addClass('fixed');
                            flag1 = 1;
                        }
                    }
                } else {
                    $('.blog-grid.blog-list-cards').removeClass('fixed');
                }

                // New condition
                if ($winWidth >= 992) {
                    if (($winTop >= height - 100)) {
                        $('.blog-grid.blog-list-cards').addClass('active');
                        flag1 = 1;
                    } else {
                        $('.blog-grid.blog-list-cards').removeClass('active');
                    }

                    if (($winTop <= $offset - 200)) {
                        $('.blog-grid.blog-list-cards').removeClass('fixed');
                        flag1 = 0;
                    }
                }
            }
        });

    }
})(jQuery);
