(function($){ 
  if($('body').hasClass('node-1951')){  
    var tmp = [];
    
    $(window).on('load',function(){
      var href = window.location.href, array=[];
      href= href.split('?');
      href.shift();
      var newhref= href.join(" ");
      newhref= newhref.split("&");
      for(var i=0; i< newhref.length;i++){
        array.push(newhref[i].toString().split('='));
      }
      for(var i=0;i<array.length;i++) {
        var item = array[i];
        if (item[0] === "datasheets") {
            $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="datasheets"]').prop('checked', true);
             tmp.push('datasheets');
        }
        if(item[0] === "video"){
          $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="video"]').prop('checked', true);
           tmp.push('video');
        }
        if(item[0] === "publications"){
          $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="publications"]').prop('checked', true);
           tmp.push('publications');
      }
      if(item[0] === "infographics"){
          $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="infographics"]').prop('checked', true);
           tmp.push('infographics');
      }
      if(item[0] === "case_study"){
        $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="case_study"]').prop('checked', true);
         tmp.push('case_study');
    }
      }
      for(var i=0;i<tmp.length;i++){
        $('.views-exposed-form .js-form-item select').val([tmp[0], tmp[1],tmp[2],tmp[3],tmp[4]]);
      }
      setTimeout(function(){
        $('.views-exposed-form .form-submit').trigger('click');
      },300)           
  });

    $(document).on('click','.resources-grid .check-content input',function(){
        var checked = $(this).val();
      if ($(this).is(':checked')) {
        tmp.push(checked);
        urlUpdate(checked,checked)
      } else {
        tmp.splice($.inArray(checked, tmp),1);
        removeUrl(checked);
      }
      for(var i=0;i<tmp.length;i++){
        $('.views-exposed-form .js-form-item select').val([tmp[0], tmp[1],tmp[2],tmp[3],tmp[4]]);
      }
      if(tmp.length===0){
        $('.views-exposed-form .js-form-item select').val(['datasheets', 'video','publications','infographics','case_study']);
      }
        setTimeout(function(){
          $('.views-exposed-form .form-submit').trigger('click');
        },300)
    });
    $('.resources-grid .reset-filter').on('click',function(){
      tmp=[];
      var href = window.location.href;
        var href1 = href.split("?");
        history.pushState({}, null, href1[0]);
      $('.resources-grid .content-wrapper .filter-wrap .check-content').each(function(){
        if ($(this).find('input').is(':checked')) {
          $(this).find('input').prop('checked', false);
        }
      });
      $('.views-exposed-form .js-form-item select').val(['datasheets', 'video','publications','infographics','case_study']);
      setTimeout(function(){
        $('.views-exposed-form .form-submit').trigger('click');
      },300)
    })
    function removeUrl(str) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var item = 0; item < newhref.length; item++) {
            if (newhref[item].toString().includes(str)) {
              newhref.splice(item, 1)
            }
          }
          newhref = newhref.join('&');
          if (newhref === '') {
            href1 = newitem + '' + newhref;
          }
          else {
            href1 = newitem + '?' + newhref;
          }
          history.pushState({}, null, href1);
        }
      }
    
      function urlUpdate(str, value) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var i = 0; i < newhref.length; i++) {
            if (newhref[i].toString().includes(str)) {
              newhref.splice(i, 1)
              newhref.push(str + '=' + value)
            }
          }
          newhref = newhref.join('&')
          href1 = newitem + '?' + newhref;
          history.pushState({}, null, href1);
        }
        else {
          if (href1.length < 1) {
            history.pushState({}, null, href + '?' + str + '=' + value);
          } else {
            history.pushState({}, null, href + '&' + str + '=' + value);
          }
        }
      }
    }
})(jQuery);        