(function ($) {

    $(function () {
        $("body .place-location").click(function () {
            var thisImage = $(this).parents().find('.bg-img img').attr('src');
            var thisText = $(this).find("span").text();
            $("body .home-overlay").addClass("active");
            $("body .home-overlay .overlay-box img ").attr('src', thisImage);
            $("body .home-overlay .place-location span").text(thisText);

        });

        $("body .close-btn").click(function () {
            $("body .home-overlay").removeClass("active");
        });

    });

    //cookies banner//
    window.geofeed = function(options) {
        // string two digit continent code EU, NA, OC, AS, SA, AF
        var continent = options.continent.toString();
        console.log(continent);
        if (continent !== 'EU') {
            var cookieBanner = $("body .cookies-banner");

            $(".close-btn , .close-txt").on("click", function () {
                $(cookieBanner).removeClass("active");
                localStorage.setItem("cookieexist", "true");
            });

            setTimeout(function () {
                if (!localStorage.getItem("cookieexist")) {
                    $(cookieBanner).addClass("active");
                }
            }, 1000);


            $(window).on("load ", function () {
                $("body .cookies-banner").css({"opacity": 1});

            });
        }

    };

// Call geo-location JSONP service
    var jsonp = document.createElement('script');
    jsonp.setAttribute('src', 'https://geolocation.onetrust.com/cookieconsentpub/v1/geo/location/geofeed');
    document.head.appendChild(jsonp);

    setTimeout(function () {
        $(".pattern-animation").css({"visibility": "visible"});
    }, 1000);


})(jQuery);