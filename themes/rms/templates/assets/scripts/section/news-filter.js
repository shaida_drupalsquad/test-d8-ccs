(function($){   
    if($('body').hasClass('node-1903')){
    var tmp1 = [];
    $('#views-exposed-form-news-block-2 .form-item-created-min input').val('2020-01-01 00:00:00'); 
            $('#views-exposed-form-news-block-2 .form-item-created-max input').val('2020-12-31 00:00:00');
    $(window).on('load',function(){
      var href = window.location.href, array=[];
      href= href.split('?');
      href.shift();    
      var newhref= href.join(" ");
      newhref= newhref.split("&");
      for(var i=0; i< newhref.length;i++){
        array.push(newhref[i].toString().split('='));
      }
      
      for(var i=0;i<array.length;i++) {
        var item = array[i];

        if (item[0] === "news") {
            $('.news-filter .content-wrapper .filter-wrap .check-content input[value="1"]').prop('checked', true);
             tmp1.push('1');
        }
        if(item[0] === "pressRelease"){
          $('.news-filter .content-wrapper .filter-wrap .check-content input[value="2"]').prop('checked', true);
           tmp1.push('2');
        }

        if (item[0] === "Year") {
            var itt = item[1];
            $('.news-filter .select-plugin .Year').val(itt);
            $('#views-exposed-form-news-block-2 .form-item-created-min input').val(itt+'-01-01 00:00:00'); 
            $('#views-exposed-form-news-block-2 .form-item-created-max input').val(itt+'-12-31 00:00:00');
        }
        
        if(item[0] === "Month"){
            var itt1= item[1];
          $('.news-filter .select-plugin .Month').parent().css('display','block');
          setTimeout(function(){     
            $('.news-filter .select-plugin .Month').val(itt1);
          $('#views-exposed-form-news-block-2 .form-item-created-min input').val(itt+'-'+itt1+'-01 00:00:00')  ;  
          $('#views-exposed-form-news-block-2 .form-item-created-max input').val(itt+'-'+itt1+'-31 00:00:00')  ; 
          },50);
        }
      }
      for(var i=0;i<tmp1.length;i++){
        $('#views-exposed-form-news-block-2 .js-form-item select').val([tmp1[0], tmp1[1]]);
      }
      setTimeout(function(){
        $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
      },300)           
  });


    $(document).on('click','.news-filter .check-content input',function(){
        var checked = $(this).val();
        var text= $(this).next().text();
        if(text.match('News'))
            text = 'news';
        else
            text = 'pressRelease';
      if ($(this).is(':checked')) {
        tmp1.push(checked);
        urlUpdate(text,checked)
      } else {
        tmp1.splice($.inArray(checked, tmp1),1);
        removeUrl(text);
      }
      for(var i=0;i<tmp1.length;i++){
        $('.views-exposed-form .js-form-item select').val([tmp1[0], tmp1[1]]);
      }
      if(tmp1.length===0){
        console.log(true);
        $('.views-exposed-form .js-form-item select').val(['1', '2']);
      }
        setTimeout(function(){
          $('.views-exposed-form .form-submit').trigger('click');
        },300)
    });

    var val1=0;
    var val=0;

    $(document).on('change','.news-filter .select-plugin .Month',function(){
        val = $(this).val();
        if(val1==='All'){
          removeUrl('Month');
          $('#views-exposed-form-news-block-2 .form-item-created-min input').val(val1+'-01-01 00:00:00')  ;  
          $('#views-exposed-form-news-block-2 .form-item-created-max input').val(val1+'-12-31 00:00:00')  ; 
          setTimeout(function(){
              $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
            },300) 
        }else{
        $('#views-exposed-form-news-block-2 .form-item-created-min input').val(val1+'-'+val+'-01 00:00:00')  ;  
        $('#views-exposed-form-news-block-2 .form-item-created-max input').val(val1+'-'+val+'-31 00:00:00')  ; 
        setTimeout(function(){
            $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
          },300) 
        }
    });
    $(document).on('change','.news-filter .select-plugin .Year',function(){
        val1 = $(this).val();
          if(val1==='All'){
          removeUrl('Month');
          removeUrl('Year')
          $('.news-filter .select-plugin .Month').parent().css('display','none');
          $('#views-exposed-form-news-block-2 .form-item-created-min input').val('')  ;  
          $('#views-exposed-form-news-block-2 .form-item-created-max input').val('')  ; 
          setTimeout(function(){
              $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
          },300)  
      }else{
        $('.news-filter .select-plugin .Month').parent().css('display','block');
        $('#views-exposed-form-news-block-2 .form-item-created-min input').val(val1+'-01-01 00:00:00')  ;  
        $('#views-exposed-form-news-block-2 .form-item-created-max input').val(val1+'-12-31 00:00:00')  ; 
        setTimeout(function(){
            $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
          },300)  
        }  
    });
    $('.news-filter .reset-filter').on('click',function(){
      tmp1=[];
      var href = window.location.href;
        var href1 = href.split("?");
        history.pushState({}, null, href1[0]);
      $('.news-filter .content-wrapper .filter-wrap .check-content').each(function(){
        if ($(this).find('input').is(':checked')) {
          $(this).find('input').prop('checked', false);
        }
      });
      $('.views-exposed-form .js-form-item select').val(['1', '2']);
      setTimeout(function(){
        $('.views-exposed-form .form-submit').trigger('click');
      },300)
    })
    function removeUrl(str) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var item = 0; item < newhref.length; item++) {
            if (newhref[item].toString().includes(str)) {
              newhref.splice(item, 1)
            }
          }
          newhref = newhref.join('&');
          if (newhref === '') {
            href1 = newitem + '' + newhref;
          }
          else {
            href1 = newitem + '?' + newhref;
          }
          history.pushState({}, null, href1);
        }
      }

        // Param Update
        function urlUpdate(str, value) {
            var url = window.location.href;
            if (url.indexOf("?") > -1) {
                var urlArr = url.split("?");
                var makeURL = urlArr[0];
                for(var i=0;i<urlArr.length;i++) {
                    if(i == 0) {
                        makeURL = makeURL + '?'+urlArr[1];
                    } else {
                        makeURL = makeURL + '&'+str;
                    }
                }
                history.pushState({}, null, makeURL);
            } else {
                history.pushState({}, null, url + '?' + str);
            }
        }
        //
    }
})(jQuery);        