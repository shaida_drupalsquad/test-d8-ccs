(function ($) {
    $(function () {
        $("body .header-rms .header-wrap .hamburger-logo").on("click", function () {
            if ((window.matchMedia("(max-width: 991px)").matches)) {
                if ($(this).hasClass("open")) {
                    $(this).removeClass("open");
                    $("body .header-rms .header-wrap nav").stop(true, true).slideUp();
                }
                else {
                    $(this).addClass("open");
                    $("body .header-rms .header-wrap nav").stop(true, true).slideDown();
                }
            }
        });

        //banner scroll full width//
        var bannerHeight = $('body .header-rms').height() / 2;
        $(window).on('scroll load', function () {
            var windScrolled = $(document).scrollTop();
            if ((windScrolled > bannerHeight)) {
                $("body .header-rms").addClass("fixed");
            }
            else {
                $("header").removeClass("fixed");

            }
        });

        //sub menu //

        //search btn click//
        $('body .header-rms .header-wrap nav .search-box').click(function (e) {
            e.stopPropagation();
            if ($(this).hasClass("active")) {
                $(this).removeClass('active');
                $(this).find(".sub-menu").stop(true, true).slideUp();
                $(this).parent("nav").find("ul li").removeClass("no-hover");
                $(this).parent("nav").find(".login-box").removeClass("no-hover");
            }
            else {
                $(this).addClass('active');
                $(this).find(".sub-menu").stop(true, true).slideDown();
                $(this).parent("nav").find("ul li").addClass("no-hover");
                $(this).parent("nav").find(".login-box").addClass("no-hover");
            }
            $("body .header-rms .header-wrap nav .login-box").removeClass('rotate-arrow');
            $("body .header-rms .header-wrap nav .login-box > .profiles-block").stop(true, true).slideUp(500);
            $('body .header-rms .header-wrap nav ul li').removeClass('rotate-arrow');
            $("body .header-rms .header-wrap nav ul li > .sub-menu").stop(true, true).slideUp(500);
        });
        $("body .header-rms .header-wrap nav .search-box .sub-menu").click(function (e) {
            e.stopPropagation();
        });
        $("body .header-rms .header-wrap nav ul li > .sub-menu").click(function (e) {
            e.stopPropagation();
        });
        $("body .header-rms .header-wrap nav .login-box > .profiles-block").click(function (e) {
            e.stopPropagation();
        });

        // responsive nav//
        // $(window).on("resize", function (e) {
        $("body .header-rms .header-wrap nav ul li, ul .red-link").on("click", function (e) {
            // e.stopPropagation();
            var windowWidth = $(window).width();
            if (windowWidth < 991) {
                if ($(this).hasClass('rotate-arrow')) {
                    $(this).removeClass('rotate-arrow');
                    $(this).find("> .sub-menu").stop(true, true).slideUp(500);
                }
                else {
                    // Hide the other tabs
                    $('body .header-rms .header-wrap nav ul li').removeClass('rotate-arrow');
                    $(this).addClass('rotate-arrow');
                    $("body .header-rms .header-wrap nav ul li > .sub-menu").stop(true, true).slideUp(500);
                    $(this).find("> .sub-menu").stop(true, true).slideDown(500);

                }
            }
        });

        //profile drop down//
        $("body .header-rms .header-wrap nav .login-box").on("click", function (e) {
            e.stopPropagation();
            var windowWidth = $(window).width();
            if (windowWidth < 991) {
                if ($(this).hasClass('rotate-arrow')) {
                    $(this).removeClass('rotate-arrow');
                    $(this).find("> .profiles-block").stop(true, true).slideUp(500);
                }
                else {
                    // Hide the other tabs
                    $('body .header-rms .header-wrap nav .login-box').removeClass('rotate-arrow');
                    $(this).addClass('rotate-arrow');
                    $("body .header-rms .header-wrap nav .login-box > .profiles-block").stop(true, true).slideUp(500);
                    $(this).find("> .profiles-block").stop(true, true).slideDown(500);
                }
            }
            $('body .header-rms .header-wrap nav .search-box').find("> .sub-menu").stop(true, true).slideUp();
            $('body .header-rms .header-wrap nav .search-box').removeClass('active');
            $('body .header-rms .header-wrap nav ul li').removeClass('rotate-arrow');
            $("body .header-rms .header-wrap nav ul li > .sub-menu").stop(true, true).slideUp(500);

        });
        // });


        $(document).click(function (e) {
            e.stopPropagation();
            $("body .header-rms .header-wrap nav .search-box").removeClass('active');
            $("body .header-rms .header-wrap nav .search-box .sub-menu").stop(true, true).slideUp();
            $("body .header-rms .header-wrap nav ul .red-link").removeClass('arrow-rotate');
            $("body .header-rms .header-wrap nav .login-box").removeClass('rotate-arrow');
            $("body .header-rms .header-wrap nav .login-box").find("> .profiles-block").stop(true, true).slideUp();
            $("body .header-rms .header-wrap nav ul li").removeClass("no-hover");
            $("body .header-rms .header-wrap nav .login-box").removeClass("no-hover");
        });


        //window url code//
        var pathName = window.location.pathname;
        var currentNode = pathName.replace("/node/", "");
        $("body .header-rms .header-wrap nav ul li > a").each(function () {
            var data = $(this).attr('href');
            // if (pathName == data) after node will convert into pathnamesw
            if (currentNode == data) {
                $(this).parents('ul').find('li').removeClass('active');
                $(this).parent().addClass('active');
            }
        });
    });
    $(window).on("load resize", function (e) {
        $("body .header-rms .header-wrap nav ul li > a").click(function (e) {
            var windowWidth = $(window).width();
            if (windowWidth < 991) {
                e.preventDefault();
            }
        });

    });
    $(window).on("load resize", function (e) {
        $("body .new-header.header-rms").css({"opacity": 1});
        var headerWidht = $("body .header-rms .wrapper-white").width();
        var winWidth = $(window).width();
        var extra_space1;
        if (winWidth >= 992) {
            extra_space1 = 64;
        }
        else {
            extra_space1 = 0;
        }

        if ((window.matchMedia("(min-width: 992px)").matches)) {
            $("body .header-rms .header-wrap nav .sub-menu").css({"width": headerWidht + extra_space1  +"px"});
        }
        else {
            $("body .header-rms .header-wrap nav .sub-menu").css({"width": "auto"});
        }
    });



})(jQuery);