(function ($) {
  $(function () {
    var itemLength = $("body .image-with-slider .award-top-carousel .slider-outer .items").length;
    // Run code
    $("body .image-with-slider .award-top-carousel .slider-outer").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      speed: 700,
      arrows: true,
      draggable: true,
      appendDots: $('body .image-with-slider .button-wrap .dots-outer'),
      prevArrow: $('body .image-with-slider .button-wrap .pre-btn'),
      nextArrow: $('body .image-with-slider .button-wrap .next-btn'),
    });


    if ((itemLength == 1)) {
      $("body .image-with-slider .award-top-carousel .button-wrap").css({"display": "none"});
    }

  });
  $(window).on("load resize", function (e) {
    $(".slider-outer").css({"opacity": 1});
  });

  //new condition
  if ($('body .image-with-text-slider ').length > 0) {
    var window_width = $(window).width();
    if (window_width <= 768) {
      $(document).on("scroll");
      $("body .image-with-slider .button-wrap .dots-outer, body .image-with-slider .button-wrap .pre-btn, body .image-with-slider .button-wrap .next-btn ").click(function () {
        $('html, body').animate({
          scrollTop: $("body .image-with-slider .award-top-carousel .slider-outer").offset().top - 80
        }, 2000);
      });
    }
  }

})(jQuery);
