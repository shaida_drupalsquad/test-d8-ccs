(function ($) {

  $(function (e) {
    var slider_item = 3;
    var itemLength = $("body .col-three-slider .column-wrapper .slider .item").length;

    $("body .col-three-slider .column-wrapper .slider").slick({
      infinite: true,
      slidesToShow: slider_item,
      slidesToScroll: 1,
      dots: true,
      arrows: true,
      draggable: true,
      appendDots: $('body .col-three-slider .column-wrapper .dots-outer'),
      prevArrow: $('body .col-three-slider .column-wrapper .pre-btn'),
      nextArrow: $('body .col-three-slider .column-wrapper .next-btn'),
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            draggable: true
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            draggable: true
          }
        },
        {
          breakpoint: 595,
          settings: {
            slidesToShow: 1,
            draggable: true
          }
        }
      ]

    });

    if (itemLength == 2) {
      $('body .col-three-slider').addClass('two-slider-item');
      if ($(".col-three-slider").hasClass("two-slider-item")) {
        slider_item = 2;
      }
    } else if (itemLength == 1) {
      $('body .col-three-slider').addClass('one-slider-item');
      if ($(".col-three-slider").hasClass("one-slider-item")) {
        slider_item = 1;
      }
    } else {
      $('body .col-three-slider').removeClass('no-slider-item');

    }
  });
  $(window).on("load resize", function () {
    $('body .col-three-slider .column-wrapper .slider .card-box').matchHeight();
  });

  $(window).on("load", function () {
    if($('.ua-firefox').length > 0) {
     setTimeout(function () {
       $(".col-three-slider .column-wrapper .slider").css({"opacity": 1});
       $(".col-three-slider .column-wrapper .button-wrap").css({"opacity": 1});
     },1000);
    } else {
      $(".col-three-slider .column-wrapper .slider").css({"opacity": 1});
      $(".col-three-slider .column-wrapper .button-wrap").css({"opacity": 1});
    }
  });


  $(document).ready(function () {

    limit_lines('.col-three-slider .column-wrapper .card-box .content-box h5', 3);
    limit_lines('.col-three-slider .column-wrapper .card-box .content-box p', 3);



  });
})(jQuery);