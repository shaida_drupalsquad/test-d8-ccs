//video overlay
(function ($) {
  var i = 0;
  $(document).ready(function () {
    function get_popup_video() {
      var video_play = $('.video-play').length;
      if(video_play) {
        $('.video-play').each(function () {
          var url = $(this).attr('data-video');

          // check if watch is there (for youtube)
          var check_watch = url.indexOf('watch');
          var check_youtube = url.indexOf('youtube.com');
          if(check_youtube !== -1 && check_watch !== -1) {
            var arrange_url = url.split('?');

            // Grab video ID
            var video_id = arrange_url[1].replace(/\&.*/,'').split('=');
            $(this).attr('data-video', 'https://www.youtube.com/embed/' + video_id[1]);
          }
        });
      }
    }
    get_popup_video();

    $(document).on('click', '.video-play', function (e) {
      e.preventDefault();
      var linkval = $(this).attr('data-video');
      var thisText = $(this).find("p").text();

      $(" .video-overlay").addClass("active");
      $('body').css("overflow", "hidden");
      $('.video-overlay.active iframe').attr("src", linkval + "?rel=0");
      $('.video-overlay').css({'display': '1', 'visibility': 'visible'});
      //$(".video-overlay.active .video-container p").text(thisText);
      $(".video-overlay.active .video-container h6").text(thisText);
    });

    $(".video-overlay .closeVideo, .video-overlay").click(function () {
      $('.video-overlay').removeClass('active');
      $('.video-overlay').css({'display': '0', 'visibility': 'hidden'});
      $('body').css("overflow", "inherit");
      $('.video-overlay iframe').attr("src", "");
      $(".video-overlay.active .video-container p").text("");
    });
  });

//thank you overlay
  $(".thank-you-overlay .column-wrapper .close-btn").click(function () {
    $('.thank-you-overlay').removeClass('active');
  });



  $(window).on('load',function(){

    $(document).on('click','.Infographics',function(e){
      e.preventDefault();
    })
    $( document ).ajaxComplete(function() {
      init();
    });

  });


  function init(){
    $("a[rel^='prettyPhoto']").prettyPhoto({
      markup: '<div class="pp_pic_holder">                         <div class="ppt">&nbsp;</div>                         <div class="pp_top info-top">                             <div class="pp_left"></div>                             <div class="pp_middle"></div>                             <div class="pp_right"></div>                         </div>                         <div class="pp_content_container info-pp-container">                             <div class="pp_left">                             <div class="pp_right">                                 <div class="pp_content">                                     <div class="pp_loaderIcon"></div>                                     <div class="pp_fade infographics-img">                                         <a href="#" class="pp_expand" title="Expand the image">Expand</a>                                         <a class="pp_close" href="#">Close</a>                                         <div class="pp_details">                                             <div class="pp_nav">                                                 <a href="#" class="pp_arrow_previous">Previous</a>                                                 <p class="currentTextHolder">0/0</p>                                                 <a href="#" class="pp_arrow_next">Next</a>                                             </div>                                             <p class="pp_description"></p>                                             {pp_social}                                         </div>                                         <div class="pp_hoverContainer">                                             <a class="pp_next" href="#">next</a>                                             <a class="pp_previous" href="#">previous</a>                                         </div>                                         <div id="pp_full_res"></div>                                     </div>                                 </div>                             </div>                             </div>                         </div>                         <div class="pp_bottom info-pp-bottom">                             <div class="pp_left"></div>                             <div class="pp_middle"></div>                             <div class="pp_right"></div>                         </div>                     </div>                     <div class="pp_overlay"></div>',
      animation_speed: "fast",
      slideshow: 5e3,
      autoplay_slideshow: !1,
      opacity: .8,
      show_title: !1,
      allow_resize: !0,
      default_width: 500,
      default_height: 344,
      counter_separator_label: "/",
      theme: "pp_default",
      horizontal_padding: 20,
      hideflash: !1,
      wmode: "opaque",
      autoplay: !0,
      modal: !1,
      deeplinking: !1,
      overlay_gallery: !0,
      keyboard_shortcuts: !1,
      changepicturecallback: function() {
          $("html, body").css("overflow", "hidden"), $("iframe").attr("allowFullScreen", "");
          var t = (100 - 100 * $(".pp_pic_holder.pp_default.infog").width() / $("html body").innerWidth()) / 2;
          $(".pp_pic_holder.pp_default.infog").css({
              left: t + "%",
              top: ( $(window).height() - $(".pp_pic_holder.pp_default.infog").height() ) / 2 + 10  + "px"
          })
      },
      callback: function() {
          $("html, body").css("overflow", "auto")
      },
      ie6_fallback: !0,
      social_tools: ""
  }).click(function() {
      $(".pp_pic_holder").addClass("infog")
  })
  }
  $(document).ready(function(){
        init();
    });
})(jQuery);



