(function ($) {
    $(function () {
        $("body .news-slider .slider-block").slick({
            centerPadding: '60px',
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            draggable: true,
            appendDots: $('body .news-slider .button-wrap .dots-outer'),
            prevArrow: $('body .news-slider .button-wrap .pre-btn'),
            nextArrow: $('body .news-slider .button-wrap .next-btn'),
            responsive: [
                {
                    breakpoint: 811,
                    settings: {
                        adaptiveHeight: true
                    }
                },
            ]
        });

        $(".overview .col-grid .content-box," +
            ".logo-grid .container-box .row-wrap .content-box").on("click", function (event) {
            event.stopPropagation();
            $(".logo-grid .container-box .row-wrap .content-box").removeClass("active");
            $(".overview .col-grid .content-box").removeClass("active");
            $(this).addClass("active");
            $(this).addClass("active");
        });


        $(document).on("click", function () {
            $(".logo-grid .container-box .row-wrap .content-box").removeClass("active");
            $(".overview .col-grid .content-box").removeClass("active");
        });

        // $(window).on("load resize", function (e) {
        var itemsLength = $("body .news-slider .slider-block .item").length;
        $("body .news-slider .slider-block").css({"opacity": 1});
        if (itemsLength == 1) {
            $("body .news-slider .button-wrap").css({"display": "none"});
            $("body .news-slider .slider-block .item .resources-block .wrapper-box").addClass("one-slide");
        }


      if ($('body .news-slider ').length > 0) {

        var window_width = $(window).width();
        if (window_width <= 768) {
          $(document).on("scroll");
          $("body .news-slider .button-wrap .dots-outer, body .news-slider .button-wrap .pre-btn, body .news-slider .button-wrap .next-btn ").click(function () {
            $('html, body').animate({
              scrollTop: $("body .news-slider .slider-block").offset().top - 80
            }, 2000);
          });

        }
      };
    });

    $(document).ready(function() {
        var urlHash = window.location.hash;
        if (urlHash) {
            var urlID = urlHash.substr(1);
            var newsItems = $('section.news-slider .item[aria-describedby]');
            var wrapperFound = false;
            var occure = 0;
            var newsInter = 0;
            if (newsItems.length > 0) {
                $(newsItems).each(function (index) {
                    var mediaDataID = $(this).children('.media-block').attr('data-id');
                    var wrapperdataID = $(this).find('.wrapper-box');
                    if (mediaDataID == urlID) {
                        occure = newsInter;
                        wrapperFound = true;
                        return false;
                    } else if (!wrapperFound) {
                        $(wrapperdataID).each(function (innerIndex) {
                            var wrapperID = $(this).attr('data-id');
                            if (wrapperID == urlID) {
                                occure = newsInter;
                                wrapperFound = true;
                                return false;
                            }
                        });
                    }
                    newsInter++;
                });
                if (wrapperFound) {
                    setTimeout(function () {
                        $("body .news-slider .slider-block").slick("slickGoTo", occure);
                    }, 0);
                    setTimeout(function () {
                        $('html, body').animate({
                            scrollTop: $(".news-slider div[data-id='"+urlID+"']").offset().top - 80
                        }, 500);
                    }, 10);
                }
            }

            // Image with text
            var imageWithText = $('section.image-with-text');
            if(imageWithText.length > 0) {
                $(imageWithText).each(function (index) {
                    var imgTextData = $(this).attr('data-id');
                    if (imgTextData == urlID) {
                        $('html, body').animate({
                            scrollTop: $(this).offset().top - 80
                        }, 500);
                        return false;
                    }
                });
            }
        }
    });

})(jQuery);