(function ($) {
  $(function () {
    var itemLength = $("body .reference-carousel .col-wrapper .col-item").length;
    $("body .reference-carousel .col-wrapper ").slick({
      centerPadding: '60px',
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: true,
      draggable: true,
      appendDots: $('body .reference-carousel .button-wrap .dots-outer'),
      prevArrow: $('body .reference-carousel  .button-wrap .pre-btn'),
      nextArrow: $('body .reference-carousel .button-wrap .next-btn'),
    });

    if (itemLength < 2) {
      $("body .reference-carousel .button-wrap").css({"display": "none"});
    }


  });
  $(window).on("load resize", function (e) {
    $("body .reference-carousel .col-wrapper").css({"opacity": 1});

  });

  //new condition

  if ($('body .reference-carousel  ').length > 0) {
    var window_width = $(window).width();
    if (window_width <= 768) {
      $(document).on("scroll");
      $("body .reference-carousel  .button-wrap .pre-btn, body .reference-carousel .button-wrap .next-btn, body .reference-carousel .button-wrap .dots-outer ").click(function () {
        $('html, body').animate({
          scrollTop: $("body .reference-carousel .col-wrapper").offset().top - 80
        }, 2000);
        console.log("b")
      });

    }
  }

})(jQuery);