(function ($) {
    $(function () {
        var itemLength = $("body .image-with-text-slider .award-top-carousel .slider-outer .items").length;
        // Run code
        $("body .image-with-text-slider .award-top-carousel .slider-outer").slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            speed: 700,
            arrows: true,
            draggable: true,
            appendDots: $('body .image-with-text-slider .button-wrap .dots-outer'),
            prevArrow: $('body .image-with-text-slider .button-wrap .pre-btn'),
            nextArrow: $('body .image-with-text-slider .button-wrap .next-btn'),
        });

        if ((itemLength == 1)) {
            $("body .image-with-text-slider .award-top-carousel .button-wrap").css({"display": "none"});
        }

        // URL redirection
        var urlHash = window.location.hash;
        if (urlHash) {
            var urlID = urlHash.substr(1);

            // Image with text
            var imgSliderItems = $('section.image-with-text-slider .items[aria-describedby]');
            var occure = 0;
            var newsInter = 0;
            var wrapperFound = false;
            if(imgSliderItems.length > 0) {
                $(imgSliderItems).each(function (index) {
                    var imgTextData = $(this).attr('data-id');
                    if (imgTextData == urlID) {
                        occure = newsInter;
                        wrapperFound = true;
                        return false;
                    }
                    newsInter++;
                });
                if (wrapperFound) {
                    setTimeout(function () {
                        $("body .image-with-text-slider .award-top-carousel .slider-outer").slick("slickGoTo", occure);
                    }, 0);
                    setTimeout(function () {
                        $('html, body').animate({
                            scrollTop: $(".image-with-text-slider").offset().top - 80
                        }, 500);
                    }, 10);
                }
            }
        }
    });
    $(window).on("load resize", function (e) {
        $(".slider-outer").css({"opacity": 1});
    });


  //new condition
  if ($('body .image-with-text-slider ').length > 0) {
    var window_width = $(window).width();
    if(window_width <= 768){
      $(document).on("scroll");
      $("body .image-with-text-slider .button-wrap .dots-outer, body .image-with-text-slider .button-wrap .pre-btn, body .image-with-text-slider .button-wrap .next-btn").click(function (){
        $('html, body').animate({
          scrollTop: $("body .image-with-text-slider").offset().top - 80
        }, 2000);
      });

    }
  }




})(jQuery);