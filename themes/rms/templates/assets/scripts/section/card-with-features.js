(function ($) {


    $(function () {
        $(window).on("load resize", function (e) {
            if ((window.matchMedia("(min-width: 1700px)").matches)) {
                var containerPos = $("body .container").offset().left;
                $("body .card-with-features .row-wrapper").css({"left": containerPos});
            }
            else {
                $("body .card-with-features .row-wrapper").css({"left": 0});
            }

          var winWidth = $(window).width();
          if (winWidth >= 992) {
            var white_box = $("body .card-with-features .slider-block").outerHeight();
            $("body .card-with-features .row-wrapper .overview-block").css({"height": white_box + 69});
          }else {
            $("body .card-with-features .row-wrapper .overview-block").css({"height":"auto"});
          }

        })
    });

})(jQuery);