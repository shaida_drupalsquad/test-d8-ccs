// (function ($) {
//   $(window).on('load', function () {
//     $('.year-slider').css('opacity', 1);
//     tilesWidth();
//     cards_bg();
//
//     $('.year-slider .content-box').matchHeight();
//     $('.year-slider .slider-wrapper').mCustomScrollbar({
//       axis: "x",
//       scrollType: "continuous",
//       autoDraggerLength: false,
//       scrollbarPosition: "outside",
//       advanced: {
//         autoExpandHorizontalScroll: 2
//       },
//     });
//
//     //scroller bg full width
//     $('.year-slider .mCSB_scrollTools_horizontal .mCSB_draggerRail').append('<div class="bg-line"></div>');
//     scrollerBg();
//   });
//
//   $(window).resize(function () {
//     tilesWidth();
//     cards_bg();
//     scrollerBg();
//   });
//
//   //setting width of the columns
//   function tilesWidth() {
//     var wrapper_width = $('body .year-slider .slider-wrapper').width();
//     var target = $('.year-slider .slider-wrapper .content-box');
//     var win_width = $(window).width();
//
//     if (win_width > 992) {
//       $(target).css({
//         'width': wrapper_width / 4 + 14
//       });
//     }
//     else if (win_width > 768) {
//       $(target).css({
//         'width': wrapper_width / 3 + 14
//       });
//     }
//     else if (win_width > 595) {
//       $(target).css({
//         'width': wrapper_width / 2 + 14
//       });
//     }
//     else if (win_width > 320) {
//       $(target).css({
//         'width': wrapper_width / 1 + 14
//       });
//     }
//   }
//
//   //card bg-line position
//   function cards_bg() {
//     var card_pos = $('body .year-slider .container').position().top;
//     var h3_height = $('body .year-slider .slider-wrapper .content-box .content h3').height() / 2 - 2;
//     var h3_pos = $('body .year-slider .slider-wrapper .content-box .content h3').position().top;
//     var bg_pos = card_pos + h3_height + h3_pos;
//     $('body .year-slider .bg-cards').css({
//       'top': bg_pos
//     });
//   }
//
//   //scroller bg-line position
//   function scrollerBg() {
//     var left_value = $('.year-slider .mCSB_scrollTools_horizontal .mCSB_draggerRail').offset().left;
//     $('.year-slider .mCSB_scrollTools_horizontal .mCSB_draggerRail .bg-line').css({
//       'width': $(window).width(),
//       'left': -left_value,
//     });
//   }
// })(jQuery);
//
