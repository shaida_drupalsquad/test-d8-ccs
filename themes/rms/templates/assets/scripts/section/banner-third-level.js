(function ($) {
    function setBannerheight() {
        if ($('body .banner-third-level').length > 0) {
            var winWidth = $(window).width();
            var val1, val2, section_height;
            val1 = $('body .banner-third-level .inner-text').height();
            val2 = $('body .banner-third-level .inner-text').offset().top;
            var extra_space1;
            if (winWidth >= 992) {
                extra_space1 = 50;
            }
            else if (winWidth >= 596) {
                extra_space1 = 70;
            }
            else {
                extra_space1 = 100;
            }
            section_height = val1 + val2 + extra_space1;
            if ($("body .banner-third-level").hasClass("banner-with-out-image")) {
                $('.banner-third-level .bg-img').css({"height": section_height + "px"});

            } else if ($("body .banner-third-level").hasClass("banner-blog-detail")) {
                $('.banner-third-level .bg-img').css({"height": section_height + "px"});
            }
        }
    }
    $(document).ready(function () {
        setBannerheight();
        $("body .banner-third-level").css({"opacity": 0});
    });
    $(window).on('load resize', function () {
        setBannerheight();
        $("body .banner-third-level").css({"opacity": 1});
    });


})(jQuery);