(function($){
    if($('body').hasClass('node-1905')) {
        var xhttp = new XMLHttpRequest();
        var result = '';
        var loadmore = 6;
        var intialstate = 0;
        var str1 = 'all',
            str2 = 'all',
            str3 = 'all',
            str4 = 'all';
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                result = JSON.parse(this.responseText);
                var cla = 'Region',
                    cla1 = 'Catastrophe';

                var regions = result.regions;
                loop(regions, cla);
                var catastrophes = result.catastrophes;
                loop(catastrophes, cla1);          

            }
        };

        xhttp.open("GET", "/sites/default/files/json/event-response/owl.json", true);
        xhttp.send();

        function loop(regions, it) {
            for (var i = 0; i < regions.length; i++) {
                var convert = '';
                if (it === 'Month') {
                    convert = regions[i].categoryId;
                } else {
                    convert = regions[i].name.split(' ');
                    convert = convert.join('_');
                }
                $('.' + it).append('<option value=' + convert + '>' + regions[i].name + '</option>')
            }
        }

        var xhttp1 = new XMLHttpRequest();
        var result1 = [];
        xhttp1.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                result1 = JSON.parse(this.responseText);
                var res = result1.results;
                loadmore = 6;
                intialstate = 0;
                gridLoop(res, intialstate, loadmore);
            }
        };

        xhttp1.open("GET", "/sites/default/files/json/event-response/event.json", true);
        xhttp1.send();
         
        function gridLoop(res, intialstate, loadmore) {
            $('.event-response-grid .column-wrapper .cover-two-col .card-grid .grid > div').remove();
            if (res.length > 0) {
                res.slice(intialstate, loadmore).map(function (item) {
                    var date = item.modified.split("T");
                    $('.event-response-grid .column-wrapper .cover-two-col .card-grid .grid').append('<div class="event-post">' +
                        '<h5>' + item.title + '</h5>' +
                        '<span class="date">Updated ' + date[0] + '</span>' +
                        '<div class="media-content">' +
                        '<div class="img-box">' +
                        '<img src="https://support.rms.com' + item.eventImage + '" alt="map image">' +
                        '</div>' +
                        '<div class="content-text">' +
                        '<div class="small-content">' +
                        '<span>Loss Drivers</span>' +
                        '<div class="icon-text">' +
                        '<div class=icon-box id='+ item.perilLossDrivers+'>' +
                        '</div>' +
                        '<p>' + item.peril + '</p>' +
                        '</div>' +
                        '</div>' +
                        '<div class="gray-content">' +
                        '<ul class="content-inner">' +
                        '<li><p>' + item.headlineStatistic1Small + '</p></li>' +
                        '<li><p>' + item.headlineStatistic3Small + '</p></li>' +
                        '<li><p>' + item.headlineStatistic2Small + '</p></li>' +
                        '</ul>' +
                        '<ul class="content-inner">' +
                        '<li><h5>' + item.headlineStatistic1Large + '</h5></li>' +
                        '<li><h5>' + item.headlineStatistic3Large + '</h5></li>' +
                        '<li><h5>' + item.headlineStatistic2Large + '</h5></li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="event-summary">' +
                        '<h6>Event Summary</h6>' +
                        '<p>' + item.rssFeedDescription + '</p>'+
                        '<p>RMS customers can '+
                        '<a href="https://support.rms.com/group/rms/event-report?eventId='+item.eventId+'" target="_blank"> login to Owl</a> to access the full report.</p>'+
                        '<span class="read-more-btn">Read More</span>' +
                        '</div>' +
                        '</div>')
                });
            } else {
                $('.event-response-grid .column-wrapper .cover-two-col .card-grid .grid').append('<div class="event-post">' +
                    '<h4>No Result Found</h4>' +
                    '</div>');
            }
            if (res.length <= loadmore) {
                $('body .event-response-grid .button-box a.event-load').css('display', 'none');
            } else {
                $('body .event-response-grid .button-box a.event-load').css('display', 'inline-block');
            }
        }

        var filter = function (re) {
            let array = [];
            var array1 = [];
            var array2 = [];
            var array3 = [];
            if (str1 === 'all' && str2 === 'all' && str3 === 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    array.push(re[i])
                }
            }
            else if (str1 !== 'all' && str2 === 'all' && str3 === 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array.push(re[i])
                    }
                }
            } else if (str1 === 'all' && str2 !== 'all' && str3 === 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].region === str2) {
                        array.push(re[i])
                    }
                }
            } else if (str1 === 'all' && str2 === 'all' && str3 !== 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array.push(re[i])
                    }
                }
            } else if (str1 === 'all' && str2 === 'all' && str3 === 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array.push(re[i])
                    }
                }
            } else if (str1 !== 'all' && str2 !== 'all' && str3 === 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array1.push(re[i])

                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i].region === str2) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 !== 'all' && str2 == 'all' && str3 !== 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array1.push(re[i])
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 !== 'all' && str2 == 'all' && str3 == 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array1.push(re[i])
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 === 'all' && str2 !== 'all' && str3 !== 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].region === str2) {
                        array1.push(re[i])
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 === 'all' && str2 !== 'all' && str3 === 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].region === str2) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 === 'all' && str2 === 'all' && str3 !== 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array.length; i++) {
                    if (array[i].region === str3) {
                        var convert = array[i].eventDate.split('-');
                        if (convert[1] == str4) {
                            array1.push(array[i])
                        }
                    }
                }
            } else if (str1 !== 'all' && str2 === 'all' && str3 !== 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array2.push(array[i]);
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    if (array2[i].peril === str1) {
                        array.push(array2[i])
                    }
                }
            } else if (str1 === 'all' && str2 !== 'all' && str3 !== 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str4) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array2.push(array[i]);
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    if (array2[i].region === str2) {
                        array.push(array2[i])
                    }
                }
            } else if (str1 !== 'all' && str2 !== 'all' && str3 !== 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i].peril === str1) {
                        array2.push(re[i])
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    if (array2[i].region === str2) {
                        array.push(array2[i])
                    }
                }
            } else if (str1 !== 'all' && str2 !== 'all' && str3 === 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i].peril === str1) {
                        array2.push(re[i])
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    if (array2[i].region === str2) {
                        array.push(array2[i])
                    }
                }
            } else if (str1 !== 'all' && str2 !== 'all' && str3 !== 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i].region === str2) {
                        array2.push(re[i]);
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    var convert = array2[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array3.push(array2[i])
                    }
                }
                for (var i = 0; i < array3.length; i++) {
                    var convert = array3[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array.push(array3[i])
                    }
                }
            }
            gridLoop(array, intialstate, loadmore);
        }
        var loadmoreFunc = function (rs, intialstate, loadmore) {
            if (loadmore < rs.length) {
                filter(rs, intialstate, loadmore);
            } else {
                filter(rs, intialstate, loadmore);
            }
        }
        $(document).on('click', 'body .event-response-grid .button-box a.event-load', function (e) {
            e.preventDefault();
            var loadLength = result1.results;
            loadmore = loadmore + 6;
            intialstate = 0;
            loadmoreFunc.call(this, loadLength, intialstate, loadmore);
        });

        $(document).on('change', '.Catastrophe', function () {
            var val = $(this).val();
            var convert = val.split('_'),
                res = result1.results;
            str1 = convert.join(' ');
            loadmore = 6;
            intialstate = 0;
            filter(res, intialstate, loadmore);
            if (str1 === 'all') {
                removeUrl('Catastrophe')
            } else {
                urlUpdate('Catastrophe', val);
            }
        });

        $(document).on('change', '.Region', function () {
            var val = $(this).val();
            var convert = val.split('_'),
                res = result1.results;
            str2 = convert.join(' ');
            loadmore = 6;
            intialstate = 0;
            filter(res, intialstate, loadmore);
            if (str2 === 'all') {
                removeUrl('Region')
            } else {
                urlUpdate('Region', val);
            }
        });

        $(window).on('load', function () {
            var href = window.location.href, array = [];
            href = href.split('?');
            href.shift();
            var newhref = href.join(" ");
            newhref = newhref.split("&");
            for (var i = 0; i < newhref.length; i++) {
                array.push(newhref[i].toString().split('='));
            }
            for (var i = 0; i < array.length; i++) {
                var item = array[i];
                if (item[0] === "Catastrophe") {
                    var convert = item[1].split('_');
                    str1 = convert.join(' ');
                    $('.Catastrophe').val(item[1]).trigger("change");
                }
                if (item[0] === "Region") {
                    var convert = item[1].split('_');
                    str2 = convert.join(' ');
                    $('.Region').val(item[1]).trigger("change");
                }
            }
        });

        function removeUrl(str) {
            var href = window.location.href;
            var href1 = href.split("?");
            var newitem = href1[0].toString();
            href1.shift();
            var newhref = href1.join(" ");
            newhref = newhref.split("&");
            if (href.includes(str)) {
                for (var item = 0; item < newhref.length; item++) {
                    if (newhref[item].toString().includes(str)) {
                        newhref.splice(item, 1)
                    }
                }
                newhref = newhref.join('&');
                if (newhref === '') {
                    href1 = newitem + '' + newhref;
                }
                else {
                    href1 = newitem + '?' + newhref;
                }
                history.pushState({}, null, href1);
            }
        }

        function urlUpdate(str, value) {
            var href = window.location.href;
            var href1 = href.split("?");
            var newitem = href1[0].toString();
            href1.shift();
            var newhref = href1.join(" ");
            newhref = newhref.split("&");
            if (href.includes(str)) {
                for (var i = 0; i < newhref.length; i++) {
                    if (newhref[i].toString().includes(str)) {
                        newhref.splice(i, 1)
                        newhref.push(str + '=' + value)
                    }
                }
                newhref = newhref.join('&')
                href1 = newitem + '?' + newhref;
                history.pushState({}, null, href1);
            }
            else {
                if (href1.length < 1) {
                    history.pushState({}, null, href + '?' + str + '=' + value);
                } else {
                    history.pushState({}, null, href + '&' + str + '=' + value);
                }
            }
        }
    }
})(jQuery);