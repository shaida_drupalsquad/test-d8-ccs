(function ($) {
  $(document).ready(function () {
    $('.infographics #info-popup').click(function (e) {
      e.preventDefault();
      $('.infographic-popup').addClass('active');
    });

    $('.infographic-popup .content-box .close-popup').click(function () {
      $('.infographic-popup').removeClass('active');
    });


    //copy iframe


    $('#copytext').click(function () {
      var copyText = $('.infographic-popup #copylink');
      $(copyText).select();
      document.execCommand('copy');
    });
  });
})(jQuery);

