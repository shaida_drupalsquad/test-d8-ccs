(function($){
    $(document).ready(function(){

    $('.tabs-with-detail .column-wrapper ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        var selected_text = $(this).text();

        $('.tabs-with-detail ul.tabs li').removeClass('current');
        $('.tabs-with-detail .tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');

        $(this).text(selected_text);

        $(".tabs-with-detail .column-wrapper .open").text(selected_text);
    });


    $(".tabs-with-detail .column-wrapper .open").click(function () {
        var winWidth = $(window).width();
        if (winWidth < 768) {
            $(".tabs-with-detail .column-wrapper .open").toggleClass("active"),
            $(".tabs-with-detail .column-wrapper ul.tabs").slideToggle();
                }
    });

    $(".tabs-with-detail .column-wrapper ul.tabs li").click(function () {
        var winWidth = $(window).width();
        if (winWidth < 768) {
            $(".tabs-with-detail .column-wrapper .open").removeClass('active'),
            $(".tabs-with-detail .column-wrapper ul.tabs").slideUp();

        }
    });

});

})(jQuery);
    