(function ($) {
    $(function () {

        var readtxtLength = $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .more-text").length;
        console.log(readtxtLength + "new change ");
        if (readtxtLength >= 1) {
            $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn").show();
        }
        else {
            $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn").hide();
        }

        $(document).on("click", "body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn", function (e) {
            // e.stopPropagation();


            $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn").show();
            if ($(this).hasClass('rotate')) {
                $(this).removeClass('rotate');
                $(this).parent().find(".more-text").stop(true, true).slideUp(500);
                $(this).text("Read More");
            }
            else {
                // Hide the other tabs
                $('body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn').removeClass('rotate');
                $('body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn').text("Read More");
                $(this).addClass('rotate');
                $(this).text("Read Less");
                $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn").parent().find(".more-text").stop(true, true).slideUp(500);
                $(this).parent().find(".more-text").stop(true, true).slideDown(500);

            }


        });

        $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post").slice(0, 4).show();


    });


})(jQuery);