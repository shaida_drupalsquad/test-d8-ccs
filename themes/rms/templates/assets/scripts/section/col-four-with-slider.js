(function ($) {
    $(function () {
        $(".col-four-with-slider .slider-block .slide-outer").slick2({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            appendDots: $(
                "body .col-four-with-slider .row-wrapper .slider-block .button-wrap .dots-outer"
            ),
            prevArrow: $(
                "body .col-four-with-slider .row-wrapper .slider-block .button-wrap .pre-btn"
            ),
            nextArrow: $(
                "body .col-four-with-slider .row-wrapper .slider-block .button-wrap .next-btn"
            ),
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });

        var itemLength = $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").length;
        if (itemLength <= 3) {
            $(".col-four-with-slider .slider-block .slide-outer").slick('unslick');
            $(".col-four-with-slider .slider-block .slide-outer").addClass('row');
            $(".col-four-with-slider .slider-block .slide-outer .item-box").addClass('un-slick');
            $(".col-four-with-slider .slider-block .slide-outer .item-box").removeClass('slick-init');
            $("body .col-four-with-slider .row-wrapper .slider-block .button-wrap").css({opacity: 0});
            $("body .col-four-with-slider .row-wrapper .overview-block").addClass("extra-space");
        }
        else {
            $(".col-four-with-slider .slider-block .slide-outer .item-box").addClass('slick-init');
            $("body .col-four-with-slider .row-wrapper .overview-block").removeClass("extra-space");
        }
        if (itemLength == 3) {
            $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").addClass("col-three");
        }
        if (itemLength == 2) {
            $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").addClass("col-two");
        }
        if (itemLength == 1) {
            $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").addClass("col");
        }

        var arrayItem = $(
            "body .col-four-with-slider .slider-block .slide-outer .item-box"
        );
        var outerWidth = $(arrayItem).width();
        var slickOuter =
            $(arrayItem)
                .parents()
                .find(".slick-slider .slick-track")
                .width() * 2;
        var newWidth = outerWidth + slickOuter;

        $(window).on("load resize", function (e) {
            $("body .col-four-with-slider .slider-block .slide-outer").css({
                opacity: 1
            });
            if (window.matchMedia("(min-width: 991px)").matches) {
                $(arrayItem).on("mouseover", function () {
                    $(
                        "body .col-four-with-slider .slider-block .slide-outer .slick-list .slick-track"
                    ).css({width: newWidth * 3});
                });

                $(arrayItem).on("mouseout", function () {
                    $(
                        "body .col-four-with-slider .slider-block .slide-outer .slick-list .slick-track"
                    ).css({width: slickOuter});
                });
            }
            if (window.matchMedia("(min-width: 1700px)").matches) {
                var containerPos = $("body .container").offset().left;
                $("body .col-four-with-slider .row-wrapper").css({
                    left: containerPos
                });
            } else {
                $("body .col-four-with-slider .row-wrapper").css({left: 0});
            }
        });


        $(window).on("load resize", function () {
            $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").each(function () {
                var hoverState = $(this).find(".hover-state").length;
                if (hoverState < 1) {
                    $(this).addClass("nohover");
                    $(this).removeClass("slick-init");
                }
            });
        });

        //new condition
        if ($('body .col-four-with-slider ').length > 0) {
            var window_width = $(window).width();
            if (window_width <= 768) {
                $(document).on("scroll");
                $("body .col-four-with-slider .row-wrapper .slider-block .button-wrap .pre-btn, body .col-four-with-slider .row-wrapper .slider-block .button-wrap .next-btn, body .col-four-with-slider .row-wrapper .slider-block .button-wrap .dots-outer ").click(function () {
                    $('html, body').animate({
                        scrollTop: $("body .col-four-with-slider .row-wrapper .slider-block").offset().top - 90
                    }, 2000);
                });
            }
        }


    });
})(jQuery);
