(function ($) {
    $(function () {
        $(".hero-slider .separator .slider-outer").slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            draggable: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                }
            ]
        });

        $(window).on("load resize", function (e) {
            $("body .hero-slider .slider-outer").css({"opacity": 1});
        });
    });
})(jQuery);