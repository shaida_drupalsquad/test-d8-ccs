(function($){
    if($('body').hasClass('node-2083')){
    var tmp1 = [];

    $(window).on('load',function(){
      var href = window.location.href, array=[];
      href= href.split('?');
      href.shift();
      var newhref= href.join(" ");
      newhref= newhref.split("&");
      for(var i=0; i< newhref.length;i++){
        array.push(newhref[i].toString().split('='));
      }

      for(var i=0;i<array.length;i++) {
        var item = array[i];
        if (item[0] === "search") {
            var it = item[1].split('_');
                it = it.join(' ');
                $(".blog-listing .search input").val('it');
            $('#views-exposed-form-blog-block-5 .form-item-keys input').val(it);
        }
      }
      setTimeout(function(){
        $('#views-exposed-form-blog-block-5 .form-submit').trigger('click');
      },300)           
  });

  $(document).on('change','.blog-listing .select-plugin .Month',function(){
    val = $(this).val();
    $('#views-exposed-form-blog-block-5 .form-item-sort-order select').val(val.toUpperCase());
    setTimeout(function(){
        $('#views-exposed-form-blog-block-5 .form-submit').trigger('click');
      },300)
});
$(".blog-listing .search form").on("submit", function (e) {
    e.preventDefault();
});

$(".blog-listing .search input").on("keyup", function (e) {
    // Number 13 is the "Enter" key on the keyboard
    var text = $(this).val();
    if(e.which == 13) {
        e.preventDefault();
        if (text.length > 0)
        urlUpdate('search', text.replace(/\s+/g,"_"));
      else
        removeUrl('search');
        $('#views-exposed-form-blog-block-5 .form-item-keys input').val(text);
        setTimeout(function(){
            $('#views-exposed-form-blog-block-5 .form-submit').trigger('click');
        },300);
    }
  });

    function removeUrl(str) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var item = 0; item < newhref.length; item++) {
            if (newhref[item].toString().includes(str)) {
              newhref.splice(item, 1)
            }
          }
          newhref = newhref.join('&');
          if (newhref === '') {
            href1 = newitem + '' + newhref;
          }
          else {
            href1 = newitem + '?' + newhref;
          }
          history.pushState({}, null, href1);
        }
      }

      function urlUpdate(str, value) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var i = 0; i < newhref.length; i++) {
            if (newhref[i].toString().includes(str)) {
              newhref.splice(i, 1)
              newhref.push(str + '=' + value)
            }
          }
          newhref = newhref.join('&')
          href1 = newitem + '?' + newhref;
          history.pushState({}, null, href1);
        }
        else {
          if (href1.length < 1) {
            history.pushState({}, null, href + '?' + str + '=' + value);
          } else {
            history.pushState({}, null, href + '&' + str + '=' + value);
          }
        }
      }
    }
})(jQuery);        