(function ($) {

    $(function () {

        var titleMain = $("body .leadership-slider .slider-block .slider-outer");
        var cycle = 1;
        if (titleMain.length) {
            titleMain.slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: false,
                dots: true,
                arrows: true,
                speed: 800,
                autoplaySpeed: 800,
                appendDots: $('body .leadership-slider .slider-block .button-wrap .dots-outer'),
                prevArrow: $('body .leadership-slider .slider-block .button-wrap .pre-btn'),
                nextArrow: $('body .leadership-slider .slider-block .button-wrap .next-btn'),
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                            draggable: true,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            draggable: true,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 595,
                        settings: {
                            slidesToShow: 1,
                            draggable: true,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            draggable: true,
                            infinite: true
                        }
                    }
                ]
            });


        }

        var slideLength = $("body .leadership-slider .slider-block .slider-outer .card-item").length;

        if (slideLength <= 4) {
            titleMain.slick('unslick');
            $("body .leadership-slider .slider-block .button-wrap").css({"display": "none"});
            $("body .leadership-slider .slider-block .slider-outer .card-item .content-wrap .footer-box").css({"max-width": "90%"});
        }

        if (slideLength == 4) {
            $("body .leadership-slider .slider-block .slider-outer .card-item").addClass("col-four");
        }
        if (slideLength == 3) {
            $("body .leadership-slider .slider-block .slider-outer .card-item").addClass("col-three");
        }
        if (slideLength == 2) {
            $("body .leadership-slider .slider-block .slider-outer .card-item").addClass("col-two");
        }
        if (slideLength == 1) {
            $("body .leadership-slider .slider-block .slider-outer .card-item").addClass("col");
        }


        $(window).on("load resize", function () {
            $(titleMain).css({"opacity": 1});
        });
    });


})(jQuery);
