(function ($) {
    $(window).on("load", function (e) {
      $('.insight-with-progress .content-box .column-wrapper .cover-item .we-items').matchHeight();
      $('.insight-with-progress .content-box .column-wrapper .cover-item .we-items .items').matchHeight();
    });

        $(function () {
            $(window).on("load", function () {
                $("body .insight-with-progress").each(function () {
                    var thisText = $(this).find(".content-box .column-wrapper .cover-item .text-after .top-heading h6").clone();
                    var thisTextsecond = $(this).find(".content-box .column-wrapper .cover-item .text-after .items h6").clone();

                    $(thisText).clone().appendTo('.content-box .column-wrapper .cover-item .hass-arrow .top-heading').wrap('<div class="hide" />');
                    $(thisTextsecond).clone().appendTo('.content-box .column-wrapper .cover-item .hass-arrow .items').wrap('<div class="hide" />');
                });
            });
        });


    })(jQuery);