
(function ($) {
    $(document).ready(function(){

    $('.info-with-tabs .column-wrapper .tab-div .icon-with-heading').mouseover(function(){

        var winWidth = $(window).width();

        if (winWidth >= 768) {
            var tab_selected = $(this).attr('data-tab');

            $('.info-with-tabs .column-wrapper .tab-div .icon-with-heading').removeClass('hide');
            $('.info-with-tabs .column-wrapper .tab-text').removeClass('hide');

            $(this).addClass('hide');
            $("#"+tab_selected).addClass('hide');
        }
    });

    $(window).on("load resize", function (e) {
        $(".info-with-tabs .column-wrapper .scroll-wrap .tab-div .icon-with-heading").css({"opacity": 1});
    });
});

$(document).ready(function(){
    $('.info-with-tabs .column-wrapper .tab-div .icon-with-heading').click(function(){

        var winWidth = $(window).width();

        if (winWidth <= 767) {
            var tab_selected = $(this).attr('data-tab');
            var selected_text = $(this).html();

            $('.info-with-tabs .column-wrapper .tab-div .icon-with-heading').removeClass('hide');
            $('.info-with-tabs .column-wrapper .tab-text').removeClass('hide');

            $(this).addClass('hide');
            $("#"+tab_selected).addClass('hide');

            $('.info-with-tabs .column-wrapper .open .cover').html(selected_text);

            $(".info-with-tabs .column-wrapper .open").removeClass('active')
            $(".info-with-tabs .column-wrapper .scroll-wrap").slideUp();

        }
    });
    $(".info-with-tabs .column-wrapper .open").click(function () {
        var winWidth = $(window).width();
        if (winWidth < 768) {
            $(".info-with-tabs .column-wrapper .open").toggleClass('active')
            $(".info-with-tabs .column-wrapper .scroll-wrap").slideToggle();
        }
    });

});

    function wrappBlock() {
        var winWidth = $(window).width();
        if (winWidth >= 768) {
            $(".info-with-tabs .column-wrapper .scroll-wrap").find('.icon-with-heading').each(function (index, sel) {

                //Wrap 2 div in 1 div
                var n;
                var itemLength = $(".info-with-tabs .column-wrapper .tab-div .icon-with-heading").length;
                for (var i = 0; i < itemLength; i += 7) {
                    if (i < itemLength) {
                        n = i + 2;
                    }

                    if (itemLength == 4) {
                        $(this).parents("section.info-with-tabs").addClass('info-four-items')
                    }
                    if (itemLength == 6) {
                        $(this).parents("section.info-with-tabs").addClass('solution-role')
                    } else if (itemLength == 7) {
                        $(this).parents("section.info-with-tabs").addClass('seven-items')
                    }
                    if (itemLength > 7) {
                        $(this).parents("section.info-with-tabs").addClass("seven-plus-items");
                        $(".seven-plus-items .column-wrapper .scroll-wrap .tab-div").addClass("slider");
                        $(".seven-plus-items .column-wrapper .scroll-wrap .tab-div .icon-with-heading").addClass("item");

                    } else {
                        $(this).parents("section.info-with-tabs").addClass('single-item')
                    }
                }

            });
        }
    }

    $(window).on("load", function () {
        // setPosition();
        wrappBlock();
        load_OnReady();
    });

    function load_OnReady() {
        var winWidth = $(window).width();
        if (winWidth >= 768) {
            $("body .seven-plus-items .slider").slick({
                infinite: true,
                slidesToShow: 7,
                slidesToScroll: 1,
                autoplay: false,
                dots: false,
                arrows: true,
                draggable: true,
            });
            $(window).on("load resize", function (e) {
                $("body .seven-plus-items .items").css({"opacity": 1});
            });
        }

    };
})(jQuery);