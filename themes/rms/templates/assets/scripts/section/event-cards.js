(function ($) {
    $(function () {
        $("body .events-cards .card-wrap .cards-item").slice(0, 6).show();
        $("body .events-cards .button-box .btn-white").on("click", function (e) {
            e.preventDefault();
            $("body .events-cards .card-wrap .cards-item:hidden").slice(0, 3).slideDown(500);

            if ($("body .events-cards .card-wrap .cards-item:hidden").length == 0) {
                $("body .events-cards .button-box .btn-white").fadeOut(500);
            }

            $('html,body').animate({
                scrollTop: $(this).offset().top - 500
            }, 500);


        });
    });


})(jQuery);