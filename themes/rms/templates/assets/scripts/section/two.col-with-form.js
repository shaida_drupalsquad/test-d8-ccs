(function ($) {
  $(function () {


    //scroll the form
    if ($('body .two-col-with-form').length > 0) {
      $('body .two-col-with-form .column-wrapper .form-box .column-wrap form').submit(function() {
        var window_width = $(window).width();
        var header_height =$("header").outerHeight();
        if (window_width >= 992) {
          $(document).on("scroll");
          $('html, body').animate({
            scrollTop: $("body .two-col-with-form .column-wrapper ").offset().top - header_height - 20
          }, 2000);
          // console.log("x");

        }
        else {
          $(document).on("scroll");
          $('html, body').animate({
            scrollTop: $("body .two-col-with-form .column-wrapper .form-box .column-wrap").offset().top - header_height - 20
          }, 2000);
        }
      });
     }


     //showing the thank you message after submission
    var resourcePageForm = $('.two-col-with-form .form-box form'),
      mktoFormId = resourcePageForm.attr('data-form-id');
    var newsletterPage = $('body.node-2970 section.two-col-with-form form');

    if(mktoFormId) {
      MktoForms2.loadForm("//app-sj10.marketo.com", "729-DJX-565", mktoFormId, function (form) {
        // Add an onSuccess handler
        form.onSuccess(function (values, followUpUrl) {
          // Get the form's jQuery element and hide it
          form.getFormElem().hide();
          $('.thank-you-overlay .button-box').hide();
          // Thank you message
          var thankyou_msg = $('.two-col-with-form input#thank-you-message').val();
          if(!thankyou_msg) {
            thankyou_msg = "Thank You";
          }
          $('.two-col-with-form .form-box .column-wrap').html('<h3>'+ thankyou_msg +'</h3>').addClass('form-submit');
          // Trigger event pixel for newsletter
          if(newsletterPage && newsletterPage.length) {
            try{
              __adroll.record_user({"adroll_segments": "c0e2ac60"})
            } catch(err) {}
          }
          // Return false to prevent the submission handler from taking the lead to the follow up url
          return false;
        });
      });
    }
  });
})(jQuery);
