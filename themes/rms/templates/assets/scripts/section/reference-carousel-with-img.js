(function ($) {
    $(function () {
        var itemLength = $("body .reference-carousel-with-img .col-wrapper .col-item").length;
        $("body .reference-carousel-with-img .col-wrapper ").slick({
            centerPadding: '60px',
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            draggable: true,
            appendDots: $('body .reference-carousel-with-img .button-wrap .dots-outer'),
            prevArrow: $('body .reference-carousel-with-img  .button-wrap .pre-btn'),
            nextArrow: $('body .reference-carousel-with-img .button-wrap .next-btn'),
            adaptiveHeight: true,
        });

        if (itemLength < 2) {
            $("body .reference-carousel-with-img .button-wrap").css({"display": "none"});
        }

      //new condition
      if ($('body .reference-carousel-with-img ').length > 0) {
        var window_width = $(window).width();
        if(window_width <= 768) {
          $(document).on("scroll");
          $("body .reference-carousel-with-img .button-wrap .dots-outer, body .reference-carousel-with-img  .button-wrap .pre-btn, body .reference-carousel-with-img .button-wrap .next-btn").click(function () {
            $('html, body').animate({
              scrollTop: $(" .reference-carousel-with-img .col-wrapper").offset().top - 80
            }, 2000);
          });
        }
      }

    });

    $(window).on("load resize", function (e) {
        $("body .reference-carousel-with-img .col-wrapper").css({"opacity": 1});

    });


})(jQuery);