(function ($) {
  $(function () {
    var resourcePageForm = $('.content-with-form .form-box form'),
      mktoFormId = resourcePageForm.attr('data-form-id');
      var newsletterPage = $('body.node-2692 section.content-with-form form');

    if(mktoFormId) {

      // Add mkto hidden val
      if(mktoFormId == 1594) {
        MktoForms2.whenReady(function(form) {
          form.vals({"last_form_fill_url":document.location});
        });
      }

      MktoForms2.loadForm("//app-sj10.marketo.com", "729-DJX-565", mktoFormId, function (form) {
        // Add an onSuccess handler
        form.onSuccess(function (values, followUpUrl) {
            if(mktoFormId == 1623) {
                window.location.href = 'http://rms.com/event-response/hwind/legacy-archive/storms';
            }

          // Get the form's jQuery element and hide it
          form.getFormElem().hide();

          $('.thank-you-overlay .button-box').hide();

          // Thank you message
          var thankyou_msg = $('.content-with-form input#thank-you-message').val();
          if(!thankyou_msg) {
              thankyou_msg = "Thank You";
          }


          $('.content-with-form .form-box .column-wrap').html('<p class="contact-thank-you">'+ thankyou_msg +'</p>').addClass('form-submit');

          //$('.thank-you-overlay').addClass('active');

          // Trigger event pixel for newsletter
          if(newsletterPage && newsletterPage.length) {
              try{
                  __adroll.record_user({"adroll_segments": "c0e2ac60"})
              } catch(err) {}
          }
          // Return false to prevent the submission handler from taking the lead to the follow up url
          return false;
        });
      });
    }
  });
})(jQuery);
