(function ($) {
    $(function () {

        var slide_to_show = 3;
        if ($("body .statistic-slider").hasClass("box-two")) {
            slide_to_show = 2;
        }

        $("body .statistic-slider .items").slick({
            infinite: true,
            slidesToShow: slide_to_show,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true,
            arrows: false,
            draggable: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                }
            ]

        });



        var itemLength = $("body .statistic-slider .items .item-box").length;
        if (itemLength < 4) {
            $("body .statistic-slider").addClass("no-dots");
        }
    });
    $(window).on("load", function (e) {
        $("body .statistic-slider .items").css({"opacity": 1});
    });
})(jQuery);

(function ($) {
    function wrappBlock(e) {
        $("body .statistic-slider.box-two").each(function (index, sel) {
            if ($('.statistic-slider.box-two').length > 0) {
                var itemLength = $(".statistic-slider.box-two .items .item-box").length;
                if (itemLength == 1) {
                    $(this).addClass('one-item');
                } else if (itemLength == 2) {
                    $(this).addClass('two-item');
                }
                if (itemLength >= 3) {
                    $(this).removeClass('one-item');
                    $(this).removeClass('two-item');
                } else {
                    $(this).addClass('slide');
                }
            }
        });
    }

    $(window).on("load", function () {
        wrappBlock();
    });
})(jQuery);
