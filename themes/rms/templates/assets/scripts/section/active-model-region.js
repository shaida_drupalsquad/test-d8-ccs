(function ($) {
  $(document).ready(function () {
    $('.sticky-nav').each(function () {
      if ($('.sticky-nav').length > 0) {
        var WindowHeight = jQuery(window).height();
        var load_element = 0;
        var nav = $('header').outerHeight();
        //position of element
        var scroll_position = jQuery('.sticky-nav').offset().top - nav;
        var screen_height = jQuery(window).height();
        var activation_offset = 0;
        var max_scroll_height = jQuery('body').height() + screen_height;

        var scroll_activation_point = scroll_position - (screen_height * activation_offset);

        jQuery(window).on('scroll', function (e) {

          var y_scroll_pos = window.pageYOffset;
          var element_in_view = y_scroll_pos > scroll_activation_point;
          var has_reached_bottom_of_page = max_scroll_height <= y_scroll_pos && !element_in_view;

          if (element_in_view || has_reached_bottom_of_page) {
            jQuery('.sticky-nav').addClass("change");

            jQuery('section.sticky-nav').next().addClass("style");
            // console.log('yash');
          } else {
            jQuery('.sticky-nav').removeClass("change");
            jQuery('section.sticky-nav').next().removeClass("style");
          }
        });
      }
    });
    $(window).on("load resize", function () {
      $("body .sticky-nav").each(function (index, sel) {
        if ($('.sticky-nav').length > 0) {
          var winWidth = $(window).width();
          if (winWidth <= 400) {
            var itemLength = $("body .sticky-nav .column-wrapper .by-category").length;
            for (var i = 0; i < itemLength; i += 5) {
              if (itemLength == 3) {
                $("body .sticky-nav .column-wrapper").css({"display": "block"});
                // console.log('y')
              }
              else {
                $("body .sticky-nav .column-wrapper").css({"display": "flex"});
                // console.log('f')
              }
            }
          } else if (winWidth <= 596) {
            var itemLength = $("body .sticky-nav .column-wrapper .by-category").length;
            for (var i = 0; i < itemLength; i += 5) {
              if (itemLength > 3) {
                $("body .sticky-nav .column-wrapper").css({"display": "block"});
                // console.log('p')
              }
              else {
                $("body .sticky-nav .column-wrapper").css({"display": "flex"});
                // console.log('Q');
              }
            }
          }
          else {
            $("body .sticky-nav .column-wrapper").css({"display": "flex"});
            // console.log("scroll")
          }
        }
      });
    });
  });


  $(document).ready(function () {
    if ($('.sticky-nav').length > 0) {
      $(document).on("scroll", onScroll);
      $(document).on('click', 'a[href^="#"]',function () {
        // e.preventDefault();
        $(document).off("scroll");
        // target element id
        var id = $(this).attr('href');
        // target element
        var $id = $(id);
        var height = $($id).offset().top;
        var nav_A = $('header').outerHeight();
        var nav_B = $(".sticky-nav").outerHeight();
        var winWidth = $(window).width();
        // if ($id.length === 0) {
        //   return;
        // }
        var pos = $id.offset().top;
        if (winWidth > 596) {
          $('html, body').animate({
            scrollTop: pos - nav_A - nav_B + 30
          }, 'slow');
          $(document).on("scroll", onScroll);
          // console.log('yash')
        }
        else {
          $('html, body').animate({
            scrollTop: pos - nav_A
          }, 'slow');
          $(document).on("scroll", onScroll);
        }
        $('.sticky-nav .column-wrapper .by-category').removeClass("active");
        $(this).parent().addClass("active");
        // console.log('click');
        $(this).parent().find('.sticky-nav').addClass("change");
      });
    }
  });
  function onScroll(event) {
      $('.sticky-nav .column-wrapper .by-category a[href^="#"]').each(function () {
          var id = $(this).attr('href');
          // target element
          var $id = $(id);
          var scrollPos = $(document).scrollTop() + 140;
          // console.log("click");
          var currLink = $(this);
          var refElement = $(currLink.attr("href"));
          var winWidth = $(window).width();
          if (winWidth > 596) {
          if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
              $('body .sticky-nav .column-wrapper .by-category').removeClass("active");
              currLink.parent().addClass("active");
          }
          else {
              currLink.parent().removeClass("active");
          }
          }
      });
  }
})(jQuery);