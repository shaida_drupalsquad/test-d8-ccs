(function($){
$(document).ready(function(){
    var itemLength = $(".image-slider .slider .item").length;
    $('.image-slider .slider').slick({
        dots: true,
        infinite:true,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendDots: $('body .image-slider .button-wrap .dots-outer'),
        prevArrow: $('body .image-slider .button-wrap .pre-btn'),
        nextArrow: $('body .image-slider .button-wrap .next-btn'),

    });
    if ((itemLength == 1)) {
        $(".image-slider .button-wrap").css({"display": "none"});
    }
    $(window).on("load resize", function (e) {
        $(".image-slider .column-wrapper .slider").css({"opacity": 1});
    });
});
})(jQuery);
