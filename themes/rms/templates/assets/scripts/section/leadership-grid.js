(function ($) {
    $(window).on("load resize", function (e) {
        if ($('body .leadership-grid .cards-wrapper .card-item').length > 0) {
            var containerOffsetLeft = $('header').offset().left;
            var $windowWidth = $('header').width();
            $('body .leadership-grid .cards-wrapper .card-item').each(function (index, value) {
                var offsetLeft = $(value).offset().left;
                $(value).find('.collapse-box').css({
                    'left': (containerOffsetLeft - offsetLeft),
                    "width": $windowWidth
                });

                if ((window.matchMedia("(max-width:595px)").matches)) {
                    $(value).find('.collapse-box').css({
                        'left': (containerOffsetLeft - offsetLeft - 25),
                        "width": $windowWidth + 25
                    });
                }
            });
        }

        $("body .leadership-grid .cards-wrapper .card-item .content-wrap").matchHeight();
        $("body .leadership-grid .cards-wrapper .card-item .content-wrap .text-wrap").matchHeight();
        $("body .slider-block .container-block .slider-outer").css({"opacity": 1});
    });
    $(function () {

        $("body .leadership-grid .cards-wrapper .card-item").click(function (event) {
            event.stopPropagation();
            $("body .leadership-grid .cards-wrapper .card-item").find(".collapse-box").removeAttr('id');

            $("body .leadership-grid .cards-wrapper .card-item").removeClass("active");
            $("body .leadership-grid .cards-wrapper .card-item .collapse-box").slideUp();
            $("body .slider-event-detail").removeClass("work");
            if ($(this).find(".collapse-box").is(':visible'))
                return;
            $(this).addClass('active');

            $("body .slider-event-detail").addClass("work");

            $(this).find(".collapse-box").attr('id', 'Uni');

            $(this).find(".collapse-box").slideDown({
                start: function () {
                    if ($(this).find(".slider-outer").length) {
                        $(this).find(".slider-outer").css({"height": 56});
                        if ($(this).find(".slider-outer").hasClass("slick-slider")) {
                            $(this).find(".slider-outer").slick("unslick");
                        }
                    }
                    // slick-initialized slick-slider
                },
                done: function () {
                    if ($(this).find(".slider-outer").length) {
                        $(this).find(".slider-outer").css({"height": "auto", "transition-delay": "0.8s"});
                        $(this).find(".slider-outer").slick({
                            infinite: true,
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            dots: false,
                            arrows: true,
                            autoplay: false,
                            draggable: true,
                            responsive: [
                                {
                                    breakpoint: 992,
                                    settings: {
                                        slidesToShow: 2,
                                        draggable: true
                                    }
                                },
                                {
                                    breakpoint: 768,
                                    settings: {
                                        slidesToShow: 1,
                                        draggable: true
                                    }
                                }
                            ]
                        });
                    }
                    setTimeout(animateCol());

                }

            })
            ;


            function animateCol() {
                $('html, body').animate({
                    scrollTop: $("#Uni").offset().top - 80
                }, 800);
            }


            if ($("body .leadership-grid .cards-wrapper .card-item").hasClass(".slider-outer")) {

                var slideItemlength = $("body .leadership-grid .cards-wrapper .card-item").find(".slider-outer").length;

                if (slideItemlength < 3) {
                    console.log("unslick" + slideItemlength);
                    $("body .leadership-grid .cards-wrapper .card-item").find(".slider-outer").slick("unslick");
                }

                $("body .leadership-grid .cards-wrapper .card-item .collapse-box .right-content .content-wrapper .slider-block .slider-outer .item-box").css({"max-width": "100%"});
            }


        });

        $('body .leadership-grid .cards-wrapper .card-item .collapse-box').click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            // $("body .leadership-section .cards-wrapper .card-item").removeClass('active');
            // $('body .leadership-section .cards-wrapper .card-item .collapse-box').slideUp();
            // $("body .slider-event-detail").removeClass("work");
        });
    });


})(jQuery);

