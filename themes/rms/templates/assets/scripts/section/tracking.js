(function ($) {
    $(function () {
        // Activate tracking on home video click
        $(document).on('click', 'body.node-1616 section.video-banner .button-box a.video-play', function() {
            try{
                __adroll.record_user({"adroll_segments": "2a205996"})
            } catch(err) {}
        });

        $(document).on('click', 'body.node-2715 section.cta .button-box a.btn-red', function() {
            try{
                __adroll.record_user({"adroll_segments": "c0e2ac60"})
            } catch(err) {}
        });

    });
})(jQuery);
