// Force external links to open in new tab / window
(function($){
  $(function(){
    var siteName = window.location.host.replace('www.', '');
    $('a[href^="htt"]:not([target]):not([href*="www.' + siteName + '"]):not([href*="//' + siteName + '"]):not([class*="fancybox"])').attr('target', '_blank');
  });
  if ($('body').hasClass('node-2256')) {
    $('a[href^="https://"]').attr('target', '_blank');
  }

})(jQuery);

(function ($) {
  // On DOM ready
  $(function () {
    // Initialize WOW.js plugin
    new WOW().init();

    jQuery(window).on({
      'orientationchange resize scroll': function (e) {
        detect_touchDevice();
      }
    });

    function detect_touchDevice() {
      var is_touch_device = 'ontouchstart' in document.documentElement;
      if (is_touch_device) {
        jQuery('body').addClass('touch');
      }
      // Add class on touch device//

      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        jQuery('body').addClass('touch');
      } else {
        jQuery('body').removeClass('touch');
      }
    }

    objectFitImages();
  });
})(jQuery);

(function ($) {

  $(window).on("load resize", function (e) {
    if ($("body section").hasClass("full-intro")) {
      $(".intro-box").addClass("full-width-intro");
    }
    else {
      $(".intro-box").removeClass("full-width-intro");
    }
    $('.col-three-with-text .content-column h4').matchHeight({byRow: 0});
  });


  $(window).on("load", function () {
    if ($("body table").hasClass("table-bordered")) {
      $("body table.table-bordered").wrap("<div class='table-responsive'></div>");
    }
  });

// Add class on touch device//
  jQuery(document).ready(function ($) {
    var deviceAgent = navigator.userAgent.toLowerCase();
    var agentID = deviceAgent.match(/(iPad|iPhone|iPod)/i);
    if (agentID) {
      window.addEventListener('DOMContentLoaded', function () {
        $('body .col-three-with-image .col-grid  .content-box, body .leadership-slider .slider-block .slider-outer .card-item').on('touchstart', function (e) {
          'use strict'; //satisfy code inspectors
          var link = $(this); //preselect the link
          if (link.hasClass('active')) {
            return true;
          } else {
            link.addClass('active');
            // console.log("n");
            $('a.taphover').not(this).removeClass('active');
            e.preventDefault();
            return false; //extra, and to make sure the function has consistent return points
          }
        });

        $(document).on('touchstart', function () {
          $("body .leadership-slider .slider-block .slider-outer .card-item").removeClass('active');
        })

        $("body .blog-grid .search .fixed-position input").click(function () {
          $(this).addClass("change");
        });

        $("body .blog-grid .search .fixed-position .close span").click(function (e) {
          e.stopPropagation();
          $(this).parent().parent().find("input").removeClass("change");
        });
      });
    }
    else {
      $('body .col-three-with-image .col-grid  .content-box, body .leadership-slider .slider-block .slider-outer .card-item').on('touchstart', function (e) {
        'use strict'; //satisfy code inspectors
        var link = $(this); //preselect the link
        if (link.hasClass('active')) {
          return true;
        } else {
          link.addClass('active');
          // console.log("n");
          $('a.taphover').not(this).removeClass('active');
          e.preventDefault();
          return false; //extra, and to make sure the function has consistent return points
        }
      });

      $("body .blog-grid .search .fixed-position input").click(function () {
        $(this).addClass("change");
      });

      $("body .blog-grid .search .fixed-position .close span").click(function (e) {
        e.stopPropagation();
        $(this).parent().parent().find("input").removeClass("change");
      });
    }
  });
})(jQuery);


// Limit content Js START

// Get part of an object if it is set
function object_get() {
  // Turn arguments into array and get what we need now
  var args = Array.prototype.slice.call(arguments);
  var object = args.shift();
  var parameter = args.shift();

  // Get value if it exists or run recursively to look for it
  if (typeof object === 'object' || typeof object === 'function') {
    if (parameter in object) {
      if (args.length === 0) {
        return object[parameter];
      }
      args.unshift(object[parameter]);
      return object_get.apply(null, args);
    }
  }
  return undefined;
}


// Return true or false based on if an object exists
function object_exists() {
  return typeof object_get.apply(this, arguments) !== 'undefined';
}

// Limit text to a certain number of lines (requires dotdotdot plugin)
function limit_lines(target, lines, ellipsis) {
  // If target element exists
  var $target = jQuery(target);

  if ($target.length) {
    // Get line height of target
    var lineHeight = $target.css('line-height');
    if (lineHeight === 'normal') {
      lineHeight = 1.2 * parseFloat($target.css('font-size'));
    }

    // Calculate the target height
    var targetHeight = lines * parseFloat(lineHeight);

    // Set default value for ellipsis
    if (typeof ellipsis === 'undefined')
      ellipsis = '...';

    if (ellipsis === 'space')
      ellipsis = ' '

    // Run the plugin if it exists
    if (object_exists(window, 'jQuery')
        && object_exists(jQuery, 'fn', 'dotdotdot')) {
      $target.dotdotdot({
        ellipsis: ellipsis,
        height: targetHeight,
        watch: true,
//        after: "a.see-more"
      });
    }
  }
}

// Limit content Js END

(function ($) {
  $(document).ready(function () {
    $('.sticky-nav').each(function () {
      if ($('.sticky-nav').length > 0) {
        var WindowHeight = jQuery(window).height();
        var load_element = 0;
        var nav = $('header').outerHeight();
        //position of element
        var scroll_position = jQuery('.sticky-nav').offset().top - nav;
        var screen_height = jQuery(window).height();
        var activation_offset = 0;
        var max_scroll_height = jQuery('body').height() + screen_height;

        var scroll_activation_point = scroll_position - (screen_height * activation_offset);

        jQuery(window).on('scroll', function (e) {

          var y_scroll_pos = window.pageYOffset;
          var element_in_view = y_scroll_pos > scroll_activation_point;
          var has_reached_bottom_of_page = max_scroll_height <= y_scroll_pos && !element_in_view;

          if (element_in_view || has_reached_bottom_of_page) {
            jQuery('.sticky-nav').addClass("change");

            jQuery('section.sticky-nav').next().addClass("style");
            // console.log('yash');
          } else {
            jQuery('.sticky-nav').removeClass("change");
            jQuery('section.sticky-nav').next().removeClass("style");
          }
        });
      }
    });
    $(window).on("load resize", function () {
      $("body .sticky-nav").each(function (index, sel) {
        if ($('.sticky-nav').length > 0) {
          var winWidth = $(window).width();
          if (winWidth <= 400) {
            var itemLength = $("body .sticky-nav .column-wrapper .by-category").length;
            for (var i = 0; i < itemLength; i += 5) {
              if (itemLength == 3) {
                $("body .sticky-nav .column-wrapper").css({"display": "block"});
                // console.log('y')
              }
              else {
                $("body .sticky-nav .column-wrapper").css({"display": "flex"});
                // console.log('f')
              }
            }
          } else if (winWidth <= 596) {
            var itemLength = $("body .sticky-nav .column-wrapper .by-category").length;
            for (var i = 0; i < itemLength; i += 5) {
              if (itemLength > 3) {
                $("body .sticky-nav .column-wrapper").css({"display": "block"});
                // console.log('p')
              }
              else {
                $("body .sticky-nav .column-wrapper").css({"display": "flex"});
                // console.log('Q');
              }
            }
          }
          else {
            $("body .sticky-nav .column-wrapper").css({"display": "flex"});
            // console.log("scroll")
          }
        }
      });
    });
  });


  $(document).ready(function () {
    if ($('.sticky-nav').length > 0) {
      $(document).on("scroll", onScroll);
      $(document).on('click', 'a[href^="#"]',function () {
        // e.preventDefault();
        $(document).off("scroll");
        // target element id
        var id = $(this).attr('href');
        // target element
        var $id = $(id);
        var height = $($id).offset().top;
        var nav_A = $('header').outerHeight();
        var nav_B = $(".sticky-nav").outerHeight();
        var winWidth = $(window).width();
        // if ($id.length === 0) {
        //   return;
        // }
        var pos = $id.offset().top;
        if (winWidth > 596) {
          $('html, body').animate({
            scrollTop: pos - nav_A - nav_B + 30
          }, 'slow');
          $(document).on("scroll", onScroll);
          // console.log('yash')
        }
        else {
          $('html, body').animate({
            scrollTop: pos - nav_A
          }, 'slow');
          $(document).on("scroll", onScroll);
        }
        $('.sticky-nav .column-wrapper .by-category').removeClass("active");
        $(this).parent().addClass("active");
        // console.log('click');
        $(this).parent().find('.sticky-nav').addClass("change");
      });
    }
  });
  function onScroll(event) {
      $('.sticky-nav .column-wrapper .by-category a[href^="#"]').each(function () {
          var id = $(this).attr('href');
          // target element
          var $id = $(id);
          var scrollPos = $(document).scrollTop() + 140;
          // console.log("click");
          var currLink = $(this);
          var refElement = $(currLink.attr("href"));
          var winWidth = $(window).width();
          if (winWidth > 596) {
          if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
              $('body .sticky-nav .column-wrapper .by-category').removeClass("active");
              currLink.parent().addClass("active");
          }
          else {
              currLink.parent().removeClass("active");
          }
          }
      });
  }
})(jQuery);
(function($){
    $(document).ready(function () {
        $('.banner-second-level .column-wrapper .slider').slick({
            dots: true,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            appendDots: $('body .banner-second-level .dots-outer'),
            prevArrow: $('body .banner-second-level .pre-btn'),
            nextArrow: $('body .banner-second-level .next-btn'),
            draggable: true,
            fade: true,
            speed: 900,
            infinite: true,
            cssEase: 'opacity 2000ms ease-out 0s',

        });
        $(window).on("load resize", function (e) {
            $(".banner-second-level .column-wrapper .slider").css({"opacity": 1});
        });
        
        var item_length = $('.banner-second-level .column-wrapper .slider .item').length;
        if (item_length > 1) {
            $('body .banner-second-level').addClass('all-item');

        } else if (item_length === 1) {
            $('body .banner-second-level').addClass('no-card-item');
        }

    });
})(jQuery);




(function ($) {
    function setBannerheight() {
        if ($('body .banner-third-level').length > 0) {
            var winWidth = $(window).width();
            var val1, val2, section_height;
            val1 = $('body .banner-third-level .inner-text').height();
            val2 = $('body .banner-third-level .inner-text').offset().top;
            var extra_space1;
            if (winWidth >= 992) {
                extra_space1 = 50;
            }
            else if (winWidth >= 596) {
                extra_space1 = 70;
            }
            else {
                extra_space1 = 100;
            }
            section_height = val1 + val2 + extra_space1;
            if ($("body .banner-third-level").hasClass("banner-with-out-image")) {
                $('.banner-third-level .bg-img').css({"height": section_height + "px"});

            } else if ($("body .banner-third-level").hasClass("banner-blog-detail")) {
                $('.banner-third-level .bg-img').css({"height": section_height + "px"});
            }
        }
    }
    $(document).ready(function () {
        setBannerheight();
        $("body .banner-third-level").css({"opacity": 0});
    });
    $(window).on('load resize', function () {
        setBannerheight();
        $("body .banner-third-level").css({"opacity": 1});
    });


})(jQuery);
(function ($) {
    $(function () {
        var sliderElem = $("body .banner-with-carousel .right-block .slider-outer-block");
        $(sliderElem).slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            draggable: true,
        });

    });
    $(window).on("load resize", function () {
        $("body .banner-with-carousel .right-block .slider-outer-block").css({"opacity": 1});

        var itemLength = $("body .banner-with-carousel .row-wrapper .right-block .slider-outer-block .items").length;

        if (itemLength == 1) {
            $("body .banner-with-carousel .row-wrapper .right-block").addClass("no-border ")
        }

    });
})(jQuery);

if (!String.prototype.includes) {
    String.prototype.includes = function (search, start) {
        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}


(function ($) {

    $(function () {
        if ($('body').hasClass('node-1900')) {
            var val1 = 2020;
            var val = 0;

            /* Ajax filter functionality commented start */

            // $(document).on('change', '.blog-landing .select-plugin .Year', function () {
            //   $('#views-exposed-form-blog-block-3 .form-item-created-min input').val('2020-01-01 00:00:00');
            //   $('#views-exposed-form-blog-block-3 .form-item-created-max input').val('2020-12-31 00:00:00');
            //   val1 = $(this).val();
            //   if (val1 === '') {
            //     removeUrl('Month');
            //     removeUrl('Year');
            //     $('#blog-month').parent().css('display', 'none');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-min input').val('');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-max input').val('');
            //     setTimeout(function () {
            //       $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //
            //     }, 300)
            //   } else {
            //     urlUpdate('Year', val1);
            //     $('#blog-month').parent().css('display', 'block');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(val1 + '-01-01 00:00:00');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(val1 + '-12-31 00:00:00');
            //     setTimeout(function () {
            //       $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //     }, 300)
            //   }
            // });
            // $(document).on('change', '.blog-landing .select-plugin .Month', function () {
            //   $('#views-exposed-form-blog-block-3 .form-item-created-min input').val('2020-01-01 00:00:00');
            //   $('#views-exposed-form-blog-block-3 .form-item-created-max input').val('2020-12-31 00:00:00');
            //   val = $(this).val();
            //   if (val === '') {
            //     removeUrl('Month');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(val1 + '-01-01 00:00:00');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(val1 + '-12-31 00:00:00');
            //     setTimeout(function () {
            //       $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //     }, 300)
            //   } else {
            //     urlUpdate('Month', val);
            //     $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(val1 + '-' + val + '-01 00:00:00');
            //     $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(val1 + '-' + val + '-31 00:00:00');
            //     setTimeout(function () {
            //       $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //     }, 300)
            //   }
            // });
            //
            // $(window).on('load', function () {
            //   var href = window.location.href, array = [], it = 2020;
            //   href = href.split('?');
            //   href.shift();
            //   var newhref = href.join(" ");
            //   newhref = newhref.split("&");
            //   for (var i = 0; i < newhref.length; i++) {
            //     array.push(newhref[i].toString().split('='));
            //   }
            //
            //   for (var i = 0; i < array.length; i++) {
            //     var item = array[i];
            //     if (item[0] === "Year") {
            //        it = item[1];
            //       $('.blog-landing .select-plugin .Year').val(it);
            //       $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(it + '-01-01 00:00:00');
            //       $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(it + '-12-31 00:00:00');
            //     }
            //
            //     if (item[0] === "Month") {
            //       var it1 = item[1];
            //       var it2;
            //       $('#blog-month').parent().css('display', 'block');
            //       $('.blog-landing .select-plugin .Month option').each(function () {
            //
            //         if (it1 === $(this).val())
            //           it2 = $(this).text();
            //       })
            //       setTimeout(function () {
            //         $('.blog-landing .select-plugin .Month').val(it1);
            //         $('.blog-landing .select-plugin .Month').parent().find('.selectboxit-text').attr('data-val', it1).text(it2)
            //         $('#views-exposed-form-blog-block-3 .form-item-created-min input').val(it + '-' + it1 + '-01 00:00:00');
            //         $('#views-exposed-form-blog-block-3 .form-item-created-max input').val(it + '-' + it1 + '-31 00:00:00');
            //       }, 50);
            //     }
            //   }
            //   $('body .blog-grid select').selectBoxIt('refresh');
            //   setTimeout(function () {
            //     $('#views-exposed-form-blog-block-3 .form-submit').trigger('click');
            //   }, 300)
            // });

            /* Ajax filter functionality ends */

            /* Non ajax functionality start */
            $(document).on('change', '.blog-landing .select-plugin .Year', function () {
                removeUrl('page');
                val1 = $(this).val();
                if (val1 === '') {
                    removeUrl('Month');
                    removeUrl('Year');
                    $('#blog-month').parent().css('display', 'none');
                } else {
                    urlUpdate('Year', val1);
                    $('#blog-month').parent().css('display', 'block');
                }
                location.reload();
                return false;
            });
            $(document).on('change', '.blog-landing .select-plugin .Month', function () {
                removeUrl('page');
                val = $(this).val();
                if (val === '') {
                    removeUrl('Month');
                } else {
                    urlUpdate('Month', val);
                }
                location.reload();
                return false;
            });

            $(window).on('load', function () {
                var href = window.location.href, array = [], it = 2020;
                href = href.split('?');
                href.shift();
                var newhref = href.join(" ");
                newhref = newhref.split("&");
                for (var i = 0; i < newhref.length; i++) {
                    array.push(newhref[i].toString().split('='));
                }

                for (var i = 0; i < array.length; i++) {
                    var item = array[i];
                    if (item[0] === "Year") {
                        it = item[1];
                        $('.blog-landing .select-plugin .Year').val(it);
                    }

                    if (item[0] === "Month") {
                        var it1 = item[1];
                        var it2;
                        $('#blog-month').parent().css('display', 'block');
                        $('.blog-landing .select-plugin .Month option').each(function () {

                            if (it1 === $(this).val())
                                it2 = $(this).text();
                        })
                        setTimeout(function () {
                            $('.blog-landing .select-plugin .Month').val(it1);
                            $('.blog-landing .select-plugin .Month').parent().find('.selectboxit-text').attr('data-val', it1).text(it2)
                        }, 50);
                    }
                }
                $('body .blog-grid select').selectBoxIt('refresh');
            });

            /* Non ajax functionality ends */

            function removeUrl(str) {
                var href = window.location.href;
                var href1 = href.split("?");
                var newitem = href1[0].toString();
                href1.shift();
                var newhref = href1.join(" ");
                newhref = newhref.split("&");
                if (href.includes(str)) {
                    for (var item = 0; item < newhref.length; item++) {
                        if (newhref[item].toString().includes(str)) {
                            newhref.splice(item, 1)
                        }
                    }
                    newhref = newhref.join('&');
                    if (newhref === '') {
                        href1 = newitem + '' + newhref;
                    }
                    else {
                        href1 = newitem + '?' + newhref;
                    }
                    history.pushState({}, null, href1);
                }
            }

            function urlUpdate(str, value) {
                var href = window.location.href;
                var href1 = href.split("?");
                var newitem = href1[0].toString();
                href1.shift();
                var newhref = href1.join(" ");
                newhref = newhref.split("&");
                if (href.includes(str)) {
                    for (var i = 0; i < newhref.length; i++) {
                        if (newhref[i].toString().includes(str)) {
                            newhref.splice(i, 1)
                            newhref.push(str + '=' + value)
                        }
                    }
                    newhref = newhref.join('&')
                    href1 = newitem + '?' + newhref;
                    history.pushState({}, null, href1);
                }
                else {
                    if (href1.length < 1) {
                        history.pushState({}, null, href + '?' + str + '=' + value);
                    } else {
                        history.pushState({}, null, href + '&' + str + '=' + value);
                    }
                }
            }

            // Pagination to get rid zero
            $('nav.pager ul li').each(function () {
                var pageLink = $(this).children('a').attr('href');
                if (typeof pageLink !== typeof undefined && pageLink !== false) {
                    if (pageLink.indexOf('page=0') > -1 && pageLink.indexOf('page=0') == 1) {
                        $(this).children('a').attr('href', window.location.pathname);
                    } else if (pageLink.indexOf('page=0') > -1 && pageLink.indexOf('page=0') > 1) {
                        $(this).children('a').attr('href', $(this).children('a').attr('href').replace('&page=0', ''));
                    }
                }
            });

        }
    });

})(jQuery);

(function ($) {
    //New function
    function loadMasonary() {
        setTimeout(function () {
            $('body .blog-grid .column-wrapper .column-wrap .card-grid .grid').masonry({
                itemSelector: '.box',
                columnWidth: '.box',
                horizontalOrder: true,
            });

            $('body .news-grid .cover-two-col .card-grid .grid').masonry({
                itemSelector: '.box',
                columnWidth: '.box',
                horizontalOrder: true,
            });
            $('body .event-grid .column-wrapper .column-wrap .card-grid .grid').masonry({
                itemSelector: '.box',
                columnWidth: '.box',
                horizontalOrder: true,
            });
        }, 500);
        console.log("y");
    }

    $(window).on("load",function () {
        loadMasonary();
    });
    // $(document).ajaxComplete(function () {
    //     loadMasonary();
    // });

})(jQuery);


(function ($) {
    $(window).on("load ", function () {
        setTimeout(function () {
            $("body .news-grid .cover-two-col .card-grid").css({"opacity": 1});
        }, 500);
    });
    if ($("body .blog-grid").hasClass("blog-list-cards")) {
        var flag1 = 0;
        $(window).bind("load scroll resize", function () {
            if ($('.blog-grid.blog-list-cards').length > 0) {
                var $winTop = $(window).scrollTop();
                var $winWidth = $(window).width();
                var $offset = 0;
                var $blogContainer = $('.blog-grid.blog-list-cards .container');
                if ($blogContainer.length) {
                    $offset = $('.blog-grid.blog-list-cards .container').offset().top;
                }
                var height = $('.blog-grid.blog-list-cards .container').height();
                var total = $offset + height;

                var setValue1 = 140;
                if ($winWidth >= 992) {
                    if (flag1 == 0) {
                        if (($winTop >= $offset - setValue1) && ($winTop <= $offset - 130)) {
                            $('.blog-grid.blog-list-cards').addClass('fixed');
                            flag1 = 1;
                        }
                    }
                } else {
                    $('.blog-grid.blog-list-cards').removeClass('fixed');
                }

                // New condition
                if ($winWidth >= 992) {
                    if (($winTop >= height - 100)) {
                        $('.blog-grid.blog-list-cards').addClass('active');
                        flag1 = 1;
                    } else {
                        $('.blog-grid.blog-list-cards').removeClass('active');
                    }

                    if (($winTop <= $offset - 200)) {
                        $('.blog-grid.blog-list-cards').removeClass('fixed');
                        flag1 = 0;
                    }
                }
            }
        });

    }
})(jQuery);

(function($){
    if($('body').hasClass('node-2083')){
    var tmp1 = [];

    $(window).on('load',function(){
      var href = window.location.href, array=[];
      href= href.split('?');
      href.shift();
      var newhref= href.join(" ");
      newhref= newhref.split("&");
      for(var i=0; i< newhref.length;i++){
        array.push(newhref[i].toString().split('='));
      }

      for(var i=0;i<array.length;i++) {
        var item = array[i];
        if (item[0] === "search") {
            var it = item[1].split('_');
                it = it.join(' ');
                $(".blog-listing .search input").val('it');
            $('#views-exposed-form-blog-block-5 .form-item-keys input').val(it);
        }
      }
      setTimeout(function(){
        $('#views-exposed-form-blog-block-5 .form-submit').trigger('click');
      },300)           
  });

  $(document).on('change','.blog-listing .select-plugin .Month',function(){
    val = $(this).val();
    $('#views-exposed-form-blog-block-5 .form-item-sort-order select').val(val.toUpperCase());
    setTimeout(function(){
        $('#views-exposed-form-blog-block-5 .form-submit').trigger('click');
      },300)
});
$(".blog-listing .search form").on("submit", function (e) {
    e.preventDefault();
});

$(".blog-listing .search input").on("keyup", function (e) {
    // Number 13 is the "Enter" key on the keyboard
    var text = $(this).val();
    if(e.which == 13) {
        e.preventDefault();
        if (text.length > 0)
        urlUpdate('search', text.replace(/\s+/g,"_"));
      else
        removeUrl('search');
        $('#views-exposed-form-blog-block-5 .form-item-keys input').val(text);
        setTimeout(function(){
            $('#views-exposed-form-blog-block-5 .form-submit').trigger('click');
        },300);
    }
  });

    function removeUrl(str) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var item = 0; item < newhref.length; item++) {
            if (newhref[item].toString().includes(str)) {
              newhref.splice(item, 1)
            }
          }
          newhref = newhref.join('&');
          if (newhref === '') {
            href1 = newitem + '' + newhref;
          }
          else {
            href1 = newitem + '?' + newhref;
          }
          history.pushState({}, null, href1);
        }
      }

      function urlUpdate(str, value) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var i = 0; i < newhref.length; i++) {
            if (newhref[i].toString().includes(str)) {
              newhref.splice(i, 1)
              newhref.push(str + '=' + value)
            }
          }
          newhref = newhref.join('&')
          href1 = newitem + '?' + newhref;
          history.pushState({}, null, href1);
        }
        else {
          if (href1.length < 1) {
            history.pushState({}, null, href + '?' + str + '=' + value);
          } else {
            history.pushState({}, null, href + '&' + str + '=' + value);
          }
        }
      }
    }
})(jQuery);        
(function ($) {
    //Height match funchtion
    function cardHeight() {
          var big_card_body = 0;
          $('.blog-grid .column-wrapper .column-wrap .card-grid .grid .box .card-with-img .card-body').each(function(){
            if($(this).height() > big_card_body){
              big_card_body = $(this).height();
            }
          });
          $('.blog-grid .column-wrapper .column-wrap .card-grid .grid .box .card-with-img .card-body').height(big_card_body);

          var smallcard_body = 0;
          $('.blog-grid .column-wrapper .column-wrap .card-grid .grid .box  .small-card .card-body').each(function(){
            if($(this).height() > smallcard_body){
              smallcard_body = $(this).height();
            }
          });
          $('.blog-grid .column-wrapper .column-wrap .card-grid .grid .box .small-card .card-body').height(smallcard_body);
    }

    $(window).on("load", function (e) {
        $(".blog-grid .column-wrapper .column-wrap .card-grid .grid").css({"opacity": 1});
        cardHeight();
    });

    $(function () {

        // Search functionality
        $("body .blog-grid .search .fixed-position input" ).click(function () {
            $(this).addClass("change");
        });

        $("body .blog-grid .search .fixed-position .close span").click(function (e) {
            e.stopPropagation();
            $(this).parent().parent().find("input").removeClass("change");
            removeUrl('search');
  $(".blog-listing .search input").val(' ');
  $('#views-exposed-form-blog-block-5 .form-item-keys input').val('');
  setTimeout(function(){
    $('#views-exposed-form-blog-block-5 .form-submit').trigger('click');
},300);
        });

        function removeUrl(str) {
          var href = window.location.href;
          var href1 = href.split("?");
          var newitem = href1[0].toString();
          href1.shift();
          var newhref = href1.join(" ");
          newhref = newhref.split("&");
          if (href.includes(str)) {
            for (var item = 0; item < newhref.length; item++) {
              if (newhref[item].toString().includes(str)) {
                newhref.splice(item, 1)
              }
            }
            newhref = newhref.join('&');
            if (newhref === '') {
              href1 = newitem + '' + newhref;
            }
            else {
              href1 = newitem + '?' + newhref;
            }
            history.pushState({}, null, href1);
          }
        }

    });

})(jQuery);

(function ($) {
    $(document).ready(function () {
        if($('html').hasClass( "ua-firefox") || $('html').hasClass( "ua-ie")){
            var blog_lines = 3;
            var blog_head = 4;
        }else{
            var blog_lines = 2;
            var blog_head = 3;
        }
        limit_lines('.card-with-blog  .card.card-without-image .content-btn.item p', blog_lines);
        limit_lines('.card-with-blog  .card.card-without-image .content-btn.item h6', blog_lines);
        limit_lines('.card-with-blog  .card .content-btn.item h6', blog_head);
    });
})(jQuery);
(function ($) {


    $(function () {
        $(window).on("load resize", function (e) {
            if ((window.matchMedia("(min-width: 1700px)").matches)) {
                var containerPos = $("body .container").offset().left;
                $("body .card-with-features .row-wrapper").css({"left": containerPos});
            }
            else {
                $("body .card-with-features .row-wrapper").css({"left": 0});
            }

          var winWidth = $(window).width();
          if (winWidth >= 992) {
            var white_box = $("body .card-with-features .slider-block").outerHeight();
            $("body .card-with-features .row-wrapper .overview-block").css({"height": white_box + 69});
          }else {
            $("body .card-with-features .row-wrapper .overview-block").css({"height":"auto"});
          }

        })
    });

})(jQuery);
(function ($) {
    $(function () {
        $(".col-four-with-slider .slider-block .slide-outer").slick2({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            appendDots: $(
                "body .col-four-with-slider .row-wrapper .slider-block .button-wrap .dots-outer"
            ),
            prevArrow: $(
                "body .col-four-with-slider .row-wrapper .slider-block .button-wrap .pre-btn"
            ),
            nextArrow: $(
                "body .col-four-with-slider .row-wrapper .slider-block .button-wrap .next-btn"
            ),
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });

        var itemLength = $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").length;
        if (itemLength <= 3) {
            $(".col-four-with-slider .slider-block .slide-outer").slick('unslick');
            $(".col-four-with-slider .slider-block .slide-outer").addClass('row');
            $(".col-four-with-slider .slider-block .slide-outer .item-box").addClass('un-slick');
            $(".col-four-with-slider .slider-block .slide-outer .item-box").removeClass('slick-init');
            $("body .col-four-with-slider .row-wrapper .slider-block .button-wrap").css({opacity: 0});
            $("body .col-four-with-slider .row-wrapper .overview-block").addClass("extra-space");
        }
        else {
            $(".col-four-with-slider .slider-block .slide-outer .item-box").addClass('slick-init');
            $("body .col-four-with-slider .row-wrapper .overview-block").removeClass("extra-space");
        }
        if (itemLength == 3) {
            $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").addClass("col-three");
        }
        if (itemLength == 2) {
            $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").addClass("col-two");
        }
        if (itemLength == 1) {
            $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").addClass("col");
        }

        var arrayItem = $(
            "body .col-four-with-slider .slider-block .slide-outer .item-box"
        );
        var outerWidth = $(arrayItem).width();
        var slickOuter =
            $(arrayItem)
                .parents()
                .find(".slick-slider .slick-track")
                .width() * 2;
        var newWidth = outerWidth + slickOuter;

        $(window).on("load resize", function (e) {
            $("body .col-four-with-slider .slider-block .slide-outer").css({
                opacity: 1
            });
            if (window.matchMedia("(min-width: 991px)").matches) {
                $(arrayItem).on("mouseover", function () {
                    $(
                        "body .col-four-with-slider .slider-block .slide-outer .slick-list .slick-track"
                    ).css({width: newWidth * 3});
                });

                $(arrayItem).on("mouseout", function () {
                    $(
                        "body .col-four-with-slider .slider-block .slide-outer .slick-list .slick-track"
                    ).css({width: slickOuter});
                });
            }
            if (window.matchMedia("(min-width: 1700px)").matches) {
                var containerPos = $("body .container").offset().left;
                $("body .col-four-with-slider .row-wrapper").css({
                    left: containerPos
                });
            } else {
                $("body .col-four-with-slider .row-wrapper").css({left: 0});
            }
        });


        $(window).on("load resize", function () {
            $("body .col-four-with-slider .row-wrapper .slider-block .slide-outer .item-box").each(function () {
                var hoverState = $(this).find(".hover-state").length;
                if (hoverState < 1) {
                    $(this).addClass("nohover");
                    $(this).removeClass("slick-init");
                }
            });
        });

        //new condition
        if ($('body .col-four-with-slider ').length > 0) {
            var window_width = $(window).width();
            if (window_width <= 768) {
                $(document).on("scroll");
                $("body .col-four-with-slider .row-wrapper .slider-block .button-wrap .pre-btn, body .col-four-with-slider .row-wrapper .slider-block .button-wrap .next-btn, body .col-four-with-slider .row-wrapper .slider-block .button-wrap .dots-outer ").click(function () {
                    $('html, body').animate({
                        scrollTop: $("body .col-four-with-slider .row-wrapper .slider-block").offset().top - 90
                    }, 2000);
                });
            }
        }


    });
})(jQuery);

(function ($) {

  $(function (e) {
    var slider_item = 3;
    var itemLength = $("body .col-three-slider .column-wrapper .slider .item").length;

    $("body .col-three-slider .column-wrapper .slider").slick({
      infinite: true,
      slidesToShow: slider_item,
      slidesToScroll: 1,
      dots: true,
      arrows: true,
      draggable: true,
      appendDots: $('body .col-three-slider .column-wrapper .dots-outer'),
      prevArrow: $('body .col-three-slider .column-wrapper .pre-btn'),
      nextArrow: $('body .col-three-slider .column-wrapper .next-btn'),
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            draggable: true
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            draggable: true
          }
        },
        {
          breakpoint: 595,
          settings: {
            slidesToShow: 1,
            draggable: true
          }
        }
      ]

    });

    if (itemLength == 2) {
      $('body .col-three-slider').addClass('two-slider-item');
      if ($(".col-three-slider").hasClass("two-slider-item")) {
        slider_item = 2;
      }
    } else if (itemLength == 1) {
      $('body .col-three-slider').addClass('one-slider-item');
      if ($(".col-three-slider").hasClass("one-slider-item")) {
        slider_item = 1;
      }
    } else {
      $('body .col-three-slider').removeClass('no-slider-item');

    }
  });
  $(window).on("load resize", function () {
    $('body .col-three-slider .column-wrapper .slider .card-box').matchHeight();
  });

  $(window).on("load", function () {
    if($('.ua-firefox').length > 0) {
     setTimeout(function () {
       $(".col-three-slider .column-wrapper .slider").css({"opacity": 1});
       $(".col-three-slider .column-wrapper .button-wrap").css({"opacity": 1});
     },1000);
    } else {
      $(".col-three-slider .column-wrapper .slider").css({"opacity": 1});
      $(".col-three-slider .column-wrapper .button-wrap").css({"opacity": 1});
    }
  });


  $(document).ready(function () {

    limit_lines('.col-three-slider .column-wrapper .card-box .content-box h5', 3);
    limit_lines('.col-three-slider .column-wrapper .card-box .content-box p', 3);



  });
})(jQuery);
(function ($) {

    $(function () {

        $(window).on("load resize", function () {
            $("body .col-three-with-image").each(function () {
                $(".blue-overlay  h5").wrap('<div class="cover-heading" />');
            });
        });
        $("body .col-three-with-image .col-grid .content-box").matchHeight();

        var boxLength = $("body .col-three-with-image .col-grid .content-box").length;

        if (boxLength == 2) {
            $("body .col-three-with-image .col-grid .content-box").addClass("two-box");

        }
        if (boxLength == 1) {
            $("body .col-three-with-image .col-grid .content-box").addClass("m-0");
        }
        if (boxLength > 3) {
            $("body .col-three-with-image .col-grid .content-box").addClass("add-more");
            $("body .col-three-with-image .col-grid").css("justify-content", "left")
        }

    });



})(jQuery);
(function ($) {
    //New function

    if($("body .col-two-with-cards").hasClass("col-three-benefits-cards")){
        function loadMasonary() {
            // setTimeout(function () {
            $('body .col-three-benefits-cards .content-box .column-wrapper').masonry({
                itemSelector: '.card',
                columnWidth: '.card',
                horizontalOrder: true,
                isAnimated: false,
                // percentPosition: true,
            });
            // });
        }

        function addTransition(it){
            let length =$('body .col-three-benefits-cards .content-box .column-wrapper .card').length;
            for(let i=0; i<length;i++){
                if(i>=length-it){
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card:eq('+i+') .card-inner').addClass('last-items');
                }else{
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card:eq('+i+') .card-inner').removeClass('last-items');
                }
            }
        }
        let calc = $('body .col-three-benefits-cards .content-box .column-wrapper .card').outerHeight();
        function windowSize(){
            let length =$('body .col-three-benefits-cards .content-box .column-wrapper .card').length;
            if(window.matchMedia("(min-width: 992px)").matches){
                addTransition(3);
            }
            else if (window.matchMedia("(max-width: 991px)").matches && window.matchMedia("(min-width: 596px)").matches) {
                addTransition(2);
            }else{
                addTransition(length);
            }
        }
        windowSize();
        $(window).on("load resize",function () {
            loadMasonary();
            windowSize();
            calc = $('body .col-three-benefits-cards .content-box .column-wrapper .card').outerHeight();
            var heightbg =$('body .col-two-with-cards .content-box').height();
            $('body .col-two-with-cards .bg-img').css('height',heightbg+'px');
        });

        var heightbg =$('body .col-two-with-cards .content-box').height();
        $('body .col-two-with-cards .bg-img').css('height',heightbg+'px');
        $('body .col-three-benefits-cards .content-box .column-wrapper .card .card-inner').matchHeight();

        $('body .col-three-benefits-cards .content-box .column-wrapper .card').hover( function () {

            if(!$('body').hasClass('touch')){
                $(this).addClass("hover-active");
                let duration = $(this).find('.card-inner').css('transition-duration');
                duration=parseFloat(duration)*1000;
                setHeight.call(this);
                mas(duration);
                $('html, body').delay(700).animate({
                    'scrollTop': $(this).offset().top - $('header').height()
                }, 350, 'swing', function () {
                });
            }


        },function () {
            if(!$('body').hasClass('touch')){
                if($('html').hasClass( "ua-safari")){
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').removeAttr("style");
                    // $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').css('height',calc+'px');
                }

                $(this).removeClass('hover-active');
                let duration = $(this).find('.card-inner').css('transition-duration');
                duration=parseFloat(duration)*1000;
                $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').matchHeight();
                // $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').css('height',calc+'px');
                // $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').removeAttr("style");
                mas(duration);
            }
        });


        $('body .col-three-benefits-cards .content-box .column-wrapper .card').on('click',function (e) {
            console.log("l");
            e.stopPropagation();
            if($('body').hasClass('touch')){
                if(!$(this).hasClass('hover-active')){
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card').removeClass('hover-active');
                    $(this).addClass("hover-active");
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').css('height',calc+'px');
                    let duration = $(this).find('.card-inner').css('transition-duration');
                    duration=parseFloat(duration)*1000;
                    setHeight.call(this);
                    mas(duration);
                }
                else{
                    $(this).removeClass('hover-active');
                    let duration = $(this).find('.card-inner').css('transition-duration');
                    duration=parseFloat(duration)*1000;
                    $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').css('height',calc+'px');
                    mas(50);
                }
            }
        });

        $(document).on('click',function(){
            if($('body').hasClass('touch')){
                $('body .col-three-benefits-cards .content-box .column-wrapper .card').removeClass('hover-active');
                // $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').matchHeight();
                mas(50);
                $('body .col-three-benefits-cards .content-box .column-wrapper .card-inner').removeAttr("style");

            }
        });
        function setHeight(){
            $this = $(this);
            var winWidth = $(window).width();
            var extra_space1;
            if (winWidth >= 992) {
                extra_space1 = 0;
            }
            else if (winWidth >= 596) {
                extra_space1 = 20;
            }
            else {
                extra_space1 = 40;
            }
            let pcalc = $this.find('p > span').outerHeight() - 168,
                totalheight= calc + pcalc;
            $this.find('.card-inner').css('height',totalheight + extra_space1 +'px');
        }
    }
    function mas(duration){
        setTimeout(function(){
            loadMasonary();
        },duration+50)

    }
})(jQuery);


//  add/remove Class function
(function($){
  $(function(){
    if ($('body .content-with-form').length > 0) {
      // console.log("z");
      $('body .content-with-form .column-wrapper .form-box .column-wrap form').submit(function() {
        var window_width = $(window).width();
        var header_height =$("header").outerHeight();
        if (window_width >= 992) {
          $(document).on("scroll");
          $('html, body').animate({
            scrollTop: $("body .content-with-form .column-wrapper ").offset().top - header_height - 20
          }, 2000);
          // console.log("x");
          
        }
        else {
          $(document).on("scroll");
          $('html, body').animate({
            scrollTop: $("body .content-with-form .column-wrapper .form-box .column-wrap").offset().top - header_height - 20
          }, 2000);
          // console.log("p");
        }
        // console.log("m");
      });
    }
  });

})(jQuery);
(function ($) {
    $(function () {
        $("body .events-cards .card-wrap .cards-item").slice(0, 6).show();
        $("body .events-cards .button-box .btn-white").on("click", function (e) {
            e.preventDefault();
            $("body .events-cards .card-wrap .cards-item:hidden").slice(0, 3).slideDown(500);

            if ($("body .events-cards .card-wrap .cards-item:hidden").length == 0) {
                $("body .events-cards .button-box .btn-white").fadeOut(500);
            }

            $('html,body').animate({
                scrollTop: $(this).offset().top - 500
            }, 500);


        });
    });


})(jQuery);
(function($){   
    if($('body').hasClass('node-1902')){
    var tmp1 = [];
    
    $(window).on('load',function(){
      var href = window.location.href, array=[];
      href= href.split('?');
      href.shift();    
      var newhref= href.join(" ");
      newhref= newhref.split("&");
      for(var i=0; i< newhref.length;i++){
        array.push(newhref[i].toString().split('='));
      }
      
      for(var i=0;i<array.length;i++) {
        var item = array[i];

        if (item[0] === "tradeshows") {
            $('.event-grid .content-wrapper .filter-wrap .check-content input[value="9"]').prop('checked', true);
             tmp1.push('9');
        }
        if(item[0] === "upcomingwebcasts"){
          $('.event-grid .content-wrapper .filter-wrap .check-content input[value="10"]').prop('checked', true);
           tmp1.push('10');
        }
        if(item[0] === "ondemandwebcasts"){
          $('.event-grid .content-wrapper .filter-wrap .check-content input[value="7"]').prop('checked', true);
           tmp1.push('7');
        }
        if(item[0] === "speakingengagements"){
          $('.event-grid .content-wrapper .filter-wrap .check-content input[value="8"]').prop('checked', true);
           tmp1.push('8');
        }
      }
      for(var i=0;i<tmp1.length;i++){
        $('#views-exposed-form-event-block-2 .js-form-item select').val([tmp1[0], tmp1[1],tmp1[2],tmp1[3]]);
      }
      setTimeout(function(){
        $('#views-exposed-form-event-block-2 .form-submit').trigger('click');
      },300)           
  });

    $(document).on('click','.event-grid .check-content input',function(){
        var checked = $(this).val();
        var text= $(this).next().text();
        if(text.match('Tradeshows & Conferences'))
            text = 'tradeshows';
        else if(text.match('Upcoming Webcasts'))
            text= 'upcomingwebcasts';
        else if(text.match('Speaking Engagements'))
            text= 'speakingengagements';
        else
            text = 'ondemandwebcasts';

      if ($(this).is(':checked')) {
        tmp1.push(checked);
        urlUpdate(text,checked)
      } else {
        tmp1.splice($.inArray(checked, tmp1),1);
        removeUrl(text);
      }
        $('#views-exposed-form-event-block-2 .js-form-item select').val([tmp1[0], tmp1[1],tmp1[2],tmp1[3]]);
      if(tmp1.length===0){
        $('#views-exposed-form-event-block-2 .js-form-item select').val(['10', '9','7','8']);
      }
        setTimeout(function(){
          $('#views-exposed-form-event-block-2 .form-submit').trigger('click');
        },300)
    });
    $('.event-grid .reset-filter').on('click',function(){
      tmp1=[];
      var href = window.location.href;
        var href1 = href.split("?");
        history.pushState({}, null, href1[0]);
      $('.event-grid .content-wrapper .filter-wrap .check-content').each(function(){
        if ($(this).find('input').is(':checked')) {
          $(this).find('input').prop('checked', false);
        }
      });
      $('#views-exposed-form-event-block-2 .js-form-item select').val(['10', '9','7','8']);
      setTimeout(function(){
        $('#views-exposed-form-event-block-2 .form-submit').trigger('click');
      },300)
    })
    function removeUrl(str) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var item = 0; item < newhref.length; item++) {
            if (newhref[item].toString().includes(str)) {
              newhref.splice(item, 1)
            }
          }
          newhref = newhref.join('&');
          if (newhref === '') {
            href1 = newitem + '' + newhref;
          }
          else {
            href1 = newitem + '?' + newhref;
          }
          history.pushState({}, null, href1);
        }
      }
    
        // Param Update
        function urlUpdate(str, value) {
            var url = window.location.href;
            if (url.indexOf("?") > -1) {
                var urlArr = url.split("?");
                var makeURL = urlArr[0];
                for(var i=0;i<urlArr.length;i++) {
                    if(i == 0) {
                        makeURL = makeURL + '?'+urlArr[1];
                    } else {
                        makeURL = makeURL + '&'+str;
                    }
                }
                history.pushState({}, null, makeURL);
            } else {
                history.pushState({}, null, url + '?' + str);
            }
        }
        //
    }
})(jQuery);        
(function ($) {
    $(function () {

        var readtxtLength = $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .more-text").length;
        console.log(readtxtLength + "new change ");
        if (readtxtLength >= 1) {
            $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn").show();
        }
        else {
            $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn").hide();
        }

        $(document).on("click", "body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn", function (e) {
            // e.stopPropagation();


            $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn").show();
            if ($(this).hasClass('rotate')) {
                $(this).removeClass('rotate');
                $(this).parent().find(".more-text").stop(true, true).slideUp(500);
                $(this).text("Read More");
            }
            else {
                // Hide the other tabs
                $('body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn').removeClass('rotate');
                $('body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn').text("Read More");
                $(this).addClass('rotate');
                $(this).text("Read Less");
                $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post .event-summary .read-more-btn").parent().find(".more-text").stop(true, true).slideUp(500);
                $(this).parent().find(".more-text").stop(true, true).slideDown(500);

            }


        });

        $("body .event-response-grid .column-wrapper .cover-two-col .card-grid .grid .event-post").slice(0, 4).show();


    });


})(jQuery);
(function($){
    if($('body').hasClass('node-1905')) {
        var xhttp = new XMLHttpRequest();
        var result = '';
        var loadmore = 6;
        var intialstate = 0;
        var str1 = 'all',
            str2 = 'all',
            str3 = 'all',
            str4 = 'all';
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                result = JSON.parse(this.responseText);
                var cla = 'Region',
                    cla1 = 'Catastrophe';

                var regions = result.regions;
                loop(regions, cla);
                var catastrophes = result.catastrophes;
                loop(catastrophes, cla1);          

            }
        };

        xhttp.open("GET", "/sites/default/files/json/event-response/owl.json", true);
        xhttp.send();

        function loop(regions, it) {
            for (var i = 0; i < regions.length; i++) {
                var convert = '';
                if (it === 'Month') {
                    convert = regions[i].categoryId;
                } else {
                    convert = regions[i].name.split(' ');
                    convert = convert.join('_');
                }
                $('.' + it).append('<option value=' + convert + '>' + regions[i].name + '</option>')
            }
        }

        var xhttp1 = new XMLHttpRequest();
        var result1 = [];
        xhttp1.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                result1 = JSON.parse(this.responseText);
                var res = result1.results;
                loadmore = 6;
                intialstate = 0;
                gridLoop(res, intialstate, loadmore);
            }
        };

        xhttp1.open("GET", "/sites/default/files/json/event-response/event.json", true);
        xhttp1.send();
         
        function gridLoop(res, intialstate, loadmore) {
            $('.event-response-grid .column-wrapper .cover-two-col .card-grid .grid > div').remove();
            if (res.length > 0) {
                res.slice(intialstate, loadmore).map(function (item) {
                    var date = item.modified.split("T");
                    $('.event-response-grid .column-wrapper .cover-two-col .card-grid .grid').append('<div class="event-post">' +
                        '<h5>' + item.title + '</h5>' +
                        '<span class="date">Updated ' + date[0] + '</span>' +
                        '<div class="media-content">' +
                        '<div class="img-box">' +
                        '<img src="https://support.rms.com' + item.eventImage + '" alt="map image">' +
                        '</div>' +
                        '<div class="content-text">' +
                        '<div class="small-content">' +
                        '<span>Loss Drivers</span>' +
                        '<div class="icon-text">' +
                        '<div class=icon-box id='+ item.perilLossDrivers+'>' +
                        '</div>' +
                        '<p>' + item.peril + '</p>' +
                        '</div>' +
                        '</div>' +
                        '<div class="gray-content">' +
                        '<ul class="content-inner">' +
                        '<li><p>' + item.headlineStatistic1Small + '</p></li>' +
                        '<li><p>' + item.headlineStatistic3Small + '</p></li>' +
                        '<li><p>' + item.headlineStatistic2Small + '</p></li>' +
                        '</ul>' +
                        '<ul class="content-inner">' +
                        '<li><h5>' + item.headlineStatistic1Large + '</h5></li>' +
                        '<li><h5>' + item.headlineStatistic3Large + '</h5></li>' +
                        '<li><h5>' + item.headlineStatistic2Large + '</h5></li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="event-summary">' +
                        '<h6>Event Summary</h6>' +
                        '<p>' + item.rssFeedDescription + '</p>'+
                        '<p>RMS customers can '+
                        '<a href="https://support.rms.com/group/rms/event-report?eventId='+item.eventId+'" target="_blank"> login to Owl</a> to access the full report.</p>'+
                        '<span class="read-more-btn">Read More</span>' +
                        '</div>' +
                        '</div>')
                });
            } else {
                $('.event-response-grid .column-wrapper .cover-two-col .card-grid .grid').append('<div class="event-post">' +
                    '<h4>No Result Found</h4>' +
                    '</div>');
            }
            if (res.length <= loadmore) {
                $('body .event-response-grid .button-box a.event-load').css('display', 'none');
            } else {
                $('body .event-response-grid .button-box a.event-load').css('display', 'inline-block');
            }
        }

        var filter = function (re) {
            let array = [];
            var array1 = [];
            var array2 = [];
            var array3 = [];
            if (str1 === 'all' && str2 === 'all' && str3 === 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    array.push(re[i])
                }
            }
            else if (str1 !== 'all' && str2 === 'all' && str3 === 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array.push(re[i])
                    }
                }
            } else if (str1 === 'all' && str2 !== 'all' && str3 === 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].region === str2) {
                        array.push(re[i])
                    }
                }
            } else if (str1 === 'all' && str2 === 'all' && str3 !== 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array.push(re[i])
                    }
                }
            } else if (str1 === 'all' && str2 === 'all' && str3 === 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array.push(re[i])
                    }
                }
            } else if (str1 !== 'all' && str2 !== 'all' && str3 === 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array1.push(re[i])

                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i].region === str2) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 !== 'all' && str2 == 'all' && str3 !== 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array1.push(re[i])
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 !== 'all' && str2 == 'all' && str3 == 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array1.push(re[i])
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 === 'all' && str2 !== 'all' && str3 !== 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].region === str2) {
                        array1.push(re[i])
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 === 'all' && str2 !== 'all' && str3 === 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].region === str2) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array.push(array1[i])
                    }
                }
            } else if (str1 === 'all' && str2 === 'all' && str3 !== 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array.length; i++) {
                    if (array[i].region === str3) {
                        var convert = array[i].eventDate.split('-');
                        if (convert[1] == str4) {
                            array1.push(array[i])
                        }
                    }
                }
            } else if (str1 !== 'all' && str2 === 'all' && str3 !== 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array2.push(array[i]);
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    if (array2[i].peril === str1) {
                        array.push(array2[i])
                    }
                }
            } else if (str1 === 'all' && str2 !== 'all' && str3 !== 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str4) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    var convert = array1[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array2.push(array[i]);
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    if (array2[i].region === str2) {
                        array.push(array2[i])
                    }
                }
            } else if (str1 !== 'all' && str2 !== 'all' && str3 !== 'all' && str4 === 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i].peril === str1) {
                        array2.push(re[i])
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    if (array2[i].region === str2) {
                        array.push(array2[i])
                    }
                }
            } else if (str1 !== 'all' && str2 !== 'all' && str3 === 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    var convert = re[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i].peril === str1) {
                        array2.push(re[i])
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    if (array2[i].region === str2) {
                        array.push(array2[i])
                    }
                }
            } else if (str1 !== 'all' && str2 !== 'all' && str3 !== 'all' && str4 !== 'all') {
                for (var i = 0; i < re.length; i++) {
                    if (re[i].peril === str1) {
                        array1.push(re[i]);
                    }
                }
                for (var i = 0; i < array1.length; i++) {
                    if (array1[i].region === str2) {
                        array2.push(re[i]);
                    }
                }
                for (var i = 0; i < array2.length; i++) {
                    var convert = array2[i].eventDate.split('-');
                    if (convert[0] == str3) {
                        array3.push(array2[i])
                    }
                }
                for (var i = 0; i < array3.length; i++) {
                    var convert = array3[i].eventDate.split('-');
                    if (convert[1] == str4) {
                        array.push(array3[i])
                    }
                }
            }
            gridLoop(array, intialstate, loadmore);
        }
        var loadmoreFunc = function (rs, intialstate, loadmore) {
            if (loadmore < rs.length) {
                filter(rs, intialstate, loadmore);
            } else {
                filter(rs, intialstate, loadmore);
            }
        }
        $(document).on('click', 'body .event-response-grid .button-box a.event-load', function (e) {
            e.preventDefault();
            var loadLength = result1.results;
            loadmore = loadmore + 6;
            intialstate = 0;
            loadmoreFunc.call(this, loadLength, intialstate, loadmore);
        });

        $(document).on('change', '.Catastrophe', function () {
            var val = $(this).val();
            var convert = val.split('_'),
                res = result1.results;
            str1 = convert.join(' ');
            loadmore = 6;
            intialstate = 0;
            filter(res, intialstate, loadmore);
            if (str1 === 'all') {
                removeUrl('Catastrophe')
            } else {
                urlUpdate('Catastrophe', val);
            }
        });

        $(document).on('change', '.Region', function () {
            var val = $(this).val();
            var convert = val.split('_'),
                res = result1.results;
            str2 = convert.join(' ');
            loadmore = 6;
            intialstate = 0;
            filter(res, intialstate, loadmore);
            if (str2 === 'all') {
                removeUrl('Region')
            } else {
                urlUpdate('Region', val);
            }
        });

        $(window).on('load', function () {
            var href = window.location.href, array = [];
            href = href.split('?');
            href.shift();
            var newhref = href.join(" ");
            newhref = newhref.split("&");
            for (var i = 0; i < newhref.length; i++) {
                array.push(newhref[i].toString().split('='));
            }
            for (var i = 0; i < array.length; i++) {
                var item = array[i];
                if (item[0] === "Catastrophe") {
                    var convert = item[1].split('_');
                    str1 = convert.join(' ');
                    $('.Catastrophe').val(item[1]).trigger("change");
                }
                if (item[0] === "Region") {
                    var convert = item[1].split('_');
                    str2 = convert.join(' ');
                    $('.Region').val(item[1]).trigger("change");
                }
            }
        });

        function removeUrl(str) {
            var href = window.location.href;
            var href1 = href.split("?");
            var newitem = href1[0].toString();
            href1.shift();
            var newhref = href1.join(" ");
            newhref = newhref.split("&");
            if (href.includes(str)) {
                for (var item = 0; item < newhref.length; item++) {
                    if (newhref[item].toString().includes(str)) {
                        newhref.splice(item, 1)
                    }
                }
                newhref = newhref.join('&');
                if (newhref === '') {
                    href1 = newitem + '' + newhref;
                }
                else {
                    href1 = newitem + '?' + newhref;
                }
                history.pushState({}, null, href1);
            }
        }

        function urlUpdate(str, value) {
            var href = window.location.href;
            var href1 = href.split("?");
            var newitem = href1[0].toString();
            href1.shift();
            var newhref = href1.join(" ");
            newhref = newhref.split("&");
            if (href.includes(str)) {
                for (var i = 0; i < newhref.length; i++) {
                    if (newhref[i].toString().includes(str)) {
                        newhref.splice(i, 1)
                        newhref.push(str + '=' + value)
                    }
                }
                newhref = newhref.join('&')
                href1 = newitem + '?' + newhref;
                history.pushState({}, null, href1);
            }
            else {
                if (href1.length < 1) {
                    history.pushState({}, null, href + '?' + str + '=' + value);
                } else {
                    history.pushState({}, null, href + '&' + str + '=' + value);
                }
            }
        }
    }
})(jQuery);
(function ($) {
    $(function () {
        $("body .events-cards .card-wrap .cards-item").matchHeight();
    });
})(jQuery);
(function($) {
    $(document).ready(function() {
        $(".feature-grid .content-wrap").click(function(
            e1
        ) {
            e1.stopPropagation();
        });
        var win_size = $(window).width();
        $(".feature-grid .col-wrapper").addClass("active");
        width_calc();
        slide_active();
        slide_close();

    });
    $(window).resize(function() {
        var win_size = $(window).width();
        $(".feature-grid .col-wrapper ").addClass("active");
        $(
            ".feature-grid .content-wrap .open-close-link"
        ).removeClass("close-active");
        $(".feature-grid .col-wrapper").removeClass("slide-active").removeClass("single-item");
        $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap .close-btn").removeClass("close-active");
        width_calc();
        // if (win_size < 576) {
        //     $(
        //         ".feature-grid .col-wrapper.active .content-wrap"
        //     ).removeAttr("style");
        // }
    });
    $(document).click(function() {
        var win_size = $(window).width();
        $(".feature-grid .col-wrapper ").addClass("active");
        $(
            ".feature-grid .col-wrapper .open-close-link"
        ).removeClass("close-active");
        $(".feature-grid .col-wrapper").removeClass("slide-active").removeClass("single-item");
        $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap .close-btn").removeClass("close-active");
        width_calc();
        // if (win_size < 576) {
        //     $(
        //         ".feature-grid .col-wrapper.active .content-wrap"
        //     ).removeAttr("style");
        // }
    });
    function slide_active() {
        var win_size = $(window).width();
        $(
            ".feature-grid .col-wrapper .open-close-link  .open-link "
        ).click(function(e) {
            e.stopPropagation();
            if (
                !$(this)
                    .parent()
                    .hasClass("close-active")
            ) {
                $(
                    ".feature-grid .col-wrapper .open-close-link"
                ).removeClass("close-active");
                $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap .close-btn").removeClass("close-active");
                $(".feature-grid .col-wrapper").removeClass(
                    "slide-active"
                ).removeClass("single-item");
                $(".feature-grid .col-wrapper ").addClass("active");
                var prev_width =
                    $(".feature-grid .col-wrapper").outerWidth() + "px";
                $(".feature-grid .col-wrapper .content-wrap").css(
                    "width",
                    prev_width
                );
            }
            mas();
            $(this)
                .parents(".col-wrapper")
                .removeClass("active");
            $(this)
                .parent()
                .addClass("close-active");
            $(this)
                .parents(".col-wrapper")
                .addClass("slide-active");
            $(this)
                .parents(".content-wrap")
                .find(".close-btn")
                .addClass("close-active");


            var par_width = $(this)
                    .parents(".col-wrapper")
                    .outerWidth(),
                width_div =
                    parseFloat(Math.ceil(par_width * 10) / 10).toFixed(1) * 2 + 16 + "px";
            $(this)
                .parents(".content-wrap")
                .css("width", width_div);
        });
    }
    function slide_close() {
        $(
            ".feature-grid .col-wrapper  .open-close-link  .close-link "
        ).click(function(a) {
            a.stopPropagation();
            $(this)
                .parents(".col-wrapper")
                .addClass("active");
            var prev_width =
                $(this)
                    .parents(".col-wrapper")
                    .outerWidth() + "px";
            $(this)
                .parents(".content-wrap")
                .css("width", prev_width);
            $(this)
                .parent()
                .removeClass("close-active");
            $(this)
                .parents(".col-wrapper")
                .removeClass("slide-active")
                .removeClass("single-item");

            $(this)
                .parents(".content-wrap")
                .find(".close-btn")
                .removeClass("close-active");
        });

        $(
            "body .feature-grid .outer-wrapper .col-wrapper .content-wrap .close-btn "
        ).click(function(a) {
            a.stopPropagation();
            $(this)
                .parents(".col-wrapper")
                .addClass("active");
            var prev_width =
                $(this)
                    .parents(".col-wrapper")
                    .outerWidth() + "px";
            $(this)
                .parents(".content-wrap")
                .css("width", prev_width);
            $(this)
                .parent(".content-wrap")
                .find(".open-close-link")
                .removeClass("close-active");
            $(this)
                .parents(".col-wrapper")
                .removeClass("slide-active")
                .removeClass("single-item");

            $(this)
                .removeClass("close-active");
        });


    }

    function width_calc() {
        var win_size = $(window).width();
        if (win_size >= 480) {
            var init_width =
                $(".feature-grid .col-wrapper.active").outerWidth() +
                "px";
            $(".feature-grid .col-wrapper.active .content-wrap").css(
                "width",
                init_width
            );
        }
    }
    $(function () {
        //$('.feature-grid .outer-wrapper .col-wrapper .content-wrap').matchHeight();
    });

    $(window).on('load resize', function () {
        $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap").css({"opacity": 1});
        $("body .feature-grid .outer-wrapper .col-wrapper .content-wrap p").each(function(){
            if($(this).text().length < 258){
                $(this).parents(".col-wrapper").addClass("no-read-more")
            }
        });
    });

    function mas(){
        setTimeout(function(){
            var item =$("body .feature-grid .outer-wrapper .col-wrapper.slide-active");
            $(item).addClass('single-item');
        },300)

    }

    // setTimeout(function(){
    //     $(this)
    //         .parents(".col-wrapper")
    //         .addClass('single-item ');
    // }, 6000);
})(jQuery);
(function ($) {
    $(document).ready(function () {
        $("body .filter-block select").selectBoxIt();
        $("body .blog-grid select").selectBoxIt();
    });
})(jQuery);
(function ($) {
    $(function () {
        $("body .header-rms .header-wrap .hamburger-logo").on("click", function () {
            if ((window.matchMedia("(max-width: 991px)").matches)) {
                if ($(this).hasClass("open")) {
                    $(this).removeClass("open");
                    $("body .header-rms .header-wrap nav").stop(true, true).slideUp();
                }
                else {
                    $(this).addClass("open");
                    $("body .header-rms .header-wrap nav").stop(true, true).slideDown();
                }
            }
        });

        //banner scroll full width//
        var bannerHeight = $('body .header-rms').height() / 2;
        $(window).on('scroll load', function () {
            var windScrolled = $(document).scrollTop();
            if ((windScrolled > bannerHeight)) {
                $("body .header-rms").addClass("fixed");
            }
            else {
                $("header").removeClass("fixed");

            }
        });

        //sub menu //

        //search btn click//
        $('body .header-rms .header-wrap nav .search-box').click(function (e) {
            e.stopPropagation();
            if ($(this).hasClass("active")) {
                $(this).removeClass('active');
                $(this).find(".sub-menu").stop(true, true).slideUp();
                $(this).parent("nav").find("ul li").removeClass("no-hover");
                $(this).parent("nav").find(".login-box").removeClass("no-hover");
            }
            else {
                $(this).addClass('active');
                $(this).find(".sub-menu").stop(true, true).slideDown();
                $(this).parent("nav").find("ul li").addClass("no-hover");
                $(this).parent("nav").find(".login-box").addClass("no-hover");
            }
            $("body .header-rms .header-wrap nav .login-box").removeClass('rotate-arrow');
            $("body .header-rms .header-wrap nav .login-box > .profiles-block").stop(true, true).slideUp(500);
            $('body .header-rms .header-wrap nav ul li').removeClass('rotate-arrow');
            $("body .header-rms .header-wrap nav ul li > .sub-menu").stop(true, true).slideUp(500);
        });
        $("body .header-rms .header-wrap nav .search-box .sub-menu").click(function (e) {
            e.stopPropagation();
        });
        $("body .header-rms .header-wrap nav ul li > .sub-menu").click(function (e) {
            e.stopPropagation();
        });
        $("body .header-rms .header-wrap nav .login-box > .profiles-block").click(function (e) {
            e.stopPropagation();
        });

        // responsive nav//
        // $(window).on("resize", function (e) {
        $("body .header-rms .header-wrap nav ul li, ul .red-link").on("click", function (e) {
            // e.stopPropagation();
            var windowWidth = $(window).width();
            if (windowWidth < 991) {
                if ($(this).hasClass('rotate-arrow')) {
                    $(this).removeClass('rotate-arrow');
                    $(this).find("> .sub-menu").stop(true, true).slideUp(500);
                }
                else {
                    // Hide the other tabs
                    $('body .header-rms .header-wrap nav ul li').removeClass('rotate-arrow');
                    $(this).addClass('rotate-arrow');
                    $("body .header-rms .header-wrap nav ul li > .sub-menu").stop(true, true).slideUp(500);
                    $(this).find("> .sub-menu").stop(true, true).slideDown(500);

                }
            }
        });

        //profile drop down//
        $("body .header-rms .header-wrap nav .login-box").on("click", function (e) {
            e.stopPropagation();
            var windowWidth = $(window).width();
            if (windowWidth < 991) {
                if ($(this).hasClass('rotate-arrow')) {
                    $(this).removeClass('rotate-arrow');
                    $(this).find("> .profiles-block").stop(true, true).slideUp(500);
                }
                else {
                    // Hide the other tabs
                    $('body .header-rms .header-wrap nav .login-box').removeClass('rotate-arrow');
                    $(this).addClass('rotate-arrow');
                    $("body .header-rms .header-wrap nav .login-box > .profiles-block").stop(true, true).slideUp(500);
                    $(this).find("> .profiles-block").stop(true, true).slideDown(500);
                }
            }
            $('body .header-rms .header-wrap nav .search-box').find("> .sub-menu").stop(true, true).slideUp();
            $('body .header-rms .header-wrap nav .search-box').removeClass('active');
            $('body .header-rms .header-wrap nav ul li').removeClass('rotate-arrow');
            $("body .header-rms .header-wrap nav ul li > .sub-menu").stop(true, true).slideUp(500);

        });
        // });


        $(document).click(function (e) {
            e.stopPropagation();
            $("body .header-rms .header-wrap nav .search-box").removeClass('active');
            $("body .header-rms .header-wrap nav .search-box .sub-menu").stop(true, true).slideUp();
            $("body .header-rms .header-wrap nav ul .red-link").removeClass('arrow-rotate');
            $("body .header-rms .header-wrap nav .login-box").removeClass('rotate-arrow');
            $("body .header-rms .header-wrap nav .login-box").find("> .profiles-block").stop(true, true).slideUp();
            $("body .header-rms .header-wrap nav ul li").removeClass("no-hover");
            $("body .header-rms .header-wrap nav .login-box").removeClass("no-hover");
        });


        //window url code//
        var pathName = window.location.pathname;
        var currentNode = pathName.replace("/node/", "");
        $("body .header-rms .header-wrap nav ul li > a").each(function () {
            var data = $(this).attr('href');
            // if (pathName == data) after node will convert into pathnamesw
            if (currentNode == data) {
                $(this).parents('ul').find('li').removeClass('active');
                $(this).parent().addClass('active');
            }
        });
    });
    $(window).on("load resize", function (e) {
        $("body .header-rms .header-wrap nav ul li > a").click(function (e) {
            var windowWidth = $(window).width();
            if (windowWidth < 991) {
                e.preventDefault();
            }
        });

    });
    $(window).on("load resize", function (e) {
        $("body .new-header.header-rms").css({"opacity": 1});
        var headerWidht = $("body .header-rms .wrapper-white").width();
        var winWidth = $(window).width();
        var extra_space1;
        if (winWidth >= 992) {
            extra_space1 = 64;
        }
        else {
            extra_space1 = 0;
        }

        if ((window.matchMedia("(min-width: 992px)").matches)) {
            $("body .header-rms .header-wrap nav .sub-menu").css({"width": headerWidht + extra_space1  +"px"});
        }
        else {
            $("body .header-rms .header-wrap nav .sub-menu").css({"width": "auto"});
        }
    });



})(jQuery);
(function ($) {

    $(function () {
        $("body .place-location").click(function () {
            var thisImage = $(this).parents().find('.bg-img img').attr('src');
            var thisText = $(this).find("span").text();
            $("body .home-overlay").addClass("active");
            $("body .home-overlay .overlay-box img ").attr('src', thisImage);
            $("body .home-overlay .place-location span").text(thisText);

        });

        $("body .close-btn").click(function () {
            $("body .home-overlay").removeClass("active");
        });

    });

    //cookies banner//
    window.geofeed = function(options) {
        // string two digit continent code EU, NA, OC, AS, SA, AF
        var continent = options.continent.toString();
        console.log(continent);
        if (continent !== 'EU') {
            var cookieBanner = $("body .cookies-banner");

            $(".close-btn , .close-txt").on("click", function () {
                $(cookieBanner).removeClass("active");
                localStorage.setItem("cookieexist", "true");
            });

            setTimeout(function () {
                if (!localStorage.getItem("cookieexist")) {
                    $(cookieBanner).addClass("active");
                }
            }, 1000);


            $(window).on("load ", function () {
                $("body .cookies-banner").css({"opacity": 1});

            });
        }

    };

// Call geo-location JSONP service
    var jsonp = document.createElement('script');
    jsonp.setAttribute('src', 'https://geolocation.onetrust.com/cookieconsentpub/v1/geo/location/geofeed');
    document.head.appendChild(jsonp);

    setTimeout(function () {
        $(".pattern-animation").css({"visibility": "visible"});
    }, 1000);


})(jQuery);
(function ($) {
    $(function () {
        $(".hero-slider .separator .slider-outer").slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            draggable: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                }
            ]
        });

        $(window).on("load resize", function (e) {
            $("body .hero-slider .slider-outer").css({"opacity": 1});
        });
    });
})(jQuery);
(function($){
$(document).ready(function() {
    $('body .image-gallery').each(function () {
        if($('.image-gallery').length > 0)
        $(".image-gallery .column-wrap .col-grid .content-box:first-child").addClass("content-bg-img");

    });
});

})(jQuery);
(function($){
$(document).ready(function(){
    var itemLength = $(".image-slider .slider .item").length;
    $('.image-slider .slider').slick({
        dots: true,
        infinite:true,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendDots: $('body .image-slider .button-wrap .dots-outer'),
        prevArrow: $('body .image-slider .button-wrap .pre-btn'),
        nextArrow: $('body .image-slider .button-wrap .next-btn'),

    });
    if ((itemLength == 1)) {
        $(".image-slider .button-wrap").css({"display": "none"});
    }
    $(window).on("load resize", function (e) {
        $(".image-slider .column-wrapper .slider").css({"opacity": 1});
    });
});
})(jQuery);

(function ($) {
  $(function () {
    var itemLength = $("body .image-with-slider .award-top-carousel .slider-outer .items").length;
    // Run code
    $("body .image-with-slider .award-top-carousel .slider-outer").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      speed: 700,
      arrows: true,
      draggable: true,
      appendDots: $('body .image-with-slider .button-wrap .dots-outer'),
      prevArrow: $('body .image-with-slider .button-wrap .pre-btn'),
      nextArrow: $('body .image-with-slider .button-wrap .next-btn'),
    });


    if ((itemLength == 1)) {
      $("body .image-with-slider .award-top-carousel .button-wrap").css({"display": "none"});
    }

  });
  $(window).on("load resize", function (e) {
    $(".slider-outer").css({"opacity": 1});
  });

  //new condition
  if ($('body .image-with-text-slider ').length > 0) {
    var window_width = $(window).width();
    if (window_width <= 768) {
      $(document).on("scroll");
      $("body .image-with-slider .button-wrap .dots-outer, body .image-with-slider .button-wrap .pre-btn, body .image-with-slider .button-wrap .next-btn ").click(function () {
        $('html, body').animate({
          scrollTop: $("body .image-with-slider .award-top-carousel .slider-outer").offset().top - 80
        }, 2000);
      });
    }
  }

})(jQuery);

(function ($) {
    $(function () {
        var itemLength = $("body .image-with-text-slider .award-top-carousel .slider-outer .items").length;
        // Run code
        $("body .image-with-text-slider .award-top-carousel .slider-outer").slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            speed: 700,
            arrows: true,
            draggable: true,
            appendDots: $('body .image-with-text-slider .button-wrap .dots-outer'),
            prevArrow: $('body .image-with-text-slider .button-wrap .pre-btn'),
            nextArrow: $('body .image-with-text-slider .button-wrap .next-btn'),
        });

        if ((itemLength == 1)) {
            $("body .image-with-text-slider .award-top-carousel .button-wrap").css({"display": "none"});
        }

        // URL redirection
        var urlHash = window.location.hash;
        if (urlHash) {
            var urlID = urlHash.substr(1);

            // Image with text
            var imgSliderItems = $('section.image-with-text-slider .items[aria-describedby]');
            var occure = 0;
            var newsInter = 0;
            var wrapperFound = false;
            if(imgSliderItems.length > 0) {
                $(imgSliderItems).each(function (index) {
                    var imgTextData = $(this).attr('data-id');
                    if (imgTextData == urlID) {
                        occure = newsInter;
                        wrapperFound = true;
                        return false;
                    }
                    newsInter++;
                });
                if (wrapperFound) {
                    setTimeout(function () {
                        $("body .image-with-text-slider .award-top-carousel .slider-outer").slick("slickGoTo", occure);
                    }, 0);
                    setTimeout(function () {
                        $('html, body').animate({
                            scrollTop: $(".image-with-text-slider").offset().top - 80
                        }, 500);
                    }, 10);
                }
            }
        }
    });
    $(window).on("load resize", function (e) {
        $(".slider-outer").css({"opacity": 1});
    });


  //new condition
  if ($('body .image-with-text-slider ').length > 0) {
    var window_width = $(window).width();
    if(window_width <= 768){
      $(document).on("scroll");
      $("body .image-with-text-slider .button-wrap .dots-outer, body .image-with-text-slider .button-wrap .pre-btn, body .image-with-text-slider .button-wrap .next-btn").click(function (){
        $('html, body').animate({
          scrollTop: $("body .image-with-text-slider").offset().top - 80
        }, 2000);
      });

    }
  }




})(jQuery);

(function ($) {
    $(document).ready(function(){

    $('.info-with-tabs .column-wrapper .tab-div .icon-with-heading').mouseover(function(){

        var winWidth = $(window).width();

        if (winWidth >= 768) {
            var tab_selected = $(this).attr('data-tab');

            $('.info-with-tabs .column-wrapper .tab-div .icon-with-heading').removeClass('hide');
            $('.info-with-tabs .column-wrapper .tab-text').removeClass('hide');

            $(this).addClass('hide');
            $("#"+tab_selected).addClass('hide');
        }
    });

    $(window).on("load resize", function (e) {
        $(".info-with-tabs .column-wrapper .scroll-wrap .tab-div .icon-with-heading").css({"opacity": 1});
    });
});

$(document).ready(function(){
    $('.info-with-tabs .column-wrapper .tab-div .icon-with-heading').click(function(){

        var winWidth = $(window).width();

        if (winWidth <= 767) {
            var tab_selected = $(this).attr('data-tab');
            var selected_text = $(this).html();

            $('.info-with-tabs .column-wrapper .tab-div .icon-with-heading').removeClass('hide');
            $('.info-with-tabs .column-wrapper .tab-text').removeClass('hide');

            $(this).addClass('hide');
            $("#"+tab_selected).addClass('hide');

            $('.info-with-tabs .column-wrapper .open .cover').html(selected_text);

            $(".info-with-tabs .column-wrapper .open").removeClass('active')
            $(".info-with-tabs .column-wrapper .scroll-wrap").slideUp();

        }
    });
    $(".info-with-tabs .column-wrapper .open").click(function () {
        var winWidth = $(window).width();
        if (winWidth < 768) {
            $(".info-with-tabs .column-wrapper .open").toggleClass('active')
            $(".info-with-tabs .column-wrapper .scroll-wrap").slideToggle();
        }
    });

});

    function wrappBlock() {
        var winWidth = $(window).width();
        if (winWidth >= 768) {
            $(".info-with-tabs .column-wrapper .scroll-wrap").find('.icon-with-heading').each(function (index, sel) {

                //Wrap 2 div in 1 div
                var n;
                var itemLength = $(".info-with-tabs .column-wrapper .tab-div .icon-with-heading").length;
                for (var i = 0; i < itemLength; i += 7) {
                    if (i < itemLength) {
                        n = i + 2;
                    }

                    if (itemLength == 4) {
                        $(this).parents("section.info-with-tabs").addClass('info-four-items')
                    }
                    if (itemLength == 6) {
                        $(this).parents("section.info-with-tabs").addClass('solution-role')
                    } else if (itemLength == 7) {
                        $(this).parents("section.info-with-tabs").addClass('seven-items')
                    }
                    if (itemLength > 7) {
                        $(this).parents("section.info-with-tabs").addClass("seven-plus-items");
                        $(".seven-plus-items .column-wrapper .scroll-wrap .tab-div").addClass("slider");
                        $(".seven-plus-items .column-wrapper .scroll-wrap .tab-div .icon-with-heading").addClass("item");

                    } else {
                        $(this).parents("section.info-with-tabs").addClass('single-item')
                    }
                }

            });
        }
    }

    $(window).on("load", function () {
        // setPosition();
        wrappBlock();
        load_OnReady();
    });

    function load_OnReady() {
        var winWidth = $(window).width();
        if (winWidth >= 768) {
            $("body .seven-plus-items .slider").slick({
                infinite: true,
                slidesToShow: 7,
                slidesToScroll: 1,
                autoplay: false,
                dots: false,
                arrows: true,
                draggable: true,
            });
            $(window).on("load resize", function (e) {
                $("body .seven-plus-items .items").css({"opacity": 1});
            });
        }

    };
})(jQuery);
(function ($) {
  $(document).ready(function () {
    $('.infographics #info-popup').click(function (e) {
      e.preventDefault();
      $('.infographic-popup').addClass('active');
    });

    $('.infographic-popup .content-box .close-popup').click(function () {
      $('.infographic-popup').removeClass('active');
    });


    //copy iframe


    $('#copytext').click(function () {
      var copyText = $('.infographic-popup #copylink');
      $(copyText).select();
      document.execCommand('copy');
    });
  });
})(jQuery);


(function ($) {
    $(window).on("load", function (e) {
      $('.insight-with-progress .content-box .column-wrapper .cover-item .we-items').matchHeight();
      $('.insight-with-progress .content-box .column-wrapper .cover-item .we-items .items').matchHeight();
    });

        $(function () {
            $(window).on("load", function () {
                $("body .insight-with-progress").each(function () {
                    var thisText = $(this).find(".content-box .column-wrapper .cover-item .text-after .top-heading h6").clone();
                    var thisTextsecond = $(this).find(".content-box .column-wrapper .cover-item .text-after .items h6").clone();

                    $(thisText).clone().appendTo('.content-box .column-wrapper .cover-item .hass-arrow .top-heading').wrap('<div class="hide" />');
                    $(thisTextsecond).clone().appendTo('.content-box .column-wrapper .cover-item .hass-arrow .items').wrap('<div class="hide" />');
                });
            });
        });


    })(jQuery);
(function ($) {
    $(window).on("load resize", function (e) {
        if ($('body .leadership-grid .cards-wrapper .card-item').length > 0) {
            var containerOffsetLeft = $('header').offset().left;
            var $windowWidth = $('header').width();
            $('body .leadership-grid .cards-wrapper .card-item').each(function (index, value) {
                var offsetLeft = $(value).offset().left;
                $(value).find('.collapse-box').css({
                    'left': (containerOffsetLeft - offsetLeft),
                    "width": $windowWidth
                });

                if ((window.matchMedia("(max-width:595px)").matches)) {
                    $(value).find('.collapse-box').css({
                        'left': (containerOffsetLeft - offsetLeft - 25),
                        "width": $windowWidth + 25
                    });
                }
            });
        }

        $("body .leadership-grid .cards-wrapper .card-item .content-wrap").matchHeight();
        $("body .leadership-grid .cards-wrapper .card-item .content-wrap .text-wrap").matchHeight();
        $("body .slider-block .container-block .slider-outer").css({"opacity": 1});
    });
    $(function () {

        $("body .leadership-grid .cards-wrapper .card-item").click(function (event) {
            event.stopPropagation();
            $("body .leadership-grid .cards-wrapper .card-item").find(".collapse-box").removeAttr('id');

            $("body .leadership-grid .cards-wrapper .card-item").removeClass("active");
            $("body .leadership-grid .cards-wrapper .card-item .collapse-box").slideUp();
            $("body .slider-event-detail").removeClass("work");
            if ($(this).find(".collapse-box").is(':visible'))
                return;
            $(this).addClass('active');

            $("body .slider-event-detail").addClass("work");

            $(this).find(".collapse-box").attr('id', 'Uni');

            $(this).find(".collapse-box").slideDown({
                start: function () {
                    if ($(this).find(".slider-outer").length) {
                        $(this).find(".slider-outer").css({"height": 56});
                        if ($(this).find(".slider-outer").hasClass("slick-slider")) {
                            $(this).find(".slider-outer").slick("unslick");
                        }
                    }
                    // slick-initialized slick-slider
                },
                done: function () {
                    if ($(this).find(".slider-outer").length) {
                        $(this).find(".slider-outer").css({"height": "auto", "transition-delay": "0.8s"});
                        $(this).find(".slider-outer").slick({
                            infinite: true,
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            dots: false,
                            arrows: true,
                            autoplay: false,
                            draggable: true,
                            responsive: [
                                {
                                    breakpoint: 992,
                                    settings: {
                                        slidesToShow: 2,
                                        draggable: true
                                    }
                                },
                                {
                                    breakpoint: 768,
                                    settings: {
                                        slidesToShow: 1,
                                        draggable: true
                                    }
                                }
                            ]
                        });
                    }
                    setTimeout(animateCol());

                }

            })
            ;


            function animateCol() {
                $('html, body').animate({
                    scrollTop: $("#Uni").offset().top - 80
                }, 800);
            }


            if ($("body .leadership-grid .cards-wrapper .card-item").hasClass(".slider-outer")) {

                var slideItemlength = $("body .leadership-grid .cards-wrapper .card-item").find(".slider-outer").length;

                if (slideItemlength < 3) {
                    console.log("unslick" + slideItemlength);
                    $("body .leadership-grid .cards-wrapper .card-item").find(".slider-outer").slick("unslick");
                }

                $("body .leadership-grid .cards-wrapper .card-item .collapse-box .right-content .content-wrapper .slider-block .slider-outer .item-box").css({"max-width": "100%"});
            }


        });

        $('body .leadership-grid .cards-wrapper .card-item .collapse-box').click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            // $("body .leadership-section .cards-wrapper .card-item").removeClass('active');
            // $('body .leadership-section .cards-wrapper .card-item .collapse-box').slideUp();
            // $("body .slider-event-detail").removeClass("work");
        });
    });


})(jQuery);


(function ($) {

    $(function () {

        var titleMain = $("body .leadership-slider .slider-block .slider-outer");
        var cycle = 1;
        if (titleMain.length) {
            titleMain.slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: false,
                dots: true,
                arrows: true,
                speed: 800,
                autoplaySpeed: 800,
                appendDots: $('body .leadership-slider .slider-block .button-wrap .dots-outer'),
                prevArrow: $('body .leadership-slider .slider-block .button-wrap .pre-btn'),
                nextArrow: $('body .leadership-slider .slider-block .button-wrap .next-btn'),
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                            draggable: true,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            draggable: true,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 595,
                        settings: {
                            slidesToShow: 1,
                            draggable: true,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            draggable: true,
                            infinite: true
                        }
                    }
                ]
            });


        }

        var slideLength = $("body .leadership-slider .slider-block .slider-outer .card-item").length;

        if (slideLength <= 4) {
            titleMain.slick('unslick');
            $("body .leadership-slider .slider-block .button-wrap").css({"display": "none"});
            $("body .leadership-slider .slider-block .slider-outer .card-item .content-wrap .footer-box").css({"max-width": "90%"});
        }

        if (slideLength == 4) {
            $("body .leadership-slider .slider-block .slider-outer .card-item").addClass("col-four");
        }
        if (slideLength == 3) {
            $("body .leadership-slider .slider-block .slider-outer .card-item").addClass("col-three");
        }
        if (slideLength == 2) {
            $("body .leadership-slider .slider-block .slider-outer .card-item").addClass("col-two");
        }
        if (slideLength == 1) {
            $("body .leadership-slider .slider-block .slider-outer .card-item").addClass("col");
        }


        $(window).on("load resize", function () {
            $(titleMain).css({"opacity": 1});
        });
    });


})(jQuery);

(function ($) {
  $(document).ready(function () {
    $('body .dialog-off-canvas-main-canvas').find(".user-login-form").parents('.dialog-off-canvas-main-canvas').addClass('login-form');
    $('body .dialog-off-canvas-main-canvas').find(".user-pass").parents('.dialog-off-canvas-main-canvas').addClass('login-form');
    $('body .dialog-off-canvas-main-canvas').find(".user-form").parents('.dialog-off-canvas-main-canvas').addClass('login-form');
    $('body .dialog-off-canvas-main-canvas').find("#block-rms-local-tasks").parents('.dialog-off-canvas-main-canvas').addClass('login-form');
  });
})(jQuery);



(function ($) {
  function setBannerheight() {
    if ($('body .new-banner').length > 0) {
      var winWidth = $(window).width();
      var val1, val2, section_height;
      val1 = $('body .new-banner .inner-text').height();
      val2 = $('body .new-banner .inner-text').offset().top;
      var extra_space1;
      if (winWidth >= 992) {
        extra_space1 = 130;
      }
      else if (winWidth >= 596) {
        extra_space1 = 130;
      }
      else {
        extra_space1 = 130;
      }
      section_height = val1 + val2 + extra_space1;

        $('.new-banner .bg-img').css({"height": section_height + "px"});
    }

  }

  $(document).ready(function () {
    setBannerheight();
  });
  $(window).on('load resize', function () {
    setBannerheight();
    $("body .new-banner").css({"opacity": 1});
  });

})(jQuery);
(function($){   
    if($('body').hasClass('node-1903')){
    var tmp1 = [];
    $('#views-exposed-form-news-block-2 .form-item-created-min input').val('2020-01-01 00:00:00'); 
            $('#views-exposed-form-news-block-2 .form-item-created-max input').val('2020-12-31 00:00:00');
    $(window).on('load',function(){
      var href = window.location.href, array=[];
      href= href.split('?');
      href.shift();    
      var newhref= href.join(" ");
      newhref= newhref.split("&");
      for(var i=0; i< newhref.length;i++){
        array.push(newhref[i].toString().split('='));
      }
      
      for(var i=0;i<array.length;i++) {
        var item = array[i];

        if (item[0] === "news") {
            $('.news-filter .content-wrapper .filter-wrap .check-content input[value="1"]').prop('checked', true);
             tmp1.push('1');
        }
        if(item[0] === "pressRelease"){
          $('.news-filter .content-wrapper .filter-wrap .check-content input[value="2"]').prop('checked', true);
           tmp1.push('2');
        }

        if (item[0] === "Year") {
            var itt = item[1];
            $('.news-filter .select-plugin .Year').val(itt);
            $('#views-exposed-form-news-block-2 .form-item-created-min input').val(itt+'-01-01 00:00:00'); 
            $('#views-exposed-form-news-block-2 .form-item-created-max input').val(itt+'-12-31 00:00:00');
        }
        
        if(item[0] === "Month"){
            var itt1= item[1];
          $('.news-filter .select-plugin .Month').parent().css('display','block');
          setTimeout(function(){     
            $('.news-filter .select-plugin .Month').val(itt1);
          $('#views-exposed-form-news-block-2 .form-item-created-min input').val(itt+'-'+itt1+'-01 00:00:00')  ;  
          $('#views-exposed-form-news-block-2 .form-item-created-max input').val(itt+'-'+itt1+'-31 00:00:00')  ; 
          },50);
        }
      }
      for(var i=0;i<tmp1.length;i++){
        $('#views-exposed-form-news-block-2 .js-form-item select').val([tmp1[0], tmp1[1]]);
      }
      setTimeout(function(){
        $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
      },300)           
  });


    $(document).on('click','.news-filter .check-content input',function(){
        var checked = $(this).val();
        var text= $(this).next().text();
        if(text.match('News'))
            text = 'news';
        else
            text = 'pressRelease';
      if ($(this).is(':checked')) {
        tmp1.push(checked);
        urlUpdate(text,checked)
      } else {
        tmp1.splice($.inArray(checked, tmp1),1);
        removeUrl(text);
      }
      for(var i=0;i<tmp1.length;i++){
        $('.views-exposed-form .js-form-item select').val([tmp1[0], tmp1[1]]);
      }
      if(tmp1.length===0){
        console.log(true);
        $('.views-exposed-form .js-form-item select').val(['1', '2']);
      }
        setTimeout(function(){
          $('.views-exposed-form .form-submit').trigger('click');
        },300)
    });

    var val1=0;
    var val=0;

    $(document).on('change','.news-filter .select-plugin .Month',function(){
        val = $(this).val();
        if(val1==='All'){
          removeUrl('Month');
          $('#views-exposed-form-news-block-2 .form-item-created-min input').val(val1+'-01-01 00:00:00')  ;  
          $('#views-exposed-form-news-block-2 .form-item-created-max input').val(val1+'-12-31 00:00:00')  ; 
          setTimeout(function(){
              $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
            },300) 
        }else{
        $('#views-exposed-form-news-block-2 .form-item-created-min input').val(val1+'-'+val+'-01 00:00:00')  ;  
        $('#views-exposed-form-news-block-2 .form-item-created-max input').val(val1+'-'+val+'-31 00:00:00')  ; 
        setTimeout(function(){
            $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
          },300) 
        }
    });
    $(document).on('change','.news-filter .select-plugin .Year',function(){
        val1 = $(this).val();
          if(val1==='All'){
          removeUrl('Month');
          removeUrl('Year')
          $('.news-filter .select-plugin .Month').parent().css('display','none');
          $('#views-exposed-form-news-block-2 .form-item-created-min input').val('')  ;  
          $('#views-exposed-form-news-block-2 .form-item-created-max input').val('')  ; 
          setTimeout(function(){
              $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
          },300)  
      }else{
        $('.news-filter .select-plugin .Month').parent().css('display','block');
        $('#views-exposed-form-news-block-2 .form-item-created-min input').val(val1+'-01-01 00:00:00')  ;  
        $('#views-exposed-form-news-block-2 .form-item-created-max input').val(val1+'-12-31 00:00:00')  ; 
        setTimeout(function(){
            $('#views-exposed-form-news-block-2 .form-submit').trigger('click');
          },300)  
        }  
    });
    $('.news-filter .reset-filter').on('click',function(){
      tmp1=[];
      var href = window.location.href;
        var href1 = href.split("?");
        history.pushState({}, null, href1[0]);
      $('.news-filter .content-wrapper .filter-wrap .check-content').each(function(){
        if ($(this).find('input').is(':checked')) {
          $(this).find('input').prop('checked', false);
        }
      });
      $('.views-exposed-form .js-form-item select').val(['1', '2']);
      setTimeout(function(){
        $('.views-exposed-form .form-submit').trigger('click');
      },300)
    })
    function removeUrl(str) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var item = 0; item < newhref.length; item++) {
            if (newhref[item].toString().includes(str)) {
              newhref.splice(item, 1)
            }
          }
          newhref = newhref.join('&');
          if (newhref === '') {
            href1 = newitem + '' + newhref;
          }
          else {
            href1 = newitem + '?' + newhref;
          }
          history.pushState({}, null, href1);
        }
      }

        // Param Update
        function urlUpdate(str, value) {
            var url = window.location.href;
            if (url.indexOf("?") > -1) {
                var urlArr = url.split("?");
                var makeURL = urlArr[0];
                for(var i=0;i<urlArr.length;i++) {
                    if(i == 0) {
                        makeURL = makeURL + '?'+urlArr[1];
                    } else {
                        makeURL = makeURL + '&'+str;
                    }
                }
                history.pushState({}, null, makeURL);
            } else {
                history.pushState({}, null, url + '?' + str);
            }
        }
        //
    }
})(jQuery);        
(function ($) {
    $(window).on("load", function (e) {
        var highestBox = 0;
        $('.news-grid .column-wrap .card-grid .grid .card-with-img').each(function () {
            if ($(this).height() > highestBox) {
                highestBox = $(this).height();
            }
        });
        $('.news-grid  .column-wrap .card-grid .grid .card-with-img').height(highestBox);


        var smallBox = 0;
        $('.news-grid .column-wrap .card-grid .grid .small-card').each(function () {
            if ($(this).height() > smallBox) {
                smallBox = $(this).height();
            }
        });
        $('.news-grid .column-wrap .card-grid .grid .small-card').height(smallBox);

        $("<div class='pagination'></div>").insertAfter("body .news-grid .cover-two-col .card-grid .grid");
        $(".pager").appendTo(".pagination");


    });


})(jQuery);
(function ($) {
    $(function () {
        $("body .news-slider .slider-block").slick({
            centerPadding: '60px',
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            draggable: true,
            appendDots: $('body .news-slider .button-wrap .dots-outer'),
            prevArrow: $('body .news-slider .button-wrap .pre-btn'),
            nextArrow: $('body .news-slider .button-wrap .next-btn'),
            responsive: [
                {
                    breakpoint: 811,
                    settings: {
                        adaptiveHeight: true
                    }
                },
            ]
        });

        $(".overview .col-grid .content-box," +
            ".logo-grid .container-box .row-wrap .content-box").on("click", function (event) {
            event.stopPropagation();
            $(".logo-grid .container-box .row-wrap .content-box").removeClass("active");
            $(".overview .col-grid .content-box").removeClass("active");
            $(this).addClass("active");
            $(this).addClass("active");
        });


        $(document).on("click", function () {
            $(".logo-grid .container-box .row-wrap .content-box").removeClass("active");
            $(".overview .col-grid .content-box").removeClass("active");
        });

        // $(window).on("load resize", function (e) {
        var itemsLength = $("body .news-slider .slider-block .item").length;
        $("body .news-slider .slider-block").css({"opacity": 1});
        if (itemsLength == 1) {
            $("body .news-slider .button-wrap").css({"display": "none"});
            $("body .news-slider .slider-block .item .resources-block .wrapper-box").addClass("one-slide");
        }


      if ($('body .news-slider ').length > 0) {

        var window_width = $(window).width();
        if (window_width <= 768) {
          $(document).on("scroll");
          $("body .news-slider .button-wrap .dots-outer, body .news-slider .button-wrap .pre-btn, body .news-slider .button-wrap .next-btn ").click(function () {
            $('html, body').animate({
              scrollTop: $("body .news-slider .slider-block").offset().top - 80
            }, 2000);
          });

        }
      };
    });

    $(document).ready(function() {
        var urlHash = window.location.hash;
        if (urlHash) {
            var urlID = urlHash.substr(1);
            var newsItems = $('section.news-slider .item[aria-describedby]');
            var wrapperFound = false;
            var occure = 0;
            var newsInter = 0;
            if (newsItems.length > 0) {
                $(newsItems).each(function (index) {
                    var mediaDataID = $(this).children('.media-block').attr('data-id');
                    var wrapperdataID = $(this).find('.wrapper-box');
                    if (mediaDataID == urlID) {
                        occure = newsInter;
                        wrapperFound = true;
                        return false;
                    } else if (!wrapperFound) {
                        $(wrapperdataID).each(function (innerIndex) {
                            var wrapperID = $(this).attr('data-id');
                            if (wrapperID == urlID) {
                                occure = newsInter;
                                wrapperFound = true;
                                return false;
                            }
                        });
                    }
                    newsInter++;
                });
                if (wrapperFound) {
                    setTimeout(function () {
                        $("body .news-slider .slider-block").slick("slickGoTo", occure);
                    }, 0);
                    setTimeout(function () {
                        $('html, body').animate({
                            scrollTop: $(".news-slider div[data-id='"+urlID+"']").offset().top - 80
                        }, 500);
                    }, 10);
                }
            }

            // Image with text
            var imageWithText = $('section.image-with-text');
            if(imageWithText.length > 0) {
                $(imageWithText).each(function (index) {
                    var imgTextData = $(this).attr('data-id');
                    if (imgTextData == urlID) {
                        $('html, body').animate({
                            scrollTop: $(this).offset().top - 80
                        }, 500);
                        return false;
                    }
                });
            }
        }
    });

})(jQuery);
(function ($) {
    $(function () {
        var itemLength = $("body .reference-carousel-with-img .col-wrapper .col-item").length;
        $("body .reference-carousel-with-img .col-wrapper ").slick({
            centerPadding: '60px',
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            draggable: true,
            appendDots: $('body .reference-carousel-with-img .button-wrap .dots-outer'),
            prevArrow: $('body .reference-carousel-with-img  .button-wrap .pre-btn'),
            nextArrow: $('body .reference-carousel-with-img .button-wrap .next-btn'),
            adaptiveHeight: true,
        });

        if (itemLength < 2) {
            $("body .reference-carousel-with-img .button-wrap").css({"display": "none"});
        }

      //new condition
      if ($('body .reference-carousel-with-img ').length > 0) {
        var window_width = $(window).width();
        if(window_width <= 768) {
          $(document).on("scroll");
          $("body .reference-carousel-with-img .button-wrap .dots-outer, body .reference-carousel-with-img  .button-wrap .pre-btn, body .reference-carousel-with-img .button-wrap .next-btn").click(function () {
            $('html, body').animate({
              scrollTop: $(" .reference-carousel-with-img .col-wrapper").offset().top - 80
            }, 2000);
          });
        }
      }

    });

    $(window).on("load resize", function (e) {
        $("body .reference-carousel-with-img .col-wrapper").css({"opacity": 1});

    });


})(jQuery);
(function ($) {
  $(function () {
    var itemLength = $("body .reference-carousel .col-wrapper .col-item").length;
    $("body .reference-carousel .col-wrapper ").slick({
      centerPadding: '60px',
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: true,
      draggable: true,
      appendDots: $('body .reference-carousel .button-wrap .dots-outer'),
      prevArrow: $('body .reference-carousel  .button-wrap .pre-btn'),
      nextArrow: $('body .reference-carousel .button-wrap .next-btn'),
    });

    if (itemLength < 2) {
      $("body .reference-carousel .button-wrap").css({"display": "none"});
    }


  });
  $(window).on("load resize", function (e) {
    $("body .reference-carousel .col-wrapper").css({"opacity": 1});

  });

  //new condition

  if ($('body .reference-carousel  ').length > 0) {
    var window_width = $(window).width();
    if (window_width <= 768) {
      $(document).on("scroll");
      $("body .reference-carousel  .button-wrap .pre-btn, body .reference-carousel .button-wrap .next-btn, body .reference-carousel .button-wrap .dots-outer ").click(function () {
        $('html, body').animate({
          scrollTop: $("body .reference-carousel .col-wrapper").offset().top - 80
        }, 2000);
        console.log("b")
      });

    }
  }

})(jQuery);
(function($){
$(document).ready(function () {
      var winWidth = $(window).width();
    if (winWidth > 596) {
        $("body .research-reports-white-papers .column-wrapper .col-three-blog .card-with-blog .card .item").matchHeight({byRow: 0});
    }
    else {

    }

});
})(jQuery);
(function ($) {
  $(function () {
    var resourcePageForm = $('.content-with-form .form-box form'),
      mktoFormId = resourcePageForm.attr('data-form-id');
      var newsletterPage = $('body.node-2692 section.content-with-form form');

    if(mktoFormId) {

      // Add mkto hidden val
      if(mktoFormId == 1594) {
        MktoForms2.whenReady(function(form) {
          form.vals({"last_form_fill_url":document.location});
        });
      }

      MktoForms2.loadForm("//app-sj10.marketo.com", "729-DJX-565", mktoFormId, function (form) {
        // Add an onSuccess handler
        form.onSuccess(function (values, followUpUrl) {
            if(mktoFormId == 1623) {
                window.location.href = 'http://rms.com/event-response/hwind/legacy-archive/storms';
            }

          // Get the form's jQuery element and hide it
          form.getFormElem().hide();

          $('.thank-you-overlay .button-box').hide();

          // Thank you message
          var thankyou_msg = $('.content-with-form input#thank-you-message').val();
          if(!thankyou_msg) {
              thankyou_msg = "Thank You";
          }


          $('.content-with-form .form-box .column-wrap').html('<p class="contact-thank-you">'+ thankyou_msg +'</p>').addClass('form-submit');

          //$('.thank-you-overlay').addClass('active');

          // Trigger event pixel for newsletter
          if(newsletterPage && newsletterPage.length) {
              try{
                  __adroll.record_user({"adroll_segments": "c0e2ac60"})
              } catch(err) {}
          }
          // Return false to prevent the submission handler from taking the lead to the follow up url
          return false;
        });
      });
    }
  });
})(jQuery);

(function ($) {

    $(function () {
      var itemLength = $("body .resource-with-card-slider .award-top-carousel .slider-outer .items").length;
      // Run code
      $("body .resource-with-card-slider .award-top-carousel .slider-outer").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        appendDots: $('body .resource-with-card-slider .button-wrap .dots-outer'),
        prevArrow: $('body .resource-with-card-slider .button-wrap .pre-btn'),
        nextArrow: $('body .resource-with-card-slider .button-wrap .next-btn'),
      });

      if (itemLength <= 1) {
        $("body .resource-with-card-slider .award-top-carousel .button-wrap").css({"display": "none"});
      }

        //new condition
        if ($('body .resource-with-card-slider  ').length > 0) {
            var window_width = $(window).width();
            if (window_width <= 768) {
                $(document).on("scroll");
                $("body .resource-with-card-slider .button-wrap .pre-btn, body .resource-with-card-slider .button-wrap .next-btn, body .resource-with-card-slider .button-wrap .dots-outer ").click(function () {
                    $('html, body').animate({
                        scrollTop: $(".resource-with-card-slider").offset().top - 60
                    }, 2000);
                    console.log("z")
                });

            }
        }

    });
    $(window).on("load resize", function (e) {
      $(".slider-outer").css({"opacity": 1});
    });

    //
    $(document).ready(function () {
      var urlHash = window.location.hash;
      if (urlHash) {
          var urlID = urlHash.substr(1);
          var testimonialItems = $('section.resource-with-card-slider .slider-outer .items[aria-describedby]');
          var testimonialFound = false;
          var occureTestimonial = 0;
          var testimonialInter = 0;
          if (testimonialItems.length > 0) {
              $(testimonialItems).each(function (index) {
                  var wrapperdataID = $(this).attr('data-id');
                  if (wrapperdataID == urlID) {
                      occureTestimonial = testimonialInter;
                      testimonialFound = true;
                      return false;
                  }
                  testimonialInter++;
              });
              if (testimonialFound) {
                  setTimeout(function () {
                      $("body .resource-with-card-slider .slider-outer").slick("slickGoTo", occureTestimonial);
                  }, 0);
                  $('html, body').animate({
                      scrollTop: $("body .resource-with-card-slider").offset().top - 80
                  }, 500);
              }
          }
      }

  });
})(jQuery);
(function($){ 
  if($('body').hasClass('node-1951')){  
    var tmp = [];
    
    $(window).on('load',function(){
      var href = window.location.href, array=[];
      href= href.split('?');
      href.shift();
      var newhref= href.join(" ");
      newhref= newhref.split("&");
      for(var i=0; i< newhref.length;i++){
        array.push(newhref[i].toString().split('='));
      }
      for(var i=0;i<array.length;i++) {
        var item = array[i];
        if (item[0] === "datasheets") {
            $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="datasheets"]').prop('checked', true);
             tmp.push('datasheets');
        }
        if(item[0] === "video"){
          $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="video"]').prop('checked', true);
           tmp.push('video');
        }
        if(item[0] === "publications"){
          $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="publications"]').prop('checked', true);
           tmp.push('publications');
      }
      if(item[0] === "infographics"){
          $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="infographics"]').prop('checked', true);
           tmp.push('infographics');
      }
      if(item[0] === "case_study"){
        $('.resources-grid .content-wrapper .filter-wrap .check-content input[value="case_study"]').prop('checked', true);
         tmp.push('case_study');
    }
      }
      for(var i=0;i<tmp.length;i++){
        $('.views-exposed-form .js-form-item select').val([tmp[0], tmp[1],tmp[2],tmp[3],tmp[4]]);
      }
      setTimeout(function(){
        $('.views-exposed-form .form-submit').trigger('click');
      },300)           
  });

    $(document).on('click','.resources-grid .check-content input',function(){
        var checked = $(this).val();
      if ($(this).is(':checked')) {
        tmp.push(checked);
        urlUpdate(checked,checked)
      } else {
        tmp.splice($.inArray(checked, tmp),1);
        removeUrl(checked);
      }
      for(var i=0;i<tmp.length;i++){
        $('.views-exposed-form .js-form-item select').val([tmp[0], tmp[1],tmp[2],tmp[3],tmp[4]]);
      }
      if(tmp.length===0){
        $('.views-exposed-form .js-form-item select').val(['datasheets', 'video','publications','infographics','case_study']);
      }
        setTimeout(function(){
          $('.views-exposed-form .form-submit').trigger('click');
        },300)
    });
    $('.resources-grid .reset-filter').on('click',function(){
      tmp=[];
      var href = window.location.href;
        var href1 = href.split("?");
        history.pushState({}, null, href1[0]);
      $('.resources-grid .content-wrapper .filter-wrap .check-content').each(function(){
        if ($(this).find('input').is(':checked')) {
          $(this).find('input').prop('checked', false);
        }
      });
      $('.views-exposed-form .js-form-item select').val(['datasheets', 'video','publications','infographics','case_study']);
      setTimeout(function(){
        $('.views-exposed-form .form-submit').trigger('click');
      },300)
    })
    function removeUrl(str) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var item = 0; item < newhref.length; item++) {
            if (newhref[item].toString().includes(str)) {
              newhref.splice(item, 1)
            }
          }
          newhref = newhref.join('&');
          if (newhref === '') {
            href1 = newitem + '' + newhref;
          }
          else {
            href1 = newitem + '?' + newhref;
          }
          history.pushState({}, null, href1);
        }
      }
    
      function urlUpdate(str, value) {
        var href = window.location.href;
        var href1 = href.split("?");
        var newitem = href1[0].toString();
        href1.shift();
        var newhref = href1.join(" ");
        newhref = newhref.split("&");
        if (href.includes(str)) {
          for (var i = 0; i < newhref.length; i++) {
            if (newhref[i].toString().includes(str)) {
              newhref.splice(i, 1)
              newhref.push(str + '=' + value)
            }
          }
          newhref = newhref.join('&')
          href1 = newitem + '?' + newhref;
          history.pushState({}, null, href1);
        }
        else {
          if (href1.length < 1) {
            history.pushState({}, null, href + '?' + str + '=' + value);
          } else {
            history.pushState({}, null, href + '&' + str + '=' + value);
          }
        }
      }
    }
})(jQuery);        
(function ($) {
    $(function () {
        $('.left-slider-block').slick({
            vertical: true,
            verticalSwiping: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            infinite: false,
            useCSS:false,
            asNavFor: '.years-block',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                    }
                }
            ]
        });
        $('.years-block').slick({
            vertical: true,
            verticalSwiping: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.left-slider-block',
            dots: false,
            infinite: false,
            arrows: true,
            centerMode: true,
            focusOnSelect: true,
            prevArrow: $('body .slider-with-dates .right-slider-block .button-wrap .pre-btn'),
            nextArrow: $('body .slider-with-dates .right-slider-block .button-wrap .next-btn'),
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        draggable: true,
                        vertical: false,
                        verticalSwiping: false,
                        centerMode: true,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        draggable: true,
                        vertical: false,
                        verticalSwiping: false,
                        centerMode: true,
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                        draggable: true,
                        vertical: false,
                        verticalSwiping: false,
                        centerMode: false,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        draggable: true,
                        vertical: false,
                        verticalSwiping: false,
                        centerMode: true,
                    }
                }
            ]
        });

        $(window).on("load resize", function () {
            if ((window.matchMedia("(min-width: 767px)").matches)) {
                $('body .slider-with-dates .items .col-two').matchHeight();
            }
        });
    });
    $(window).on("load", function (e) {
        $('body .slider-with-dates .left-slider-block').css({"opacity": 1});
        $('body .slider-with-dates .right-slider-block .years-block').css({"opacity": 1});
    });
})(jQuery);
(function ($) {
    $(function () {

        var slide_to_show = 3;
        if ($("body .statistic-slider").hasClass("box-two")) {
            slide_to_show = 2;
        }

        $("body .statistic-slider .items").slick({
            infinite: true,
            slidesToShow: slide_to_show,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true,
            arrows: false,
            draggable: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        draggable: true
                    }
                },
                {
                    breakpoint: 595,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                }
            ]

        });



        var itemLength = $("body .statistic-slider .items .item-box").length;
        if (itemLength < 4) {
            $("body .statistic-slider").addClass("no-dots");
        }
    });
    $(window).on("load", function (e) {
        $("body .statistic-slider .items").css({"opacity": 1});
    });
})(jQuery);

(function ($) {
    function wrappBlock(e) {
        $("body .statistic-slider.box-two").each(function (index, sel) {
            if ($('.statistic-slider.box-two').length > 0) {
                var itemLength = $(".statistic-slider.box-two .items .item-box").length;
                if (itemLength == 1) {
                    $(this).addClass('one-item');
                } else if (itemLength == 2) {
                    $(this).addClass('two-item');
                }
                if (itemLength >= 3) {
                    $(this).removeClass('one-item');
                    $(this).removeClass('two-item');
                } else {
                    $(this).addClass('slide');
                }
            }
        });
    }

    $(window).on("load", function () {
        wrappBlock();
    });
})(jQuery);

(function($){
    $(document).ready(function(){

    $('.tabs-with-detail .column-wrapper ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        var selected_text = $(this).text();

        $('.tabs-with-detail ul.tabs li').removeClass('current');
        $('.tabs-with-detail .tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');

        $(this).text(selected_text);

        $(".tabs-with-detail .column-wrapper .open").text(selected_text);
    });


    $(".tabs-with-detail .column-wrapper .open").click(function () {
        var winWidth = $(window).width();
        if (winWidth < 768) {
            $(".tabs-with-detail .column-wrapper .open").toggleClass("active"),
            $(".tabs-with-detail .column-wrapper ul.tabs").slideToggle();
                }
    });

    $(".tabs-with-detail .column-wrapper ul.tabs li").click(function () {
        var winWidth = $(window).width();
        if (winWidth < 768) {
            $(".tabs-with-detail .column-wrapper .open").removeClass('active'),
            $(".tabs-with-detail .column-wrapper ul.tabs").slideUp();

        }
    });

});

})(jQuery);
    
(function ($) {
    $(function () {
        // Activate tracking on home video click
        $(document).on('click', 'body.node-1616 section.video-banner .button-box a.video-play', function() {
            try{
                __adroll.record_user({"adroll_segments": "2a205996"})
            } catch(err) {}
        });

        $(document).on('click', 'body.node-2715 section.cta .button-box a.btn-red', function() {
            try{
                __adroll.record_user({"adroll_segments": "c0e2ac60"})
            } catch(err) {}
        });

    });
})(jQuery);

(function ($) {
  $(function () {


    //scroll the form
    if ($('body .two-col-with-form').length > 0) {
      $('body .two-col-with-form .column-wrapper .form-box .column-wrap form').submit(function() {
        var window_width = $(window).width();
        var header_height =$("header").outerHeight();
        if (window_width >= 992) {
          $(document).on("scroll");
          $('html, body').animate({
            scrollTop: $("body .two-col-with-form .column-wrapper ").offset().top - header_height - 20
          }, 2000);
          // console.log("x");

        }
        else {
          $(document).on("scroll");
          $('html, body').animate({
            scrollTop: $("body .two-col-with-form .column-wrapper .form-box .column-wrap").offset().top - header_height - 20
          }, 2000);
        }
      });
     }


     //showing the thank you message after submission
    var resourcePageForm = $('.two-col-with-form .form-box form'),
      mktoFormId = resourcePageForm.attr('data-form-id');
    var newsletterPage = $('body.node-2970 section.two-col-with-form form');

    if(mktoFormId) {
      MktoForms2.loadForm("//app-sj10.marketo.com", "729-DJX-565", mktoFormId, function (form) {
        // Add an onSuccess handler
        form.onSuccess(function (values, followUpUrl) {
          // Get the form's jQuery element and hide it
          form.getFormElem().hide();
          $('.thank-you-overlay .button-box').hide();
          // Thank you message
          var thankyou_msg = $('.two-col-with-form input#thank-you-message').val();
          if(!thankyou_msg) {
            thankyou_msg = "Thank You";
          }
          $('.two-col-with-form .form-box .column-wrap').html('<h3>'+ thankyou_msg +'</h3>').addClass('form-submit');
          // Trigger event pixel for newsletter
          if(newsletterPage && newsletterPage.length) {
            try{
              __adroll.record_user({"adroll_segments": "c0e2ac60"})
            } catch(err) {}
          }
          // Return false to prevent the submission handler from taking the lead to the follow up url
          return false;
        });
      });
    }
  });
})(jQuery);

//video overlay
(function ($) {
  var i = 0;
  $(document).ready(function () {
    function get_popup_video() {
      var video_play = $('.video-play').length;
      if(video_play) {
        $('.video-play').each(function () {
          var url = $(this).attr('data-video');

          // check if watch is there (for youtube)
          var check_watch = url.indexOf('watch');
          var check_youtube = url.indexOf('youtube.com');
          if(check_youtube !== -1 && check_watch !== -1) {
            var arrange_url = url.split('?');

            // Grab video ID
            var video_id = arrange_url[1].replace(/\&.*/,'').split('=');
            $(this).attr('data-video', 'https://www.youtube.com/embed/' + video_id[1]);
          }
        });
      }
    }
    get_popup_video();

    $(document).on('click', '.video-play', function (e) {
      e.preventDefault();
      var linkval = $(this).attr('data-video');
      var thisText = $(this).find("p").text();

      $(" .video-overlay").addClass("active");
      $('body').css("overflow", "hidden");
      $('.video-overlay.active iframe').attr("src", linkval + "?rel=0");
      $('.video-overlay').css({'display': '1', 'visibility': 'visible'});
      //$(".video-overlay.active .video-container p").text(thisText);
      $(".video-overlay.active .video-container h6").text(thisText);
    });

    $(".video-overlay .closeVideo, .video-overlay").click(function () {
      $('.video-overlay').removeClass('active');
      $('.video-overlay').css({'display': '0', 'visibility': 'hidden'});
      $('body').css("overflow", "inherit");
      $('.video-overlay iframe').attr("src", "");
      $(".video-overlay.active .video-container p").text("");
    });
  });

//thank you overlay
  $(".thank-you-overlay .column-wrapper .close-btn").click(function () {
    $('.thank-you-overlay').removeClass('active');
  });



  $(window).on('load',function(){

    $(document).on('click','.Infographics',function(e){
      e.preventDefault();
    })
    $( document ).ajaxComplete(function() {
      init();
    });

  });


  function init(){
    $("a[rel^='prettyPhoto']").prettyPhoto({
      markup: '<div class="pp_pic_holder">                         <div class="ppt">&nbsp;</div>                         <div class="pp_top info-top">                             <div class="pp_left"></div>                             <div class="pp_middle"></div>                             <div class="pp_right"></div>                         </div>                         <div class="pp_content_container info-pp-container">                             <div class="pp_left">                             <div class="pp_right">                                 <div class="pp_content">                                     <div class="pp_loaderIcon"></div>                                     <div class="pp_fade infographics-img">                                         <a href="#" class="pp_expand" title="Expand the image">Expand</a>                                         <a class="pp_close" href="#">Close</a>                                         <div class="pp_details">                                             <div class="pp_nav">                                                 <a href="#" class="pp_arrow_previous">Previous</a>                                                 <p class="currentTextHolder">0/0</p>                                                 <a href="#" class="pp_arrow_next">Next</a>                                             </div>                                             <p class="pp_description"></p>                                             {pp_social}                                         </div>                                         <div class="pp_hoverContainer">                                             <a class="pp_next" href="#">next</a>                                             <a class="pp_previous" href="#">previous</a>                                         </div>                                         <div id="pp_full_res"></div>                                     </div>                                 </div>                             </div>                             </div>                         </div>                         <div class="pp_bottom info-pp-bottom">                             <div class="pp_left"></div>                             <div class="pp_middle"></div>                             <div class="pp_right"></div>                         </div>                     </div>                     <div class="pp_overlay"></div>',
      animation_speed: "fast",
      slideshow: 5e3,
      autoplay_slideshow: !1,
      opacity: .8,
      show_title: !1,
      allow_resize: !0,
      default_width: 500,
      default_height: 344,
      counter_separator_label: "/",
      theme: "pp_default",
      horizontal_padding: 20,
      hideflash: !1,
      wmode: "opaque",
      autoplay: !0,
      modal: !1,
      deeplinking: !1,
      overlay_gallery: !0,
      keyboard_shortcuts: !1,
      changepicturecallback: function() {
          $("html, body").css("overflow", "hidden"), $("iframe").attr("allowFullScreen", "");
          var t = (100 - 100 * $(".pp_pic_holder.pp_default.infog").width() / $("html body").innerWidth()) / 2;
          $(".pp_pic_holder.pp_default.infog").css({
              left: t + "%",
              top: ( $(window).height() - $(".pp_pic_holder.pp_default.infog").height() ) / 2 + 10  + "px"
          })
      },
      callback: function() {
          $("html, body").css("overflow", "auto")
      },
      ie6_fallback: !0,
      social_tools: ""
  }).click(function() {
      $(".pp_pic_holder").addClass("infog")
  })
  }
  $(document).ready(function(){
        init();
    });
})(jQuery);




(function($){
    //new cookies banner//
    var cookieBanner = $("body .violator-banner");
    $(document).ready(function () {
        $(".close-button ").on("click", function () {
            $(cookieBanner).removeClass("active");
            // localStorage.setItem("cokieexist", "true");
        });
        // mas();
    });
    //
    // function mas() {
    //     setTimeout(function () {
    //         if (!localStorage.getItem("cokieexist")) {
    //             $(cookieBanner).addClass("active");
    //         }
    //     }, 1000);
    // }


    $(window).on("ready ", function () {
        $("body .violator-banner").css({"opacity": 1});

    });
    $(window).scroll(function(){
        // if($(document).width() > 992) {
        //     $(".violator-banner").css("top",Math.max(78, $(this).scrollTop()));
            if($(window).scrollTop()) {
                $("body .violator-banner").addClass("change-position");
            } else {
                $("body .violator-banner").removeClass("change-position");
            }
        // }

    });


})(jQuery);

// (function ($) {
//   $(window).on('load', function () {
//     $('.year-slider').css('opacity', 1);
//     tilesWidth();
//     cards_bg();
//
//     $('.year-slider .content-box').matchHeight();
//     $('.year-slider .slider-wrapper').mCustomScrollbar({
//       axis: "x",
//       scrollType: "continuous",
//       autoDraggerLength: false,
//       scrollbarPosition: "outside",
//       advanced: {
//         autoExpandHorizontalScroll: 2
//       },
//     });
//
//     //scroller bg full width
//     $('.year-slider .mCSB_scrollTools_horizontal .mCSB_draggerRail').append('<div class="bg-line"></div>');
//     scrollerBg();
//   });
//
//   $(window).resize(function () {
//     tilesWidth();
//     cards_bg();
//     scrollerBg();
//   });
//
//   //setting width of the columns
//   function tilesWidth() {
//     var wrapper_width = $('body .year-slider .slider-wrapper').width();
//     var target = $('.year-slider .slider-wrapper .content-box');
//     var win_width = $(window).width();
//
//     if (win_width > 992) {
//       $(target).css({
//         'width': wrapper_width / 4 + 14
//       });
//     }
//     else if (win_width > 768) {
//       $(target).css({
//         'width': wrapper_width / 3 + 14
//       });
//     }
//     else if (win_width > 595) {
//       $(target).css({
//         'width': wrapper_width / 2 + 14
//       });
//     }
//     else if (win_width > 320) {
//       $(target).css({
//         'width': wrapper_width / 1 + 14
//       });
//     }
//   }
//
//   //card bg-line position
//   function cards_bg() {
//     var card_pos = $('body .year-slider .container').position().top;
//     var h3_height = $('body .year-slider .slider-wrapper .content-box .content h3').height() / 2 - 2;
//     var h3_pos = $('body .year-slider .slider-wrapper .content-box .content h3').position().top;
//     var bg_pos = card_pos + h3_height + h3_pos;
//     $('body .year-slider .bg-cards').css({
//       'top': bg_pos
//     });
//   }
//
//   //scroller bg-line position
//   function scrollerBg() {
//     var left_value = $('.year-slider .mCSB_scrollTools_horizontal .mCSB_draggerRail').offset().left;
//     $('.year-slider .mCSB_scrollTools_horizontal .mCSB_draggerRail .bg-line').css({
//       'width': $(window).width(),
//       'left': -left_value,
//     });
//   }
// })(jQuery);
//

//# sourceMappingURL=redesign.js.map
